<?php

namespace Models;

use Models\Order;
use Models\User;
use Models\Option;
use Models\Log;
use Models\Method;
use Models\Address;
use Controllers\TokenController;
use Controllers\HelperController;

class Quotation 
{  
    private $id;

    private $from;

    private $to;

    private $products;

    private $insurance_value;

    private $options;

    private $hashCotation;

    private $codeStore;

    public $response;

    public $package;

    const URL = 'https://q-engine.melhorenvio.com';

    public function __construct($id = null, $products = array(), $package = array(), $to = null)
    {        
        $this->id = $id;
        $this->package = $package;

        $this->from = $this->getFrom();

        if (!is_null($id)) {
            $this->products = $this->getProducts();
            $this->to = $this->getTo();
            $this->insurance_value = $this->getInsuranceValue();
            
        }

        if (!empty($products) || !empty($package) ) {
            $this->products = $products;
        }

        if (!is_null($to)) {
            $this->to = $to;
        }

        $this->options = $this->getOptions();
        $this->codeStore = md5(get_option('home'));
        $this->gravarLog("get_option('home') : "+get_option('home'));
    }

    /**
     * Return an object From data quotation
     *
     * @return Object|void
     */
    public function getFrom()
    {
        try {
            $user = (new User())->get();

            $address = (new Address())->getAddressFrom();

            if ($user['success'] && $address['success']) {
                $user = $user['data'];

                return (object) array(
                    'name'     => $user['firstname'] . ' ' . $user['lastname'],
                    'email'    => $user['email'],
                    'document' => $user['document'],
                    'phone'    => $user['phone']->phone,
                    'address'  => (object) $address['address']
                );
            }
        } catch (\Exception $e) {
            // tratar log 
        }
    }

    /**
     * Return an object To data quotation
     *
     * @return Object|void
     */
    public function getTo()
    {
        try {
            $orderWc = new \WC_Order( $this->id );

            $to = $orderWc->get_data();

            $to = $to['shipping'];

            return (object) $to;
        } catch (\Exception $e) {
            // tratar log
        }
    }
    
    /**
     * Return an array with the products by quotation
     *
     * @return array|void
     */
    public function getProducts()
    {
        
        $products = [];

        try {
            $orderWc = new \WC_Order( $this->id );

            $order_items = $orderWc->get_items();
            
            foreach ($order_items as $product) {
                $data = $product->get_data();
                
                $productId = ($data['variation_id'] != 0) ? $data['variation_id'] : $data['product_id'];

                $productInfo = wc_get_product($productId);

                $products[] = (object) array(
                    'id'           => $data['product_id'],
                    'variation_id' => $data['variation_id'],
                    'name'         => $data['name'],
                    'price'        => (!empty($productInfo) ? $productInfo->get_price() : ''),
                    'height'       => (!empty($productInfo) ? $productInfo->get_height() : ''),
                    'width'        => (!empty($productInfo) ? $productInfo->get_width(): ''),
                    'length'       => (!empty($productInfo) ? $productInfo->get_length(): ''),
                    'weight'       => (!empty($productInfo) ? $productInfo->get_weight(): ''),
                    'quantity'     => intval($data['quantity']),
                    'total'        => floatval($data['total'])
                );
            }

            //gravarLog('products : '. json_encode($products, JSON_UNESCAPED_UNICODE));

            return $products;
        } catch (\Exception $e) {
            // Tratar log aqui
        }
    }

    /**
     * Return a insurance value by quotation
     *
     * @return float|void
     */
    public function getInsuranceValue()
    {
        try {
            $orderWc = new \WC_Order( $this->id );

            $data = $orderWc->get_data();

            return floatval($data['total']);
        } catch (\Exception $e) {
            // tratar log
        }
    }

    /**
     * Return a object with the options quotation
     *
     * @return Object|void
     */
    public function getOptions()
    {
        try {
            return (new Option())->getOptions();
        } catch (\Exception $e) {
            // tratar log
        }
    }

    /**
     * Create a body to make a quotation
     *
     * @return array
     */
    private function prepareBody()
    {   
        //$this->gravarLog("this->id  :  ".$this->id );
        $options = array(
            'receipt' => $this->options->ar,
            'own_hand' => $this->options->mp,
            'collect'  => false
        );

        if (!isset($this->from->address->postal_code)) {
            return null;
        }

        //pegar aqui o from 
        $from = $this->from->address->postal_code;

        if (is_object($from) || is_array($from)) {
            return null;
        }

        $to = isset($this->to->postcode) ? $this->to->postcode : $this->to;

        if (is_object($to) || is_array($to)) {
            return null;
        }

        $body = array(
            'from' => array(
                'postal_code' => preg_replace('/\D/', '', $from),
            ),
            'to' => array(
                'postal_code' => preg_replace('/\D/', '', $to),
            ),
            'settings' => array(
                'show' => array( 
                    'price' => true,
                    'discount' => true,
                    'delivery' => true
                )
            )
        );        

        $body['options'] = $options;
        $insurance_value = [];
        $ArrayProdutosIDFornecedor = [];

        $this->gravarLog('$package : '.json_encode($this->package));
        $this->gravarLog("this->products:  ".json_encode($this->products));

        if(isset($this->package['contents'])){

            foreach ($this->package['contents'] as $key => $product) {
                $ArrayProdutosIDFornecedor[] = $key;
            }
        }else{
            $CotationProduct = $this->package['cotationProduct'][0];

            $this->gravarLog('cotationProduct : '.json_encode($CotationProduct));
            $ArrayProdutosIDFornecedor[] = $CotationProduct->id;
        }        

        $this->gravarLog('$package produts : '.json_encode($ArrayProdutosIDFornecedor));

        if (!empty($this->products)) {

            foreach ($this->products as $key => $product) {

                $ProdutoID = $product->id;
                if(!in_array($ProdutoID,  $ArrayProdutosIDFornecedor)) continue;

                $VendedorID = get_post_field( 'post_author', $ProdutoID);
                $CEPVendedor = $this->getCEPVendedor($VendedorID);

                $body['from']['postal_code'] = $CEPVendedor;   

                $helper = new HelperController();

                $body['products'][] = array(
                    'id' => $product->id,
                    'quantity' => $product->quantity,
                    'volumes' => array(array(
                        'height' => (int) $helper->converterDimension($product->height),
                        'width'  => (int) $helper->converterDimension($product->width),
                        'length' => (int) $helper->converterDimension($product->length),
                        'weight' => (float) (isset($product->notConverterWeight)) ? round($product->weight,2) : round($helper->converterIfNecessary($product->weight),2)
                    )),
                    'insurance' => round(floatval($product->price), 2)
                );
                //$this->gravarLog("body products :  ".json_encode($body['products'], JSON_UNESCAPED_UNICODE));
            }
        }

        if (!empty($package)) {
            $body['volumes'][] = $package;        }
       

        $this->gravarLog("body :  ".json_encode($body, JSON_UNESCAPED_UNICODE));
        return $body;
    }

    public function getCEPVendedor($FornecedorID){
        $CEP_FornecedorV = get_user_meta($FornecedorID, '_vendor_postcode', true);       
        
        if(isset($CEP_FornecedorV) && $CEP_FornecedorV != ""){
            $CEP_Fornecedor = $CEP_FornecedorV;
        }else{
            $CEP_FornecedorS = get_user_meta($FornecedorID, 'shipping_postcode', true);
            $CEP_FornecedorB = get_user_meta($FornecedorID, 'billing_postcode', true);

            $CEP_Fornecedor = (!isset($CEP_FornecedorS) || $CEP_FornecedorS == "")?$CEP_FornecedorB:$CEP_FornecedorS;
        }
        
        $CEP_Fornecedor = str_replace("-", "", $CEP_Fornecedor);
        $CEP_Fornecedor = str_replace(".", "", $CEP_Fornecedor);

        return  $CEP_Fornecedor;
    }


    public function gravarLog($Texto) {
        return;
        $Texto .= ' - ' . date('d-m-Y H:i:s');

        $filename = $_SERVER['DOCUMENT_ROOT'] . '/arquivos/log.txt';

        $arquivo = fopen($filename, 'a+');
        $conteudoArquivo = fread($arquivo, filesize($filename));

        fwrite($arquivo, $conteudoArquivo . PHP_EOL . PHP_EOL . $Texto);
        fclose($arquivo);
        //$this->gravarLog($service.' Antes da chamada a API');
    }   

    /**
     * Function to make a quotation on API Melhor Envio
     *
     * @param null $service
     * @return array|boolean
     */
    public function calculate($service = null)
    {
                
        $token = (new TokenController())->token();

        if (!empty($token) && !is_null($token) && ($body = $this->prepareBody())) {

            $params = array(
                'headers'           =>  array(
                    'Content-Type'  => 'application/json',
                    'Accept'        => 'application/json',
                    'Authorization' => 'Bearer '.$token,
                ),
                'body'   => json_encode($body),
                'timeout'=> 10
            );

            $this->hashCotation = md5(json_encode($body));

            if (!isset($_SESSION[$this->codeStore]['cotations'][$this->hashCotation]['results'])) {
                try {
                    $response = json_decode(
                        wp_remote_retrieve_body(
                            wp_remote_post(self::URL . '/api/v1/calculate', $params)
                        )
                    );

                   // $this->gravarLog('params : '.json_encode($params, JSON_UNESCAPED_UNICODE));
                    //$this->gravarLog('response : '.json_encode($response, JSON_UNESCAPED_UNICODE));

                    if (empty($response)) {
                        return false;
                    }

                    (new Log())->register($this->id, 'make_cotation', $body, $response);

                    $filterCotations = array();
                    foreach ($response as $item) {

                        if (isset($item->error)) {
                            (new Log())->register(
                                $this->id, 'error_cotation', $body, [
                                    'service' => $item->id,
                                    'error' => $item->error
                                ]
                            );
                            continue;
                        }
                        $filterCotations[$item->id] = $item;
                    }

                    if (empty($filterCotations)) {
                        return false;
                    }

                    $_SESSION[$this->codeStore]['cotations'][$this->hashCotation] = [
                        'created' => date('Y-m-d h:i:s'),
                        'results' => $filterCotations
                    ];

                    if (!is_null($service) && !empty($_SESSION[$this->codeStore]['cotations'][$this->hashCotation]['results'][$service])) {
                       $this->gravarLog('Results : '.json_encode($_SESSION[$this->codeStore]['cotations'][$this->hashCotation]['results'][$service]));
                        return $_SESSION[$this->codeStore]['cotations'][$this->hashCotation]['results'][$service];
                    }

                    gravarLog('Results : '.json_encode($_SESSION[$this->codeStore]['cotations'][$this->hashCotation]['results']));
                    return $_SESSION[$this->codeStore]['cotations'][$this->hashCotation]['results'];

                } catch (\Exception $e) {
                    return false;
                }
            }
        } 

        if (!is_null($service) && isset($_SESSION[$this->codeStore]['cotations'][$this->hashCotation]['results'][$service])) {
            gravarLog('Results : '.json_encode($_SESSION[$this->codeStore]['cotations'][$this->hashCotation]['results'][$service]));
            return $_SESSION[$this->codeStore]['cotations'][$this->hashCotation]['results'][$service];
        }

        $this->gravarLog('Results : '.json_encode($_SESSION[$this->codeStore]['cotations'][$this->hashCotation]['results']));
        return $_SESSION[$this->codeStore]['cotations'][$this->hashCotation]['results'];
    }
}
