<?php

class AutorizarAppSellerApi {
    public static $AppID = 'on-net-shop-2020';
    public static $AppKey = '44C65CCE9F9F308114C83F93D3449234';
    public static $TaxaSite = 0.010;
    public static $TaxaPagSeguro = 0.015;


    public static function solicitarCodigoAutorizacao($UsuarioIDVendedor) {
        $AppID = AutorizarAppSellerApi::$AppID;
        $AppKey = AutorizarAppSellerApi::$AppKey;

        $UrlBaseSite = $_SERVER['HTTP_HOST'];
        
        $URL = "https://ws.pagseguro.uol.com.br/v2/authorizations/request/?appId=$AppID&appKey=$AppKey";
        $Headers = array( 'Content-Type' => 'application/xml;charset=ISO-8859-1');
        $XML = "<?xml version='1.0' encoding='UTF-8' standalone='yes'?>
        <authorizationRequest>
            <reference>$UsuarioIDVendedor</reference>
            <permissions>
                <code>CREATE_CHECKOUTS</code>
                <code>RECEIVE_TRANSACTION_NOTIFICATIONS</code>
                <code>SEARCH_TRANSACTIONS</code>
                <code>MANAGE_PAYMENT_PRE_APPROVALS</code>
                <code>DIRECT_PAYMENT</code>
            </permissions>
            <redirectURL>https://$UrlBaseSite/</redirectURL>
            <notificationURL>https://$UrlBaseSite/wp-content/plugin/pagseguro_vendedor/pagseguro_vendedor.php</notificationURL>
        </authorizationRequest>";

        //AutorizarAppSellerApi::gravarLog($XML);exit();
        
        $Resposta = AutorizarAppSellerApi::do_request($URL, "POST", $XML, $Headers);
        //var_dump($Resposta); echo ' ---------- executou';exit();

        $dom = new DOMDocument;
        $dom->loadXML($Resposta['body']);        
        $Tags = $dom->getElementsByTagName('code');

        foreach ($Tags as $Tag) {
            $CodigoAutorizacao = $Tag->nodeValue;
        }

        //echo 'Código de notificação - '. $CodigoNotificacao; exit();
        return $CodigoAutorizacao;
    }

    public static function consultarVendedorAutorizacao($CodigoNotificacao) {
        $AppID = AutorizarAppSellerApi::$AppID;
        $AppKey = AutorizarAppSellerApi::$AppKey;

        $URL = "https://ws.pagseguro.uol.com.br/v2/authorizations/notifications/$CodigoNotificacao?appId=$AppID&appKey=$AppKey";
        $Headers = array('Content-Type' => 'application/xml;charset=ISO-8859-1');
        //$XML = "";

        $Resposta = AutorizarAppSellerApi::do_request($URL, "GET", null, $Headers);
        //AutorizarAppSellerApi::gravarLog("UsuarioIDVendedor - ".$UsuarioIDVendedor. " NotificationCode - ".$NotificationCode);
        //var_dump($Resposta);exit();

        $dom = new DOMDocument;
        $dom->loadXML($Resposta['body']);        
        $Tags = $dom->getElementsByTagName('reference');

        foreach ($Tags as $Tag) {
            $UsuarioIDVendedor = $Tag->nodeValue;
        }

        //echo 'Código de notificação - '. $CodigoNotificacao; exit();
        return $UsuarioIDVendedor;
    }
    
    
    public static function do_request( $url, $method = 'POST', $data = array(), $headers = array() ) {
		$params = array(
			'method'  => $method,
			'timeout' => 60,
		);

		if ( 'POST' == $method && ! empty( $data ) ) {
			$params['body'] = $data;
		}

		if ( ! empty( $headers ) ) {
			$params['headers'] = $headers;
		}

		return wp_safe_remote_post( $url, $params );
    }


    //Grava um registro de log
    public static function gravarLog($Texto) {
        $Texto .= ' - ' . date('d-m-Y H:i:s');

        $filename = $_SERVER['DOCUMENT_ROOT'] . '/arquivos/log.txt';

        $arquivo = fopen($filename, 'a+');
        $conteudoArquivo = fread($arquivo, filesize($filename));

        fwrite($arquivo, $conteudoArquivo . PHP_EOL . PHP_EOL . $Texto);
        fclose($arquivo);
    }   
    

    public static function enviarEmailVendedor($EmailVendedor, $LinkAutorizacao) {

        $Emails = array($EmailVendedor);
        $subject = 'Link para autorizar a aplicação do site ONetShop no PagSeguro';
        $data_envio = date('d/m/Y');
        $hora_envio = date('H:i:s');

        $message = "
            <style type='text/css'>
            body {
            margin:0px;
            font-family:Verdane;
            font-size:12px;
            color: #666666;
            }
            a{
            color: #666666;
            text-decoration: none;
            }
            a:hover {
            color: #FF0000;
            text-decoration: none;
            }
            </style>
                <html>
                    <table width='100%' border='1' cellpadding='1' cellspacing='1' bgcolor='#f2f2f2'>
                        <tr>
                        <td>
                            <tr>
                            <td ><h1>Autorizar a aplicação no PagSeguro no seguinte link</h1></td>
                            </tr>
                            <tr>
                            <td >Link para autorizar a aplicação do site ONetShop: <a href='$LinkAutorizacao'>$LinkAutorizacao</a></td>
                            </tr>
                                
                        </td>
                    </tr>
                    </table>
                </html>
            ";

        $headers = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
        //$headers .= 'From: OnnetShop <contato@onnetshop.com.br>';

        wp_mail($Emails, $subject, $message, $headers);        
    }


    public static function exportarJSON($Array) {
        return json_encode($Array, JSON_UNESCAPED_UNICODE);
    }
}