<?php
/*
Plugin Name: PagSeguro Vendedor
Description: Autorização e aquisicão de PublicKey de um vendedor.
Author: Fernando Custódio
*/

include_once 'api_seller/autorizar_app_seller_api.php';

if(isset($_GET['notificationCode'])){
    $NotificationCode = $_GET['notificationCode'];
    $PublicKey = $_GET['publicKey'];
    
    $UsuarioIDVendedor = AutorizarAppSellerApi::consultarVendedorAutorizacao($NotificationCode);    

    //echo $UsuarioIDVendedor . ' Usuario';exit();
    $wpdb->insert('on_usermeta', array(
        'user_id' => intval($UsuarioIDVendedor),
        'meta_key' => 'PublicKey',
        'meta_value' => $PublicKey
    ));
    //update_user_meta(intval($UsuarioIDVendedor), 'PublicKey', $PublicKey);

    //AutorizarAppSellerApi::gravarLog("NotificationCode - ".$NotificationCode. " PublicKey - ".$PublicKey);
}

add_action( 'after_seller_create', 'gerarSolicitacaoAutorizacaoVendedor', 10, 1 );
function gerarSolicitacaoAutorizacaoVendedor($UsuarioIDVendedor){

    //AutorizarAppSellerApi::gravarLog("Antes - ");
    $UserData = get_userdata($UsuarioIDVendedor);
    $EmailVendedor = $UserData->user_email;

    //Log
    //AutorizarAppSellerApi::gravarLog("EmailVendedor - ".$EmailVendedor);
    //echo $EmailVendedor;exit();
    //var_dump($UserData);

    $ResquestCode = AutorizarAppSellerApi::solicitarCodigoAutorizacao($UsuarioIDVendedor);
    $URLLinkAutorizacao = "https://pagseguro.uol.com.br/v2/authorization/request.jhtml?code=$ResquestCode";
    //update_user_meta($UsuarioIDVendedor, 'ResquestCode', $ResquestCode);
    

    //Log
    //AutorizarAppSellerApi::gravarLog("ResquestCode - ".$ResquestCode);

    //Envia o email para o seller
    AutorizarAppSellerApi::enviarEmailVendedor($EmailVendedor, $URLLinkAutorizacao);
}

