<?php

echo "Iniciado -------- ";
AutorizarAppSellerApi::solicitarAutorizacao();

class AutorizarAppSellerApi {

    protected $AppID = 'on-net-shop-2020';
	protected $AppKey = '44C65CCE9F9F308114C83F93D3449234';

    public static function executarCURL($URL, $Metodo = "GET", $Dados = Array(), $Headers = array()) {

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $URL);

        curl_setopt($ch, CURLOPT_ENCODING, "");
        curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
  
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $Metodo);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $Headers);
       

        if ($Metodo == "POST") {
            curl_setopt($ch, CURLOPT_POSTFIELDS, $Dados);
        }

        $result = curl_exec($ch);
        curl_close($ch);

        return $result;
    }

    public static function do_request( $url, $method = 'POST', $data = array(), $headers = array() ) {
		$params = array(
			'method'  => $method,
			'timeout' => 60,
		);

		if ( 'POST' == $method && ! empty( $data ) ) {
			$params['body'] = $data;
		}

		if ( ! empty( $headers ) ) {
			$params['headers'] = $headers;
		}

		return wp_safe_remote_post( $url, $params );
	}


    public static function exportarJSON($Array) {
        return json_encode($Array, JSON_UNESCAPED_UNICODE);
    }

    public static function solicitarAutorizacao() {

        //sleep(7);
        
        $URL = "https://ws.pagseguro.uol.com.br/v2/authorizations/request/?appId=$AppID&appKey=$AppKey";
        $Headers = array( 'Content-Type' => 'application/xml;charset=UTF-8', 'Accept' => 'application/vnd.pagseguro.com.br.v3+xml');
        $XML = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
        <authorizationRequest>
            <reference>REF1234</reference>
            <permissions>
                <code>CREATE_CHECKOUTS</code>
                <code>RECEIVE_TRANSACTION_NOTIFICATIONS</code>
                <code>SEARCH_TRANSACTIONS</code>
                <code>MANAGE_PAYMENT_PRE_APPROVALS</code>
                <code>DIRECT_PAYMENT</code>
            </permissions>
            <redirectURL>http://seusite.com.br/redirect</redirectURL>
            <notificationURL>http://seusite.com.br/notification</notificationURL>
        </authorizationRequest>';

        
        //$Resposta = AutorizarAppSellerApi::do_request($URL, "POST", $XML, $Headers);
        $Resposta = AutorizarAppSellerApi::executarCURL($URL, "POST", $XML, $Headers);
        var_dump($Resposta); echo ' ---------- executou';exit();
    }

    
    //Grava um registro de log quando for chaamdo
    function gravarLog($Texto) {
        $Texto .= ' - ' . date('d-m-Y H:i:s');

        $filename = $_SERVER['DOCUMENT_ROOT'] . '/arquivos/log.txt';

        $arquivo = fopen($filename, 'a+');
        $conteudoArquivo = fread($arquivo, filesize($filename));

        fwrite($arquivo, $conteudoArquivo . PHP_EOL . PHP_EOL . $Texto);
        fclose($arquivo);
    }

    //Envia um email com informação com o Sucesso ou não da operação de Matricula do Aluno
    function enviarEmailSeller($EmailSeller, $LinkAutorizacao) {

        $Emails = array($EmailSeller);
        $subject = 'Link para autorizar a aplicação do site ONetShop no PagSeguro';
        $data_envio = date('d/m/Y');
        $hora_envio = date('H:i:s');

        $message = "
            <style type='text/css'>
            body {
            margin:0px;
            font-family:Verdane;
            font-size:12px;
            color: #666666;
            }
            a{
            color: #666666;
            text-decoration: none;
            }
            a:hover {
            color: #FF0000;
            text-decoration: none;
            }
            </style>
                <html>
                    <table width='510' border='1' cellpadding='1' cellspacing='1' bgcolor='#f2f2f2'>
                        <tr>
                        <td>
                            <tr>
                            <td ><h1>Autorizar a aplicação no PagSeguro</h1></td>
                            </tr>
                            <tr>
                            <td >Link para autorizar a aplicação do site ONetShop: <a href=''>$LinkAutorizacao</a></td>
                            </tr>
                                
                        </td>
                    </tr>
                    </table>
                </html>
            ";

        $headers = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
        //$headers .= 'From: OnnetShop <contato@onnetshop.com.br>';

        wp_mail($Emails, $subject, $message, $headers);
        
    }
}
