<?php
/**
* Config interface for provider config
*/

class Arti_Vendors_Provider_Config {
    /**
     * Readable name of the provider
     * @var string
    */
    private $name;
    /**
     * Normalised name that serves as key for in "slug => [class,name]"
     * @var string
     *
    */
    private $slug;
    /**
     * The class name chunk that will be inserted in the provider end class
     * as in "Arti_{name}_Provider"
     * @var string
    */
    private $class;

    public function __construct($name, $slug, $class){
        $this->set_name($name);
        $this->set_slug($slug);
        $this->set_class($class);
    }

    public function set_name($value) {
        $this->name = $value;
    }
    public function get_name() {
        return $this->name;
    }

    public function set_slug($slug) {
        $this->slug = $slug;
    }
    public function get_slug() {
        return $this->slug;
    }

    public function set_class($class) {
        $this->class = $class;
    }
    public function get_class() {
        return $this->class;
    }

}
