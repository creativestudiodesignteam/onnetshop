<?php

interface Arti_Shipping_Processor {
    /**
     * Gets vendor id based on product id
     * @param  int $id      the id of the product
     * @return int          the id of the vendor
     * @throws Exception    whether the product has a vendor assigned to it
     */
    public function get_vendor_id_from_product($id);

    /**
     * Gets vendor "sold by" based on product id
     * @param  int $id      the id of the vendor
     * @return string       the "sold by" of the vendor
     */
    public function get_vendor_name($id);

    /**
     * Gets vendor post code based on their id
     * @param  int $id      the id of the vendor
     * @return string       the vendor's post code
     * @throws Exception    whether the vendor has a post code assigned to them
     */
    public function get_vendor_postcode($vendor_id);

    /**
     * Marking order as shipped
     * @param  int $order_id
     * @param  int $vendor_id
     * @param  int $package_id
     * @return void
     */
    public function mark_as_shipped($order_id, $vendor_id = null, $package_id = null);
}
