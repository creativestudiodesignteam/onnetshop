<?php
/**
* Encapsulates functionalities of a WooCommerce marketplace
*
* @author   Luis Eduardo Braschi <http://art-idesenvolvimento.com.br>
* @package
* @todo     Return array of objects instead of arrays on "{split_packages_per_vendor}"
*/
abstract class Arti_Abstract_Vendors_Provider {

    public $is_active = true;
    protected $provider_not_found_message = '';

    protected function split_packages_per_vendor($order_id){

        /**
         * @todo Must throw an exception
         */
        if(!wc_get_order($order_id)) {
            return array();
        }

        $packages = wc_get_order($order_id)->get_shipping_methods();

        $filtered_packages = array();

        foreach ($packages as $package_id => $package) {
            $filtered_packages[$package_id]['id'] = $package_id; // to not need to traverse the array to search for it
            $filtered_packages[$package_id]['vendor_id'] = wc_get_order_item_meta($package_id, 'vendor_id');
            $filtered_packages[$package_id]['cost'] = wc_get_order_item_meta($package_id, 'cost');
        }

        return $filtered_packages;

    }

    /**
     * Retrieves vendors shipping amount for the order
     * @param  int|string $order_id
     * @param  int|string $vendor_id
     * @return int            The sum of the shipping costs of all packages from the order
     */
    public function get_order_shipping_amount_for_vendor($order_id, $vendor_id) {

        $packages = $this->get_package_by_vendor($order_id, $vendor_id);
        $total_amount = array_sum(wp_list_pluck($packages, 'cost'));

        return empty($total_amount)?0:$total_amount;

    }

    /**
     * Gets the shipping package sold by the vendor
     * @param  int $order_id the ID of the order
     * @return array         the package(s) from the vendor, if any; empty array otherwise
     */
    public function get_package_by_vendor($order_id, $vendor_id) {

        $vendor_packages = $this->split_packages_per_vendor($order_id);
        $vendor_packages = array_filter($vendor_packages, function($package) use ($vendor_id){
            return (int) $package['vendor_id'] === (int) $vendor_id;
        });

        return count($vendor_packages)? $vendor_packages : array();

    }

    /**
     * Gets the comission incremented or decremented by the shipping
     * @param  int $order_id the ID of the order
     * @return float         the total
     */
    abstract public function get_order_comission_by_vendor($order_id, $vendor_id);

    /**
     * Making sure the marketplace plugin is active
     * @return void
     */
    public function provider_not_found(){

        $message = $this->provider_not_found_message;
        $notice_type = 'warning';

        ob_start();
        include ARTI_WCVC_DIR . '/includes/admin/html-notice.php';
        $message_template = ob_get_clean();

        echo apply_filters('arti_wcvc_provider_not_found_message', $message_template, $message, $notice_type);

    }

    /**
     * Render corporate fields acourdingly
     * @param  int $vendor_id
     * @param  string $template
     * @return void
     */
    protected function render_corporate_fields_html( $vendor_id, $template ){
        if( !arti_wcvc_is_corporate_code_disabled() ){

            $correios_login = get_user_meta( $vendor_id, '_correios_login', true );
            $correios_password = get_user_meta( $vendor_id, '_correios_password', true );

            $password_placeholder = '';

            if( $correios_password && $correios_login ){
                $password_placeholder = '*********';
            }

            include_once $template;

        }
    }

}
