<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly.
}

add_action( 'admin_menu', 'arti_wcvc_add_admin_menu' );
add_action( 'admin_init', 'arti_wcvc_settings_init' );

add_action( 'admin_menu', 'arti_wcvc_get_settings_link');
/**
 * Get settings link
 * @return string The link
 */
function arti_wcvc_get_settings_link(){
    add_filter( 'arti_wcvc_settings_page_url', function(){
        return menu_page_url( ARTI_WCVC_SLUG, false );
    });
}

add_filter('plugin_action_links_'.ARTI_WCVC_BASENAME, 'arti_wcvc_settings_link' );
/**
 * Add settings link on plugin page
 */
function arti_wcvc_settings_link($links) {
    $url = apply_filters('arti_wcvc_settings_page_url', '');
    $settings_link = "<a href='$url'>".__('Settings', 'arti-marketplace-correios')."</a>";
    array_unshift($links, $settings_link);
    return $links;
}


function arti_wcvc_add_admin_menu(){

    add_submenu_page( 'woocommerce',
        'WooCommerce Marketplace/Correios',
        'WooCommerce Marketplace/Correios',
        'manage_options',
        ARTI_WCVC_SLUG,
        'arti_wcvc_options_page' );

}


function arti_wcvc_settings_init(){

    register_setting( 'arti_wcvc_settings', 'arti_wcvc_vendor_provider' );
    register_setting( 'arti_wcvc_settings', 'arti_wcvc_vendorname_on_calculator' );
    register_setting( 'arti_wcvc_settings', 'arti_wcvc_vendorname_on_calculator_text', 'wp_filter_nohtml_kses' );
    register_setting( 'arti_wcvc_settings', 'arti_wcvc_corporate_code_disabled' );
    register_setting( 'arti_wcvc_settings', 'arti_wcvc_one_method_allowed' );
    register_setting( 'arti_wcvc_settings', 'arti_wcvc_debug' );

    add_settings_section(
        'arti_wcvc_settings_section',
        'WooCommerce Correios/Marketplace',
        null,
        'arti_wcvc_settings'
    );

    add_settings_field(
        'arti_wcvc_vendor_provider',
        __( 'Choose the marketplace provider adapter', 'arti-marketplace-correios' ),
        'arti_wcvc_vendor_provider',
        'arti_wcvc_settings',
        'arti_wcvc_settings_section',
        array(
            'id' => 'arti_wcvc_vendor_provider',
            'label_for' => 'arti_wcvc_vendor_provider',
            'description' => __( 'This is the name of the marketplace plug-in you are using', 'arti-marketplace-correios' )
        )
    );

    add_settings_field(
        'arti_wcvc_vendorname_on_calculator',
        __( 'Display vendors\' info in the calculator', 'arti-marketplace-correios' ),
        'arti_wcvc_checkbox_element_callback',
        'arti_wcvc_settings',
        'arti_wcvc_settings_section',
        array(
            'id' => 'arti_wcvc_vendorname_on_calculator',
            'label_for' => 'arti_wcvc_vendorname_on_calculator',
            'label' => __( 'Display info about the vendor of the products in the left column of the shipping calculator.',
                            'arti-marketplace-correios' )
        )
    );

    add_settings_field(
        'arti_wcvc_vendorname_on_calculator_text',
        __( 'Text to be displayed in the calculator', 'arti-marketplace-correios' ),
        'arti_wcvc_textarea_element_callback',
        'arti_wcvc_settings',
        'arti_wcvc_settings_section',
        array(
            'id' => 'arti_wcvc_vendorname_on_calculator_text',
            'label_for' => 'arti_wcvc_vendorname_on_calculator_text',
            'description' => __( 'You can use these placeholders to add dynamic content to the text: <code>{vendor_name}</code> (the name of the store) and <code>{vendor_postcode}</code> (the post code). EG., the text <code>{vendor_name} ({vendor_postcode})</code> is translated into "Name of the Store (90020-120)". Default is the store name',
                            'arti-marketplace-correios' ),
            'default' => '{vendor_name}'
        )
    );

    add_settings_field(
        'arti_wcvc_corporate_code_disabled',
        __( 'Disable corporate code for vendors', 'arti-marketplace-correios' ),
        'arti_wcvc_checkbox_element_callback',
        'arti_wcvc_settings',
        'arti_wcvc_settings_section',
        array(
            'id' => 'arti_wcvc_corporate_code_disabled',
            'label_for' => 'arti_wcvc_corporate_code_disabled',
            'label' => __( 'Vendors won\'t see the Correios login fields in their profile.',
                            'arti-marketplace-correios' ),
        )
    );

    add_settings_field(
        'arti_wcvc_one_method_allowed',
        __( 'Group all packages in one', 'arti-marketplace-correios' ),
        'arti_wcvc_checkbox_element_callback',
        'arti_wcvc_settings',
        'arti_wcvc_settings_section',
        array(
            'id' => 'arti_wcvc_one_method_allowed',
            'label_for' => 'arti_wcvc_one_method_allowed',
            'label' => __( 'Group the packages from all the vendors and sum their totals.', 'arti-marketplace-correios' ),
            'description' => __( 'Keep in mind that only methods set globally in the Shipping Zone settings can be used.', 'arti-marketplace-correios' ),
            'default' => 'yes'
        )
    );

    $logs_url = esc_url(
                    add_query_arg(
                        'log_file',
                        wc_get_log_file_name( 'arti-wcvc' ),
                        admin_url( 'admin.php?page=wc-status&tab=logs' )
                    )
                );

    $logs_link = sprintf( '<a href="%1$s">%2$s</a>', $logs_url, __( 'Check logs.', 'arti-marketplace-correios' ) );

    add_settings_field(
        'arti_wcvc_debug',
        __( 'Debug this extension', 'arti-marketplace-correios' ),
        'arti_wcvc_checkbox_element_callback',
        'arti_wcvc_settings',
        'arti_wcvc_settings_section',
        array(
            'id' => 'arti_wcvc_debug',
            'label_for' => 'arti_wcvc_debug',
            'description' => __( 'Logs errors related to Art-i Marketplace-Correios.',
                                 'arti-marketplace-correios' ) . " $logs_link",
            'label' => __( 'Enables plug-in debugging.', 'arti-marketplace-correios' ) ,
        )
    );

}

function arti_wcvc_textarea_element_callback( $args ) {

    $id      = $args['id'];
    $option = get_option( $id );

    if ( $option ) {
        $current = $option;
    } else {
        $current = isset( $args['default'] ) ? $args['default'] : '';
    }

    include dirname( __FILE__ ) . '/admin/html-textarea-field.php';

}
function arti_wcvc_checkbox_element_callback( $args ) {
    $id      = $args['id'];
    $option = get_option( $id );

    if ( isset( $option ) ) {
        $current = $option;
    } else {
        $current = isset( $args['default'] ) ? $args['default'] : '0';
    }

    include dirname( __FILE__ ) . '/admin/html-checkbox-field.php';
}

function arti_wcvc_vendor_provider( $args ){

    $providers_list = arti_wcvc_get_providers_list();
    $providers = apply_filters('arti_wcvc_list_providers', $providers_list);

    $vendor_provider = get_option( 'arti_wcvc_vendor_provider' );

    ?>
    <select name='arti_wcvc_vendor_provider'>
        <option value=''><?php _e('Please select an option.', 'arti-marketplace-correios')?></option>
    <?php foreach($providers as $provider):?>
        <option value='<?=$provider->get_slug()?>' <?php selected( $vendor_provider, $provider->get_slug() ); ?>>
            <?=$provider->get_name()?>
        </option>
    <?php endforeach?>
    </select>
    <?php if ( isset( $args['description'] ) ) : ?>
        <p class="description"><?php echo wp_kses_post( $args['description'] ); ?></p>
    <?php endif;
}

function arti_wcvc_options_page(){

    ?>
    <form action='options.php' method='post'>

        <?php
            settings_fields( 'arti_wcvc_settings' );
            do_settings_sections( 'arti_wcvc_settings' );
            submit_button();
        ?>

    </form>
    <p><?php
        // translators: %s URL to docs
        printf(
            __( 'Read more about theses settings <a href="%s" target="_blank" rel="noopener noreferrer">here</a>',
                'arti-marketplace-correios' ),
            'https://art-idesenvolvimento.com.br/wordpress/plugins/frete-correios-marketplace/manual.html?utm_source=client-admin'
        );?>
    </p>
    <?php

}
