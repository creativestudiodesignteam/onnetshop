<?php
/**
 * Marketplace plugins that this plugin supports
 * @return array List of marketplace functionality providers
 */
function arti_wcvc_get_providers_list(){

    $providers_list = array('wc-vendors'     => new Arti_Vendors_Provider_Config('WC Vendors',
                                                                                 'wc-vendors','WC_Vendors'),
                            'wc-marketplace' => new Arti_Vendors_Provider_Config('WC Marketplace Multivendors',
                                                                                 'wc-marketplace','WC_Marketplace'),
                            'dokan'          => new Arti_Vendors_Provider_Config('Dokan',
                                                                                 'dokan','Dokan'));

    return apply_filters('arti_wcvc_list_providers', $providers_list);
}

/**
 * Returns the global provider loaded
 * @return Arti_Shipping_Processor
 */
function arti_wcvc_current_provider(){
    return Arti_Vendor_Provider_Factory::$current_provider;
}

function arti_wcvc_woocommerce_locate_template($template, $template_name, $template_path = null) {

    global $woocommerce;
    $_template = $template;

    if (! $template_path) {
        $template_path = $woocommerce->template_url;
    }

    $template_base_path = arti_wcvc_template_base_path();

    $template = locate_template(
        array(
            $template_path . $template_name,
            $template_name
           )
       );

    $template_relative_path = $template_base_path . $template_path . $template_name;

    if (! $template && file_exists($template_relative_path)){
        $template = $template_relative_path;
    }
    if (! $template) {
        $template = $_template;
    }

    return $template;

}

/**
 * Convert currrent origin postcode into vendor postcode if it exists
 * @param  string $current_postcode
 * @param  array $package
 * @return string
 */
function arti_wcvc_vendor_postcode($origin_postcode, $package) {
    $origin_postcode = isset($package['vendor_postcode'])
                    && !empty($package['vendor_postcode']) ? $package['vendor_postcode'] : $origin_postcode;

    return $origin_postcode;
}

/**
 * Lists allowed Correios shipping methods
 * @return array
 */
function arti_wcvc_list_shipping_methods(){
    $list = array(
        'correios-pac',
        'correios-sedex',
        'correios-sedex10-envelope',
        'correios-sedex10-pacote',
        'correios-sedex12',
        'correios-sedex-hoje',
        'correios-esedex',
        'correios-carta-registrada',
        'correios-impresso-normal',
        'correios-impresso-urgente',
        'correios-mercadoria-expressa',
        'correios-mercadoria-economica',
        'correios-leve-internacional',
   );

    return apply_filters('arti_wcvc_list_shipping_methods', $list);
}

/**
 * Lists all active shipping methods system-wide
 * @return array Array containing the shipping methods in the format ['method-id' => 'Method Title']
 */
function arti_wcvc_list_active_shipping_methods(){
    $active_methods = array();

    $zones = WC_Shipping_Zones::get_zones();
    $methods_in_zones = array();

    foreach ($zones as $zone) {
        $active_methods_in_zone = arti_wcvc_filter_active_methods($zone['shipping_methods']);
        $methods_in_zones = array_merge($methods_in_zones, $active_methods_in_zone);
    }

    $global_shipping_methods = arti_wcvc_filter_active_methods(WC()->shipping->load_shipping_methods());

    $all_active_methods = array_merge($global_shipping_methods, $methods_in_zones);

    return arti_wcvc_methods_to_list($all_active_methods);
}

/**
 * Filters only active methods
 * @param  array $methods
 * @return array
 */
function arti_wcvc_filter_active_methods($methods){
    foreach ($methods as $id => $shipping_method) {
        if ($shipping_method->is_enabled()) {
            $active_methods[$shipping_method->id] = $shipping_method;
        }
    }

    return apply_filters('arti_wcvc_filter_active_methods', $active_methods);
}

/**
 * Creates a ['method-id' => 'Method Title'] list to populate comboboxes etc.
 * @param  array $methods
 * @return array Te formatted list
 */
function arti_wcvc_methods_to_list($methods){
    $methods_list = array();
    foreach ($methods as $id => $shipping_method) {
        $method_title = $shipping_method->method_title;
        $methods_list[$shipping_method->id] = $method_title;
    }
    return apply_filters('arti_wcvc_methods_to_list', $methods_list);
}

/**
 * WooCommerce package abstraction
 * @param  array $package_array The data to be inserted in the packacge
 * @return array
 */
function arti_wcvc_default_package($package_array){

    $customer = WC()->customer;

    $defaults = array(
            'contents' => array(),
            'contents_cost'   => 0,
            'applied_coupons' => WC()->cart->applied_coupons,
            'user'            => array('ID' => $customer->get_id()),
            'destination'     => array(
                'country'       => $customer->get_shipping_country(),
                'state'         => $customer->get_shipping_state(),
                'postcode'      => $customer->get_shipping_postcode(),
                'city'          => $customer->get_shipping_city(),
                'address'       => $customer->get_shipping_address(),
                'address_2'     => $customer->get_shipping_address_2()
           )
       );

    $package_array = wp_parse_args($package_array, $defaults);

    return $package_array;

}

function arti_wcvc_enqueue_scripts(){
    wp_enqueue_script('arti-wcvc', ARTI_WCVC_JS . '/main.js', array('jquery'), true);
}
add_action('admin_enqueue_scripts', 'arti_wcvc_enqueue_scripts', 10);

/**
 * Helper to check whether method is one from Correios
 * @param  string $method_label
 * @return bool
 */
function arti_wcvc_is_correios_method($method_label){
    return in_array($method_label, arti_wcvc_list_shipping_methods());
}


function arti_wcvc_is_corporate_code_disabled(){
    return get_option('arti_wcvc_corporate_code_disabled', false);
}

function arti_wcvc_update_user_meta( $vendor_id ){
    $correios_disabled = 'no';

    if( isset($_POST['_arti_shipping_correios_disabled']) ) {
        $correios_disabled = 'yes';
    }

    update_user_meta( $vendor_id, '_arti_shipping_correios_disabled', $correios_disabled );

    $login_empty = true;

    if( isset( $_POST['_correios_login'] )) {
        update_user_meta( $vendor_id, '_correios_login', sanitize_text_field( $_POST[ '_correios_login' ] ));
        $login_empty = empty( $_POST['_correios_login'] );
    }

    if( isset( $_POST['_correios_password'] )
        && !empty( $_POST['_correios_password'] )
        && !$login_empty )
    {
        update_user_meta( $vendor_id, '_correios_password', sanitize_text_field( $_POST[ '_correios_password' ] ));
    } else {
        delete_user_meta( $vendor_id, '_correios_password' );
    }
}

/**
 * Helper to add logs to WC Logs
 * @param  string $message
 * @param  string $handle
 * @return void
 */
function arti_wcvc_log( $message, $handle = 'arti-wcvc' ){

	$debug = (bool) get_option( 'arti_wcvc_debug', false );

	if( $debug ) {
		wc_get_logger()->add( $handle, $message );
	}

}
