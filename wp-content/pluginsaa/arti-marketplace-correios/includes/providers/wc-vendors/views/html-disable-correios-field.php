<div>
    <p>
        <input type="checkbox" id="_arti_shipping_correios_disabled" name="_arti_shipping_correios_disabled" value="yes" <?php checked( $correios_disabled, 'yes' ); ?>>
        <b><label for="_arti_shipping_correios_disabled"><?php esc_html_e( 'Disable Correios', 'arti-marketplace-correios' ); ?></label></b>
    </p>
</div>
