<div class="wcv-cols-group wcv-horizontal-gutters">
    <div class="all-50 small-100">
        <div class="control-group">
            <label for="_correios_login"><?php esc_html_e( 'Correios Login', 'arti-marketplace-correios' ); ?></label>
            <div class="control">
                <input type="text" name="_correios_login" id="_correios_login" value="<?php echo esc_attr( $correios_login ) ?>" >
            </div>
        </div>
    </div>
    <div class="all-50 small-100">
        <div class="control-group">
            <label for="_correios_password"><?php esc_html_e( 'Correios Password', 'arti-marketplace-correios' ); ?></label>
            <div class="control">
                <input type="password" name="_correios_password" id="_correios_password" placeholder="<?php echo $password_placeholder; ?>">
            </div>
        </div>
    </div>
</div>
