<div>
    <p>
        <b><label for="arti_wcvc_postcode"><?php esc_html_e( 'Shop post code', 'arti-marketplace-correios' ); ?></label></b>
        <br>
        <input type="text" name="arti_wcvc_postcode" id="arti_wcvc_postcode"
                   value="<?php echo get_user_meta( $vendor_id, 'arti_wcvc_postcode', true ); ?>" >

    </p>
</div>
