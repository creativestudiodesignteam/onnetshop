<?php
/**
* Encapsulates functionalities of a WooCommerce marketplace
*
* @author  Luis Eduardo Braschi <http://art-idesenvolvimento.com.br>
* @package
*
*/
class Arti_WC_Vendors_Provider extends Arti_Abstract_Vendors_Provider implements Arti_Shipping_Processor {

    private $is_pro = false;

    public function __construct(){

        if( !class_exists( 'WC_Vendors' ) ){

            $this->provider_not_found_message = __( 'The plugin "WC Vendors" could not be found. Please, install and activate it before registering this adapter.', 'arti-marketplace-correios' );
            add_action( 'admin_notices', array( $this, 'provider_not_found' ));
            $this->is_active = false;

        } else {

            add_action( 'woocommerce_order_status_processing', array( $this, 'process_shipping_due' ), 11 );
            add_action( 'woocommerce_order_status_completed', array( $this, 'process_shipping_due' ), 11 );

            add_action( 'wcv_product_options_shipping', array( $this, 'disable_correios_field' ), 11 );
            add_action( 'wcv_save_product_meta', 'save_product_meta', 11 );

            add_action( 'arti_wcvc_provider_wc-vendors_loaded', array( $this, 'hooks' ), 11 );

        }

    }


    public function hooks(){

        $this->is_pro = defined( 'WCV_PRO_PLUGIN_FILE' );

        if( current_user_can( 'administrator' ) && is_admin() ){ // admin

            if( !$this->is_pro ){
                add_action( 'wcvendors_admin_after_shop_html', array( $this, 'add_admin_postcode_field_by_user' ) );
            }

            add_action( 'wcvendors_admin_after_shop_html', array( $this, 'add_admin_disable_correios_vendor_fields_by_user' ));
            add_action( 'edit_user_profile', array( $this, 'add_admin_corporate_fields_by_user' ));

        } elseif( is_admin() ) { // vendor admin
            add_action( 'wcvendors_settings_before_bank_details', array( $this, 'add_admin_postcode_field' ) );
            add_action( 'wcvendors_settings_before_bank_details', array( $this, 'add_admin_disable_correios_vendor_fields' ));
            add_action( 'wcvendors_settings_before_bank_details', array( $this, 'add_admin_vendor_corporate_fields' ));
        } else { // vendor front-end
            add_action( 'wcvendors_settings_before_bank_details', array( $this, 'add_postcode_field' ) );
            add_action( 'wcvendors_settings_before_bank_details', array( $this, 'add_vendor_disable_correios_fields' ) );
            add_action( 'wcvendors_settings_before_bank_details', array( $this, 'add_vendor_corporate_fields' ) );
        }

        add_action( 'wcvendors_settings_after_address', array( $this, 'add_pro_vendor_corporate_fields' ) );
        add_action( 'wcvendors_settings_after_address', array( $this, 'add_pro_vendor_disable_correios_fields' ) );

        add_action( 'wcvendors_shop_settings_admin_saved', array( $this, 'save_vendor_fields' ) );
        add_action( 'wcvendors_shop_settings_saved', array( $this, 'save_vendor_fields' ) );
        add_action( 'wcvendors_update_admin_user', array( $this, 'save_vendor_fields' ) );

        add_action( 'wcv_pro_store_settings_saved', array( $this, 'save_vendor_fields' ) ); // pro version

    }


    public function get_vendor_postcode( $vendor_id ){

        $post_code = get_user_meta( $vendor_id, '_wcv_store_postcode', true ); // pro version
        $post_code = !$post_code ? get_user_meta( $vendor_id, 'arti_wcvc_postcode', true ) : $post_code;

        if( !$post_code ){
            $message = sprintf(__( 'Vendor with id %d has no post code.', 'arti-marketplace-correios' ), $vendor_id);
            throw new Exception($message);
        }

        return $post_code;
    }

    public function get_vendor_id_from_product( $id ){

        $vendor_id = WCV_Vendors::get_vendor_from_product($id);

        if( false === $vendor_id ){
            $message = sprintf(__( 'Product with id %d has no vendor assigned to it.', 'arti-marketplace-correios' ), $id);
            throw new Exception($message);
        }

        return $vendor_id;
    }

    public function get_vendor_name( $id ){
        return WCV_Vendors::get_vendor_sold_by( $id );
    }

    public function mark_as_shipped( $order_id, $vendor_id = null, $package_id = null ){
        $shippers = (array) get_post_meta( $order_id, 'wc_pv_shipped', true );
        $vendor_id = !is_null($vendor_id)? $vendor_id : get_current_user_id();
        if( !in_array( $vendor_id, $shippers)){
            $order = new WC_Order($order_id);
            $shippers[] = $vendor_id;
            $vendor_name = $this->get_vendor_name($vendor_id);
            $order->add_order_note( apply_filters( 'wcvendors_vendor_shipped_note', sprintf( __(  '%s has marked as shipped.', 'arti-marketplace-correios' ), $vendor_name ) ), $vendor_id, $vendor_name );

            if(!is_admin()){
                wc_add_notice( __( 'Order marked shipped.', 'arti-marketplace-correios' ), 'success' );
            }

        } elseif ( false != ( $key = array_search( $vendor_id, $shippers) ) ){
            unset( $shippers[$key] );
        }

        update_post_meta( $order_id, 'wc_pv_shipped', $shippers );
    }

    /**
     * Distributes the shipping dues accourding to their vendors
     * @param  int $order_id the id of the WooCommerce order
     * @return void
     */
    public function process_shipping_due( $order_id ){

        global $wpdb;

        $packages = $this->split_packages_per_vendor( $order_id );

        $summed_packages = array();

        foreach( $packages as $package ){

            if( !isset( $summed_packages[$package['vendor_id']] ) ){
                $summed_packages[$package['vendor_id']] = 0;
            }

            $summed_packages[$package['vendor_id']] = $summed_packages[$package['vendor_id']] + $package['cost'];

        }

        $table = $wpdb->prefix . "pv_commission";

        $give_shipping = get_option( 'wcvendors_vendor_give_shipping' );

        foreach( $summed_packages as $vendor_id => $cost ){

            $give_shipping_override = get_user_meta( $vendor_id, 'wcv_give_vendor_shipping', true );

            if( $give_shipping || $give_shipping_override ){

                $prepare = $wpdb->prepare( "UPDATE $table
                                            SET total_shipping = %.2f
                                            WHERE order_id = %d
                                            AND vendor_id = %d
                                            LIMIT 1",
                                            $cost,
                                            $order_id,
                                            $vendor_id );

                $update = $wpdb->query( $prepare );

            }
        }

    }

    public function get_order_comission_by_vendor( $order_id, $vendor_id ){
        return 0;
    }

    /**
     * Add postcode field
     * @param  $user
     */
    public function add_postcode_field( $vendor_id ){
        include_once ARTI_WCVENDORS_DIR . '/views/html-postcode-div.php';
    }

    /**
     * Will add new fields to vendor settings in the admin
     * @param  $user
     */
    public function add_admin_postcode_field( $vendor_id ){
        include_once ARTI_WCVENDORS_DIR . '/views/html-postcode-table-row.php';
    }

    public function add_admin_postcode_field_by_user( $user ){
        $this->add_admin_postcode_field( $user->ID );
    }

    /**
     * Will add corporate fields to user profile in admin controlled by the admin
     * @param WP_User $user
     * @return void
     */
    public function add_admin_corporate_fields_by_user( $user ){
        $this->render_corporate_fields_html( $user->ID, ARTI_WCVC_DIR . '/includes/admin/html-corporate-fields.php' );
    }

    /**
     * Will add corporate fields to user profile in admin controlled by the vendor
     * @param int $vendor_id
     * @return void
     */
    public function add_admin_vendor_corporate_fields( $vendor_id ){
        $this->render_corporate_fields_html( $vendor_id, ARTI_WCVENDORS_DIR . '/views/html-admin-corporate-fields.php' );
    }

    /**
     * Will add corporate fields to user profile in the front-end
     * @param int $vendor_id
     * @return void
     */
    public function add_vendor_corporate_fields( $vendor_id ){
        $this->render_corporate_fields_html( $vendor_id, ARTI_WCVENDORS_DIR . '/views/html-corporate-fields.php' );
    }

    /**
     * Will add corporate fields to the pro user profile in the front-end
     * @param int $vendor_id
     * @return void
     */
    public function add_pro_vendor_corporate_fields(){
        $vendor_id = get_current_user_id();
        $this->render_corporate_fields_html( $vendor_id, ARTI_WCVENDORS_DIR . '/views/html-pro-corporate-fields.php' );
    }

    /**
     * Save values from above
     * @param  $vendor_id
     */
    public function save_vendor_fields( $vendor_id ){

        if( isset( $_POST[ 'arti_wcvc_postcode' ] ) ) {
            update_user_meta( $vendor_id, 'arti_wcvc_postcode', sanitize_text_field( $_POST[ 'arti_wcvc_postcode' ] ));
        }

        arti_wcvc_update_user_meta( $vendor_id );

    }

    /**
     * Adds field to WC Vendors' product shipping on Pro Dashboard
     * @param  int $product_id
     * @return void
     */
    public function disable_correios_field( $product_id ){

        WCVendors_Pro_Form_Helper::input(
            apply_filters( 'wcv_disable_correios_field',
                array(
                    'post_id'           => $product_id,
                    'id'                => '_arti_shipping_correios_disabled',
                    'label'             => __( 'Disable Correios shipping for this product', 'arti-marketplace-correios' ),
                    'type'              => 'checkbox',

                )
            )
        );

    }

    /**
     * Adds field to WC Vendors' store form
     * @return void
     */
    public function add_pro_vendor_disable_correios_fields(){

        $correios_disabled = get_user_meta( get_current_user_id(), '_arti_shipping_correios_disabled', true );

        WCVendors_Pro_Form_Helper::input(
            apply_filters(
                'wcv_arti_shipping_correios_disabled',
                array(
                    'id'                => '_arti_shipping_correios_disabled',
                    'label'             => __( 'Disable Correios Shipping store-wide', 'arti-marketplace-correios' ),
                    'type'              => 'checkbox',
                    'wrapper_start'     => '<div class="wcv-cols-group wcv-horizontal-gutters"><div class="all-100">',
                    'wrapper_end'       => '</div></div>',
                    'value'             => $correios_disabled
                )
            )
        );

    }

    public function add_vendor_disable_correios_fields( $vendor_id ){
        $correios_disabled = get_user_meta( $vendor_id, '_arti_shipping_correios_disabled', true );
        include_once ARTI_WCVENDORS_DIR . '/views/html-disable-correios-field.php';
    }

    public function add_admin_disable_correios_vendor_fields( $vendor_id ){
        $correios_disabled = get_user_meta( $vendor_id, '_arti_shipping_correios_disabled', true );
        include_once ARTI_WCVENDORS_DIR . '/views/html-disable-correios-field-vendor-admin.php';
    }

    public function add_admin_disable_correios_vendor_fields_by_user( $vendor ){
        $this->add_admin_disable_correios_vendor_fields( $vendor->ID );
    }
    /**
     * Save metadata for product coming from the front-end
     * @param  int $product_id
     * @return void
     */
    public function save_product_meta( $product_id ){
        arti_wcvc_disable_methods_save( $product_id );
    }

}
