<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly.
}

define( 'ARTI_DOKAN_DIR', dirname( __FILE__ ));

function arti_wcvc_dokan_scripts(){
    $package_url = plugin_dir_url( __FILE__ );
    $suffix = defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ? '' : '.min';
    $dashboard_scripts = $package_url . 'assets/dashboard' . $suffix . '.js';
    wp_enqueue_script('arti-dokan-dashboard', $dashboard_scripts, array('jquery', 'wp-util'));
}
add_action('wp_enqueue_scripts', 'arti_wcvc_dokan_scripts');

include_once 'dokan-functions.php';
include_once 'class-dokan.php';
