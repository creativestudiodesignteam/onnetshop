<?php echo $message; ?>
<?php if(!empty($tracking_url)):?>
<p><strong><?php _e('Tracking URL', 'arti-marketplace-correios')?></strong>: <?php echo $tracking_url?></p>
<?php endif; ?>
<p><strong><?php _e('Tracking code', 'arti-marketplace-correios')?></strong>: <?php echo $tracking_code?></p>
