<tr>
    <th><?php esc_html_e( 'Disable Correios Shipping store-wide', 'arti-marketplace-correios' ); ?></th>
    <td>
        <input type="checkbox" name="_arti_shipping_correios_disabled" class="regular-text" value="yes" <?php checked( $correios_disabled, 'yes' ) ?>>
    </td>
</tr>
