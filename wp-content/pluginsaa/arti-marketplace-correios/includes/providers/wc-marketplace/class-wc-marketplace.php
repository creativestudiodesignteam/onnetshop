<?php
/**
* Encapsulates functionalities of WC Marketplace by Dual Cube <https://wordpress.org/plugins/dc-woocommerce-multi-vendor/>
*
* @author  Luis Eduardo Braschi <http://art-idesenvolvimento.com.br>
* @package
*
*/
class Arti_WC_Marketplace_Provider extends Arti_Abstract_Vendors_Provider implements Arti_Shipping_Processor {

    private $shipping_link;

    public function __construct() {

        if( !class_exists( 'WCMp' ) ) {

            $this->provider_not_found_message = __( 'The plugin "WC Marketplace" could not be found. Please, install and activate it before registering this adapter.', 'arti-marketplace-correios' );

            add_action( 'admin_notices', array( $this, 'provider_not_found' ) );
            $this->is_active = false;

        } else {

            add_filter( 'vendor_due_per_order', array( $this, 'process_shipping_due' ), 20, 3 );
            add_action( 'init', array( $this, 'override_hooks' ) );

            add_filter( 'wcmp_fpm_fields_shipping', array( $this, 'disable_correios_field' ), 11, 2 );
            add_filter( 'after_wcmp_fpm_data_meta_save', array( $this, 'save_product_meta' ), 11, 3 );

            add_filter( 'wcmp_locate_template', 'arti_wcvc_woocommerce_locate_template', 11, 3 );

            add_filter( 'wcmp_my_account_my_orders_actions', array( $this, 'override_shipping_action_html' ), null, 2 );
            add_filter( 'wcmp_widget_vendor_pending_shipping_row_data', array( $this, 'override_shipping_action_html_by_order' ), 10, 2 );

            /**
             * Tracking fields
             */
            add_action( 'before_wcmp_vendor_pending_shipping', array( $this, 'add_tracking_fields_template' ));
            add_action( 'wcmp_vendor_dashboard_vendor-orders_endpoint', array( $this, 'add_tracking_fields_template' ));
            add_action( 'wcmp_vendor_dashboard_vendor-orders_endpoint', array( $this, 'update_tracking_info' ), 9 );
            add_action( 'before_wcmp_dashboard_widget', array( $this, 'update_tracking_info' ), 9 );

            add_filter( 'wcmp_checkout_update_order_meta',array( $this, 'add_methods_to_suborders' ), 9, 2 );

            // admin fields
            add_filter( 'wcmp_vendor_user_fields', array( $this, 'add_vendor_fields_admin' ), 10, 2 );
            add_filter( 'wcmp_vendor_user_fields', array( $this, 'add_disable_correios_vendor_fields_admin' ), 10, 2 );
            add_filter( 'arti_wcvc_wcmp_add_disable_field', array( $this, 'add_disable_correios_vendor_fields' ), 10, 2 );
            add_action( 'personal_options_update', array( $this, 'save_vendor_fields' ) );
            add_action( 'edit_user_profile_update', array( $this, 'save_vendor_fields' ) );

            // front-end  fields
            add_action( 'other_exta_field_dcmv', array( $this, 'add_vendor_fields' ) );
            add_action( 'before_wcmp_vendor_dashboard', array( $this, 'save_vendor_fields' ) );

            add_action( 'add_meta_boxes', function(){
                add_meta_box(
                    'wc_correios',
                    __( 'Tracking codes', 'arti-marketplace-correios' ),
                    array( $this, 'override_tracking_code_fields' ),
                    'shop_order',
                    'side',
                    'default'
                );
            }, 12 );

        }

    }


    public function get_vendor_id_from_product( $id ) {

        $variation = wc_get_product( $id );

        if ( $variation->get_parent_id() ){
            $id = $variation->get_parent_id();
        }

        $vendor = get_wcmp_product_vendors( $id );

        if( false === $vendor ) {
            $message = sprintf( __( 'Product with id %d has no vendor assigned to it.', 'arti-marketplace-correios' ), $id );
            throw new Exception( $message );
        }

        return $vendor->user_data->data->ID;
    }

    public function get_vendor_postcode( $vendor_id ){

        $post_code = get_user_meta( $vendor_id, '_vendor_postcode', true );

        if( false === $post_code ) {
            $message = sprintf( __( 'Vendor with id %d has no post code.', 'arti-marketplace-correios' ), $vendor_id );
            throw new Exception( $message );
        }

        return $post_code;
    }

    public function get_vendor_name( $id ){
        $vendor = get_wcmp_vendor( $id );
        return $vendor ? $vendor->user_data->data->display_name : '';
    }

    public function mark_as_shipped( $order_id, $vendor_id = null, $package_id = null ) {
        $vendor = get_wcmp_vendor( $vendor_id );

        $tracking_id  = wc_get_order_item_meta( $package_id, 'correios_tracking_code' );
        $tracking_url = wc_get_order_item_meta( $package_id, 'correios_tracking_url' );

        $vendor->set_order_shipped( $order_id, $tracking_id, $tracking_url );
    }

    /**
     * Distributes the shipping dues accourding to their vendors
     * @param  int $order_id the id of the WooCommerce order
     * @return void
     * @todo deprecate it
     */
    public function process_shipping_due( $vendor_due, $order, $vendor_term_id ){

        global $wpdb;

        $order_id = $order->get_id();

        $packages = $this->split_packages_per_vendor( $order_id );

        $vendor = get_wcmp_vendor_by_term( $vendor_term_id );

        $vendor_shipping_totals = array_filter( $packages, function( $package ) use ( $vendor ){
            return $vendor->id == $package['vendor_id'];
        } );

        $vendor_shipping_totals = array_sum( wp_list_pluck( $vendor_shipping_totals, 'cost' ) );

        $vendor_due['shipping'] = $vendor_shipping_totals;

        $prepare = $wpdb->prepare( "UPDATE `{$wpdb->prefix}wcmp_vendor_orders`
                                    SET shipping = %.2f
                                    WHERE order_id = %d
                                    AND vendor_id = %d",
                                    0,
                                    $order_id,
                                    $vendor->id
                                );


        $update = $wpdb->query( $prepare );

        $prepare = $wpdb->prepare( "UPDATE `{$wpdb->prefix}wcmp_vendor_orders`
                                    SET shipping = %.2f
                                    WHERE order_id = %d
                                    AND vendor_id = %d
                                    LIMIT 1",
                                    $vendor_shipping_totals,
                                    $order_id,
                                    $vendor->id
                                );


        $update = $wpdb->query( $prepare );

        return $vendor_due;

    }


    public function get_order_comission_by_vendor( $order_id, $vendor_id ){
        return 0;
    }

    /**
     * Removing addition of WTF vendor_id meta
     * @return void
     */
    public function override_hooks(){
        global $WCMp;
        add_action( 'woocommerce_checkout_create_order_shipping_item', array( $this, 'add_package_qty' ), 99, 3 );
        remove_action( 'woocommerce_checkout_create_order_shipping_item',
                        array( $WCMp->frontend, 'add_meta_date_in_shipping_package' ), 10 );
    }

    public function add_package_qty( $item, $package_key, $package ){
        $package_qty = array_sum( wp_list_pluck( $package['contents'], 'quantity' ) );
        $item->add_meta_data( 'package_qty', $package_qty, true );
        do_action( 'wcmp_add_shipping_package_meta_data' );
    }

    /**
     * Add shipping items to suborders
     * @param int $vendor_order_id
     * @param int $args
     */
    public function add_methods_to_suborders( $vendor_order_id, $args ){

        $vendor_order = wc_get_order( $vendor_order_id );


        $native_packages_ids = wc_get_order( $vendor_order_id )->get_shipping_methods();

        foreach( array_keys( $native_packages_ids ) as $item ){
            $vendor_order->remove_item( $item );
            $vendor_order->save();
        }

        $parent_order_id = $args['order_id'];
        $parent_order = wc_get_order( $parent_order_id );
        $vendor_id = $args['vendor_id'];

        $packages = wc_get_order( $parent_order_id )->get_shipping_methods();

        $vendor_packages = array_filter( $packages, function( $package ) use ( $vendor_id ){
            return ( int ) $package->get_meta( 'vendor_id' ) === ( int ) $vendor_id;
        } );

        foreach( $vendor_packages as $shipping_rate ){

            $item = new WC_Order_Item_Shipping();

            $item->set_props(
                    array(
                        'method_title' => $shipping_rate->get_method_title(),
                        'method_id'    => $shipping_rate->get_method_id(),
                        'instance_id'  => $shipping_rate->get_instance_id(),
                        'total'        => $shipping_rate->get_total(),
                        'taxes'        => $shipping_rate->get_taxes(),
                    )
            );


            foreach ( $shipping_rate->get_meta_data() as $meta_data ) {
                $item->add_meta_data( $meta_data->key, $meta_data->value, true );
            }

            /**
             * Action hook to adjust item before save.
             *
             * @since 3.4.0
             */
            do_action( 'wcmp_vendor_create_order_shipping_item', $item, $vendor_id, $shipping_rate, $parent_order );

            $vendor_order->add_item( $item );

            $vendor_order->save();

        }

    }


    public function override_tracking_code_fields( $order ){

        $current_order_id = $order->ID;

        $order = wc_get_order();
        $is_suborder = false;

        if( $parent_order_id = $order->get_parent_id() ){
            $is_suborder = true;
            $order = wc_get_order( $parent_order_id );
        }

        $shipping_items = $order->get_shipping_methods();

        if( is_user_wcmp_vendor( get_current_user_id() ) || ( current_user_can( 'administrator' ) && $is_suborder ) ){

            $vendor_id = is_user_wcmp_vendor( get_current_user_id() )
                         ? get_current_user_id()
                         : get_post_meta( $current_order_id, '_vendor_id', true );


            $shipping_items = array_filter( $shipping_items, function( $package ) use( $vendor_id ) {
                return ( int ) $package->get_meta( 'vendor_id' ) === ( int ) $vendor_id;
            } );
        }

        if( 1 > count( $shipping_items ) ) {
            _e( '<i>None of the methods require tracking code.</i>', 'arti-marketplace-correios' );
            return;
        }

        $vendor_provider = arti_wcvc_current_provider();

        include_once ARTI_WCVC_DIR . '/includes/admin/html-meta-box-tracking-code.php';

    }

    /**
     * Adds field to WC Marketplace' product shipping on dashboard
     * @param  int $product_id
     * @return void
     */
    public function disable_correios_field( $fields, $product_id ){

        $correios_disabled = get_post_meta( $product_id, '_arti_shipping_correios_disabled', true );

        $correios_disabled = $correios_disabled === 'yes'?'yes':'no';

        $fields["_arti_shipping_correios_disabled"] = array(
            'label'       => __( 'Disable Correios shipping for this product', 'arti-marketplace-correios' ),
            'type'        => 'checkbox',
            'value'       => $correios_disabled,
            'dfvalue'     => 'yes',
            'class'       => 'regular-checkbox pro_ele simple variable external grouped',
            'label_class' => 'pro_title checkbox_title' );


        return $fields;
    }


    /**
     * Save metadata for product coming from the front-end
     * @param  int $product_id
     * @return void
     */
    public function save_product_meta( $product_id, $product_manager_form_data, $pro_attributes ){

        if( isset( $product_manager_form_data['_arti_shipping_correios_disabled'] ) ) {
            $_POST['_arti_shipping_correios_disabled'] = true;
        }

        arti_wcvc_disable_methods_save( $product_id );
    }

    public function override_shipping_action_html_by_order( $actions, $order ){

        $this->override_shipping_action_html( $actions, $order->get_id() );

        $actions['action'] = $this->shipping_link;

        return $actions;

    }

    public function override_shipping_action_html( $actions, $order_id ){

        $mark_ship_icon = 'ico-shippingnew-icon action-icon';
        $mark_ship_title = __( 'Add tracking info', '' );

        $this->shipping_link = sprintf( '<a href="javascript:void( 0 )" title="%1$s" onclick="populate_modal( this,%2$d )"><i class="wcmp-font %3$s"></i></a>', $mark_ship_title, ( int ) $order_id, $mark_ship_icon );

        add_filter( 'wcmp_vendor_orders_row_action_html', array( $this, 'add_shipping_link' ), 10, 2 );

        unset( $actions['mark_ship'] );

        if( wc_get_order( $order_id )->get_parent_id() ){
            $parent_order_id = wc_get_order( $order_id )->get_parent_id();
        }

        $package_per_vendor = arti_wcvc_current_provider()->get_package_by_vendor( $parent_order_id, get_current_vendor_id() );

        $tracking_info = array();

        foreach( $package_per_vendor as $package_id => $item ){

            $tracking_url  = wc_get_order_item_meta( $package_id, 'correios_tracking_url' );
            $tracking_code = wc_get_order_item_meta( $package_id, 'correios_tracking_code' );
            $method_label  = wc_get_order_item_meta( $package_id, 'method_label' );
            $package_data  = ( new WC_Order_Item_Shipping( $package_id ) )->get_data();

            $tracking_info[$package_id] = array(
                'url'          => $tracking_url,
                'method_title' => $package_data['method_title'],
                'method_label' => $method_label,
                'is_correios'  => arti_wcvc_is_correios_method( $method_label ),
                'description'  => __( '<i>Ex.: AA123456789BR</i>', 'arti-marketplace-correios' ),
                'value'        => $tracking_code
            );

        }
        $tracking_info_json = json_encode( $tracking_info );

        $this->shipping_link .= "<input type='hidden' class='tracking-info' id='tracking_info_{$order_id}' value='$tracking_info_json'>";

        return $actions;

    }

    public function add_shipping_link( $action_html, $actions ) {

        $action_html .= $this->shipping_link;

        remove_filter( 'wcmp_vendor_orders_row_action_html', array( $this, 'add_shipping_link' ), 10 );

        return $action_html;
    }

    /**
     * Will add fields to user profile in admin
     * @param WP_User $user
     * @return void
     */
    public function add_vendor_fields_admin( $fields, $vendor_id ){

        if( !is_admin() || arti_wcvc_is_corporate_code_disabled() ) {
            return $fields;
        }

        $correios_login = get_user_meta( $vendor_id, '_correios_login', true );
        $correios_password = get_user_meta( $vendor_id, '_correios_password', true );

        $login_field = array(
            'id' => '_correios_login',
            'name' => '_correios_login',
            'label' => __( 'Correios Login', 'arti-marketplace-correios' ),
            'type' => 'text',
            'value' => $correios_login,
            'class' => "user-profile-fields regular-text",
            'in_table' => 1

        );
        $password_field = array(
            'id' => '_correios_password',
            'name' => '_correios_password',
            'label' => __( 'Correios Password', 'arti-marketplace-correios' ),
            'type' => 'password',
            'value' => $correios_password,
            'class' => "user-profile-fields regular-text",
            'in_table' => 1
        );

        $fields_factory = new WCMp_WP_Fields;

        $fields_factory->text_input( $login_field );
        $fields_factory->text_input( $password_field );

        return $fields;
    }

    /**
     * Add admin field to disable Correios store-wide
     * @param WP_User $user
     * @return void
     */
    public function add_disable_correios_vendor_fields_admin( $fields, $vendor_id ){

        $correios_disabled = get_user_meta( $vendor_id, '_arti_shipping_correios_disabled', true );

        $fields['_arti_shipping_correios_disabled'] = array (
            'label' => __( 'Disable Correios Shipping store-wide', 'arti-marketplace-correios' ),
            'type' => 'checkbox',
            'dfvalue' => $correios_disabled,
            'value' => 'yes',
            'class' => 'user-profile-fields',
        );

        return $fields;

    }

    /**
     * Will add fields to user dashborad in th font-end
     * @return array
     */
    public function add_vendor_fields( $fields ){

        $vendor = get_wcmp_vendor( get_current_vendor_id() );

        if( !$vendor || arti_wcvc_is_corporate_code_disabled() ){
            return;
        }

        $vendor_id = $vendor->id;

        $correios_login = get_user_meta( $vendor_id, '_correios_login', true );
        $correios_password = get_user_meta( $vendor_id, '_correios_password', true );

        $login_field = array(
            'id' => '_correios_login',
            'name' => '_correios_login',
            'label' => __( 'Correios Login', 'arti-marketplace-correios' ),
            'type' => 'text',
            'value' => $correios_login,
            'class' => "user-profile-fields regular-text"

        );
        $password_field = array(
            'id' => '_correios_password',
            'name' => '_correios_password',
            'label' => __( 'Correios Password', 'arti-marketplace-correios' ),
            'type' => 'password',
            'value' => $correios_password,
            'class' => "user-profile-fields regular-text",
        );
        include_once ARTI_WCMP_DIR . '/views/html-corporate-correios-fields.php';

    }

    /**
     * Add front-end field to disable Correios store-wide
     * @return void
     */
    public function add_disable_correios_vendor_fields(){

        $vendor = get_wcmp_vendor( get_current_vendor_id() );

        $correios_disabled = get_user_meta( $vendor->id, '_arti_shipping_correios_disabled', true );

        ( new WCMp_Frontend_WP_Fields )->checkbox_input( array(
            'id' => '_arti_shipping_correios_disabled',
            'name' => '_arti_shipping_correios_disabled',
            'label' => __( 'Disable Correios Shipping store-wide', 'arti-marketplace-correios' ),
            'value' => 'yes',
            'dfvalue' => $correios_disabled,
            'class' => "user-profile-fields regular-text"

        ) );
    }


    /**
     * Save the corporate info
     * @param  int|string $vendor_id
     * @return void
     */
    public function save_vendor_fields( $vendor_id ){

        if ( $_SERVER['REQUEST_METHOD'] != 'POST' ) {
            return;
        }

        $vendor_id = $vendor_id  ? $vendor_id : get_current_vendor_id();

        $correios_disabled = 'no';

        if( isset($_POST['_arti_shipping_correios_disabled']) ) {
            $correios_disabled = 'yes';
        }

        update_user_meta( $vendor_id, '_arti_shipping_correios_disabled', $correios_disabled );

        if ( ( current_user_can( 'edit_user', $vendor_id ) || get_wcmp_vendor( get_current_vendor_id() ) )
             && isset( $_POST['_correios_login'] ) ) {

            update_user_meta( $vendor_id, '_correios_login', sanitize_text_field( $_POST['_correios_login'] ) );
            update_user_meta( $vendor_id, '_correios_password', sanitize_text_field( $_POST['_correios_password'] ) );

        }

    }

    /**
     * Will add tracking fields temaplate to front-end modal
     */
    public function add_tracking_fields_template(){
        include_once ARTI_WCMP_DIR . '/views/html-tmpl-tracking-fields.php';
    }
    public function update_tracking_info( $widget ){

        // Keep from marking as shipped if "other methods" fields empty
        if ( isset( $_POST['wcmp-submit-mark-as-ship'] ) ) {
            unset( $_POST['wcmp-submit-mark-as-ship'] );
        }

        return $widget;

    }

}
