<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly.
}
/**
 * @author  Luis Eduardo Braschi <http://art-idesenvolvimento.com.br>
 */

/**
 * Hiding meta info from client
 * @param  array $items list of meta items to hide
 * @return array        list of meta items to hide
 */
function arti_wcvc_hide_meta_shipping($items){

    $items[] = 'correios_tracking_code';
    $items[] = 'correios_tracking_url';
    $items[] = 'method_label';
    $items[] = 'vendor_id';
    $items[] = 'vendor_name';
    $items[] = 'seller_id';

    return $items;
}
add_filter( 'woocommerce_hidden_order_itemmeta', 'arti_wcvc_hide_meta_shipping');

/**
 * Overriding the order metaboxes
 * @return void
 */
function arti_wcvc_override_order_meta_boxes(){
    add_meta_box(
        'wc_correios',
        __('Tracking codes', 'arti-marketplace-correios'),
        'arti_wcvc_render_tracking_code_fields',
        'shop_order',
        'side',
        'default'
    );
}
add_action( 'add_meta_boxes', 'arti_wcvc_override_order_meta_boxes', 11 );

/**
 * Display tracking code fields
 * @param  WP_Post $post
 * @return void
 */
function arti_wcvc_render_tracking_code_fields($post){

    $order = new WC_Order($post->ID);
    $shipping_items = $order->get_items( 'shipping' );

    if(1 > count($shipping_items)) {
        _e('<i>None of the methods require tracking code.</i>', 'arti-marketplace-correios');
        return;
    }

    $vendor_provider = arti_wcvc_current_provider();

    include_once 'admin/html-meta-box-tracking-code.php';

}

/**
 * Splits the tracking code in the format "###,###,###" and register each one to their
 * shipping item correspondence
 * @param  int $order_id         bypassed
 * @param  string $order
 * @return void
 */
function arti_wcvc_save_admin_tracking_code($order_id, $order){

    $nonce = sanitize_text_field( wp_unslash( $_POST['woocommerce_meta_nonce'] ) );

    if ( empty( $_POST['woocommerce_meta_nonce'] ) ||
        ! wp_verify_nonce( $nonce, 'woocommerce_save_data' ) ||
        ! isset($_POST['correios_tracking_codes'])
    ) {
        return;
    }

    $tracking_codes = $_POST['correios_tracking_codes'];
    $tracking_urls = isset($_POST['correios_tracking_urls'])?$_POST['correios_tracking_urls']:array();

    foreach ($tracking_codes as $package_id => $tracking_code) {

        $tracking_url = isset($tracking_urls[$package_id])?$tracking_urls[$package_id]:'';
        arti_wcvc_add_tracking_code_to_package($order_id, $package_id, $tracking_code, $tracking_url);

    }

}
add_action( 'woocommerce_process_shop_order_meta', 'arti_wcvc_save_admin_tracking_code', 20, 2 );

/**
 * Register tracking code provided by the vendor
 * @return void
 */
function arti_wcvc_save_vendor_tracking_code(){

    $nonce = isset($_REQUEST['_wpnonce'])?$_REQUEST['_wpnonce']:'';
    if(!wp_verify_nonce( $nonce, 'track-shipment' )){
        return;
    }

    if(isset($_POST['correios_tracking_code'])){

        $tracking_codes = $_POST['correios_tracking_code'];
        $correios_tracking_urls = isset($_POST['correios_tracking_url'])?$_POST['correios_tracking_url']:'';
        $order_id = (int) $_POST['order_id'];

        foreach($tracking_codes as $package_id => $tracking_code){
            $correios_tracking_url = isset($correios_tracking_urls[$package_id])?$correios_tracking_urls[$package_id]:'';
            arti_wcvc_add_tracking_code_to_package($order_id, $package_id, $tracking_code, $correios_tracking_url);
        }

    }

}
add_action( 'init', 'arti_wcvc_save_vendor_tracking_code' );
/**
 * Will add or update tracking info to the package
 * @param  int $order_id
 * @param  int $package_id
 * @param  string $tracking_code
 * @param  string $tracking_url
 * @return bool                Whether tracking info has been updated
 */
function arti_wcvc_add_tracking_code_to_package($order_id, $package_id, $tracking_code, $tracking_url = ''){

    $old_code = wc_get_order_item_meta($package_id, 'correios_tracking_code');
    $old_url  = wc_get_order_item_meta($package_id, 'correios_tracking_url');

    if(
        (empty($old_code) && empty($tracking_code)) ||
        (($old_code == $tracking_code) && ($old_url == $tracking_url))
    ){
        return false;
    }

    $method_label = wc_get_order_item_meta($package_id, 'method_label');
    $is_correios = in_array($method_label, arti_wcvc_list_shipping_methods());
    $vendor_id = wc_get_order_item_meta($package_id, 'vendor_id');

    $updated = false;
    $updated = wc_update_order_item_meta($package_id, 'correios_tracking_code', $tracking_code);

    $url_updated = false;

    if(!empty($tracking_url)) {
        if(filter_var($tracking_url, FILTER_VALIDATE_URL)) {
            /**
             * @todo change correios_tracking_url to arti_wcvc_tracking_url
             */
            $url_updated = wc_update_order_item_meta($package_id, 'correios_tracking_url', $tracking_url);
        } else {
            /**
             * Allows for notices in the admin through redirects
             */
            set_transient('arti-wcvc-invalid-url', $tracking_url, 10);

            /**
             * Hook for displaying error messages etc.
             * @param string $tracking_url The offending URL
             */
            do_action('arti_wcvc_invalid_tracking_url', $tracking_url);
        }
    }

    if($updated && $is_correios){
        // this one is used by WooCommerce Correios
        wc_correios_update_tracking_code($order_id, $tracking_code);
    } elseif($updated || $url_updated) {
        arti_wcvc_current_provider()->mark_as_shipped($order_id, $vendor_id, $package_id);
    }

    if($updated && !is_admin()) {
        $message = __('Success. Your tracking number has been updated.', 'arti-marketplace-correios');
        wc_add_notice($message, 'success');
    }

    /**
     * Executed before returning the "$updated" value.
     * Can be used to show messages, triggering e-mails etc.
     *
     * @param bool $updated
     * @param int $order_id
     * @param int $vendor_id
     * @param int $package_id
     */
    do_action('arti_wcvc_add_tracking_code_to_package', $updated, $order_id, $vendor_id, $package_id);

    return $updated;

}

/**
 * Show admin notices through redirects
 * @return void
 */
function arti_wcvc_admin_notices(){

    if($tracking_url = get_transient('arti-wcvc-invalid-url')) {

        $message = sprintf(__('Invalid URL (%s). You should provide a valid URL.', 'arti-marketplace-correios'),
                            $tracking_url);
        $notice_type = 'error';

        include 'admin/html-notice.php';

        delete_transient('arti-wcvc-invalid-url');
    }

}

add_action('admin_notices', 'arti_wcvc_admin_notices');
