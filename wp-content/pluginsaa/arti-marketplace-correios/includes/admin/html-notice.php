<div class="notice notice-<?php echo esc_attr($notice_type) ?> is-dismissible">
    <p><?php echo esc_html($message) ?></p>
    <button type="button" class="notice-dismiss">
        <span class="screen-reader-text"><?php _e('Dismiss this notice.', 'arti-marketplace-correios')?></span>
    </button>
</div>
