<h2><?php esc_html_e( 'Correios Corporate Code', 'arti-marketplace-correios' ); ?></h2>
<table class="form-table">
    <tr>
        <th><?php esc_html_e( 'Correios Login', 'arti-marketplace-correios' ); ?></th>
        <td>
            <input type="text" name="_correios_login" class="regular-text" value="<?php echo esc_attr( $correios_login ) ?>">
        </td>
    </tr>

    <tr>
        <th><?php esc_html_e( 'Correios Password', 'arti-marketplace-correios' ); ?></th>
        <td>
            <input type="password" name="_correios_password" class="regular-text" placeholder="<?php echo $password_placeholder;?>">
        </td>
    </tr>
</table>
