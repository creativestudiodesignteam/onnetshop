<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly.
}

/**
 * Adds disable Correios field to product
 * @return void
 */
function arti_wcvc_disable_correios_product(){
    include_once 'admin/html-disable-correios-product.php';
}
add_action( 'woocommerce_product_options_shipping', 'arti_wcvc_disable_correios_product');

/**
 * Saves the disabled methods
 * @return void
 */
function arti_wcvc_disable_methods_save($product_id){
    $correios_disabled = isset($_POST['_arti_shipping_correios_disabled']) ? 'yes' : 'no';
    update_post_meta($product_id, '_arti_shipping_correios_disabled', $correios_disabled);

    $product = wc_get_product($product_id);

    // change in all variations
    // todo: make it for each variation
    if( $product->has_child() ) {

        $children = $product->get_children();

        foreach( $children as $child_id ) {
            update_post_meta($child_id, '_arti_shipping_correios_disabled', $correios_disabled);
        }

    }

}
add_action( 'woocommerce_process_product_meta', 'arti_wcvc_disable_methods_save');

/**
 * Quick edit
 */
/**
 * Adds field to quick edit
 * @return void
 */
function arti_wcvc_quick_edit_disabe_correios(){
    include_once 'admin/html-quick-edit-disable-correios.php';
}
add_action( 'woocommerce_product_quick_edit_end', 'arti_wcvc_quick_edit_disabe_correios');

function arti_wcvc_quick_edit_disabe_correios_save($product){
    arti_wcvc_disable_methods_save($product->get_id());
}
add_action( 'woocommerce_product_quick_edit_save', 'arti_wcvc_quick_edit_disabe_correios_save');

/**
 * Assign value for quick edit data
 *
 * @param array $column
 * @param integer $post_id
 *
 * @return void
 */
function arti_quick_edit_data($column, $post_id) {
    if('name' === $column) {
        include 'admin/html-quick-edit-data.php';
    }
}
add_action( 'manage_product_posts_custom_column', 'arti_quick_edit_data', 99, 2 );
