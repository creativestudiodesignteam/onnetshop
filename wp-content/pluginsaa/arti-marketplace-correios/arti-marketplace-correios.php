<?php
/**
 * Plugin Name: WooCommerce Marketplace/Correios
 * Description: WooCommerce - Marketplace - Correios integration
 * Author: Luis Eduardo Braschi
 * Author URI: Art-Idesenvolvimento.com.br
 *
 * Version: 1.4.2
 * Requires at least:    4.8.0
 * Tested up to:         5.2.4
 * WC requires at least: 3.2.0
 * WC tested up to:      3.7.1
 *
 * Text Domain: arti-marketplace-correios
 * Slug: arti-marketplace-correios
 * Prefix: arti_wcvc
 * Domain Path: /languages
 * License: GPL2
 */

if (! defined('ABSPATH')) {
	exit; // Exit if accessed directly.
}

define('ARTI_WCVC_PREFIX', 'arti_wcvc_');
define('ARTI_WCVC_SLUG', 'arti-marketplace-correios');
define('ARTI_WCVC_BASENAME', plugin_basename(__FILE__));
define('ARTI_WCVC_DIR', dirname(__FILE__));
define('ARTI_WCVC_JS', plugin_dir_url(__FILE__) . '/assets/js');

register_activation_hook(__FILE__, 'arti_wcvc_activate');
/**
 *  Plugin activation hook
 */
function arti_wcvc_activate() {

	arti_wcvc_load_plugin_textdomain();

	/**
	 *  Requires WooCommerce and WooCommerce Correios to be installed and active
	 */
	if (!arti_wcvc_dependencies_running()) {

		deactivate_plugins(ARTI_WCVC_BASENAME);
		ob_start();
		?>
		<p>
			<?php _e('WooCommerce Marketplace/Correios requires WooCommerce, Correios and a marketplace provider to run. Please, install those plugins and try again', 'arti-marketplace-correios');?>
		</p>
		<?php if(php_sapi_name() != 'cli'):?>
		<p>
			<a href='<?=admin_url('plugins.php')?>' tile=''>
				<?php _e('Return to Plugins page', 'arti-marketplace-correios')?>
			</a>
		</p>
		<?php endif;

		$result = ob_get_clean();

		// checking for WP-cli calls
		echo (php_sapi_name() == 'cli') ? preg_replace('/\s+/S', " ",strip_tags($result)) : $result;

		wp_die();

	}

}

function arti_wcvc_dependencies_running() {
	return apply_filters('arti_wcvc_dependencies_running', class_exists('WooCommerce') && class_exists('WC_Correios'));
}

add_action('plugins_loaded', 'arti_wcvc_loadplugin', 11);

function arti_wcvc_loadplugin(){

	if(arti_wcvc_dependencies_running()) {
		include_once ARTI_WCVC_DIR . '/includes/functions.php';
		include_once ARTI_WCVC_DIR . '/includes/classes/class-provider-config.php';

		/**
		 * Will allow authors to add providers in the future
		 * @var array
		 */
		$providers = arti_wcvc_get_providers_list();
		$vendor_provider_option = get_option('arti_wcvc_vendor_provider');

		if($vendor_provider_option && isset($providers[$vendor_provider_option])) {
			$provider_config = $providers[$vendor_provider_option];

			include_once ARTI_WCVC_DIR . '/includes/classes/interface-shipping-processor.php';
			include_once ARTI_WCVC_DIR . "/includes/classes/abstract-provider.php";

			$provider_package = ARTI_WCVC_DIR . "/includes/providers/{$provider_config->get_slug()}/bootstrap.php";

			include_once apply_filters('arti_wcvc_provider_package_path', $provider_package, $provider_config);

			include_once ARTI_WCVC_DIR . '/includes/classes/class-vendor-provider-factory.php';

			/**
			 * @todo Move this elsewhere?
			 */
			if(Arti_Vendor_Provider_Factory::load_provider($provider_config->get_class())){
				do_action('arti_wcvc_provider_loaded');
				do_action("arti_wcvc_provider_{$provider_config->get_slug()}_loaded");
			}

			if(Arti_Vendor_Provider_Factory::$current_provider->is_active) {
				include_once ARTI_WCVC_DIR . '/includes/package-processing.php';
				include_once ARTI_WCVC_DIR . '/includes/tracking-code-processing.php';
				include_once ARTI_WCVC_DIR . '/includes/product-shipping-options.php';
				include_once ARTI_WCVC_DIR . '/includes/classes/class-protopackage-splitting.php';
				include_once ARTI_WCVC_DIR . '/includes/woocommerce-hooks.php';
				include_once ARTI_WCVC_DIR . '/includes/dependencies-hooks.php';
			}

		}

		include_once ARTI_WCVC_DIR . '/includes/options.php';

	} else {
		add_action('admin_notices', 'arti_wcvc_plugin_not_loaded');
	}

}

add_action('init', 'arti_wcvc_load_plugin_textdomain', 10);

function arti_wcvc_plugin_not_loaded(){

	$notice_type = 'warning';
	$message = __('WooCommerce Marketplace/Correios requires WooCommerce, Correios and a marketplace provider to run. Please, install those plugins and try again', 'arti-marketplace-correios');

	include ARTI_WCVC_DIR . '/includes/admin/html-notice.php';

}

/**
 * Loading text domain
 */
function arti_wcvc_load_plugin_textdomain() {
	load_plugin_textdomain(ARTI_WCVC_SLUG, false, dirname(ARTI_WCVC_BASENAME) . '/languages');
}
