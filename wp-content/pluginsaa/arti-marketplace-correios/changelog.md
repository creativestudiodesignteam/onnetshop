# Change log

## Version 1.4.0 - 22/10/2019
* [tweak] Hability to edit calculator's package names
* [tweak] Corporate fields now available for everyone
* [tweak] It's now possible to disable Correios for the whole store
* [tweak] Made tracking code system more modular for WC Marketplace
* [tweak] Changing "one method to all" to grouping all packages in one
* [fix] Mark shipping tracking modal not showing in WC Marketplace vendor dashboard
* [fix] Translation in settings page

## Version 1.3.7 - 11/09/2019
* [fix] Fix WC Marketplace split orders support

## Version 1.3.6 - 16/07/2019
* [fix] Fixing Dokan provider class

## Version 1.3.5 - 16/07/2019
* [fix] Error from Fernando Acosta's "Simulador de frete"; changed "product" type from WC_Product to array

## Version 1.3.4 - 14/05/2019
* [tweak] More verbose settings page
* [fix] Fixed how Correios tracking codes were added, now avoinding duplicates
* [fix] PHP warning if user didn't have chosen a shipping method and "one to rule them all" was on

## Version 1.3.3 - 09/05/2019
* Updating "Tested up to"
* [fix] Bug that caused fatal error in shipping calculator
* [tweak] Changing hooks file to be loaded later

## Version 1.3.2 - 07/02/2019
* [tweak] Allow simple and variable products to have Correios shipping disabled

## Version 1.3.0 - 15/09/2018
* [fix] Better shop name in cart/checkout handling
* [tweak] Adding Shipping Simulator support

## Version 1.2.1 - 01/07/2018
* [fix] Fixing fatal error during checkout

## Version 1.2.0 - 25/05/2018
* [tweak] Adding corporate code support
* [tweak] Using new Dokan filter to overcome earnings handling in the front-end

## Version 1.1.3 - 19/03/2018
* [fix] "Uncaught exception" error in "class-dokan.php"
* [tweak] Fixing and improving tracking code handling
* Updating translation files

## Version 1.1.2 - 17/03/2018
* [fix] Wrong package index/vendor_id handling by WCMp
* [tweak] Adding user data debug
* [tweak] Adding debug logging
* [tweak] Adding Dokan dimensions template

## Version 1.1.1 - 21/02/2018
* [fix] Tracking code not showing up in admin

## Version 1.1.0 - 20/02/2018
* [fix] Dokan poor package management
* Removing old useless code in favour of new WC Correios hooks

## Version 1.0.1 - 31/01/2018
* [tweak] Adding Dokan support

## Version 0.2.5 - 17/12/2018
* [tweak] Adding "Vendors' names on front end" configuration.
* [tweak] Changed order of options to prioritise important features.
* [tweak] Code enhancement.
* [tweak] Changing this very changelog's syntax.

## Version 0.2.4 - 21/11/2017
* [fix] Fixes shipping not showing up in vendor dashboard.
