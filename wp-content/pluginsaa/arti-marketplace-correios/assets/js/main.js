jQuery(function($){
    $('#the-list').on('click', '.editinline', function(){
        /**
         * Extract metadata and put it as the value for the custom field form
         */
        var post_id = $(this).closest('tr').attr('id');

        post_id = post_id.replace("post-", "");

        var correios_disabled = $('#arti_shipping_correios_disabled_inline_' + post_id).find('#correios_disabled').text();

        if ( 'yes' === correios_disabled ) {
            $( 'input[name="_arti_shipping_correios_disabled"]', '.inline-edit-row' ).attr( 'checked', 'checked' );
        } else {
            $( 'input[name="_arti_shipping_correios_disabled"]', '.inline-edit-row' ).removeAttr( 'checked' );
        }
    });
});
