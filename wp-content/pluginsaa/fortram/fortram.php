<?php
/**
* Plugin Name: Fortram
* Plugin URI: https://fortram.com.br/
* Description: Fortram
* Version: 1.0
* Author: Daniel Valim - Fortram
* Author URI: https://fortram.com.br/
**/

require_once(dirname(__FILE__) . '/vc_templates/templates.php');

include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
 
if ( is_plugin_active( 'js_composer/js_composer.php' ) ) {
    add_action( 'init', 'vc_before_init_actions' );
    
    function vc_before_init_actions() {

    include( plugin_dir_path( __FILE__ ) . 'vc_templates/element_wpbakery.php');
    
    }

}


require_once(dirname(__FILE__) . '/imoveis/main.php');
require_once(dirname(__FILE__) . '/servicos/main.php');
require_once(dirname(__FILE__) . '/usados/main.php');
require_once(dirname(__FILE__) . '/options.php');
require_once(dirname(__FILE__) . '/ocultar.php');

function titulo(){
    return wp_kses_post( get_the_title() );
}

function link_site(){
    return get_site_url();
}

add_shortcode('titulo','titulo');
add_shortcode('link_site','link_site');

//CUSTOM META

function __update_post_meta( $post_id, $field_name, $value = '' )
{
    if ( empty( $value ) OR ! $value )
    {
        delete_post_meta( $post_id, $field_name );
    }
    elseif ( ! get_post_meta( $post_id, $field_name ) )
    {
        add_post_meta( $post_id, $field_name, $value );
    }
    else
    {
        update_post_meta( $post_id, $field_name, $value );
    }
}

//SLICK

add_action( 'wp_enqueue_scripts', 'choosy_slick_slider_enqueue_scripts' );
function choosy_slick_slider_enqueue_scripts() {
    
	wp_enqueue_script( 'owl-js', 'https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js', '1.5.3', true );
	wp_enqueue_style( 'slick-css', '//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css', '1.0.0', true );
}

//MASK.JS

add_action('wp_enqueue_scripts', function () {
    wp_enqueue_script('jquery-mask-plugin', 'https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.min.js', array(), '1.14.15', true);
});
add_action('admin_enqueue_scripts', function () {
    wp_enqueue_script('jquery-mask-plugin', 'https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.min.js', array(), '1.14.15', true);
});


function js_mask_fortram(){
?>
<script>
jQuery(document).ready(function( $ ) {


$(".ftr_celular input").one("focus", function() {
    if($(this).val().length < 15) {
         $(this).mask('(00) 0000-00000');
    }
});

$(".ftr_celular input").keyup(function() {
    if($(this).val().length < 15) {
         $(this).mask('(00) 0000-00000');
    } else {
         $(this).mask('(00) 00000-0000');
    }
});

$(".campo_descricao textarea").attr('maxlength','250');

$(".campo_descricao textarea").on('keyup',function(){
    var max = 250;
    var len = $(this).val().length;
    if (len >= max) {
    $(".campo_descricao .description").html(' <span style="color:darkred;">0 caractere restante.<span>');
    } else {
    var char = max - len;
        if (char > 1){
            $(".campo_descricao .description").html(char + ' caracteres restantes.');
        }
        if (char <= 1){
            $(".campo_descricao .description").html(char + ' caractere restante.');
        }
    }
});

// $(document).ready(function(){
//     var max = 250;
//     var len = $(".campo_descricao textarea").val().length;
//     if (len >= max) {
//     $(".campo_descricao .description").html(' <span style="color:darkred;">0 caractere restante.<span>');
//     } else {
//     var char = max - len;
//         if (char > 1){
//             $(".campo_descricao .description").html(char + ' caracteres restantes.');
//         }
//         if (char <= 1){
//             $(".campo_descricao .description").html(char + ' caractere restante.');
//         }
//     }
// });

});
</script>
<style>
    .wrap_anuncios li a {
        font-size:16px !important;
        font-weight:bold;
        margin-right:10px;
    }
    .wrap_anuncios li {
        list-style: circle inside;
        margin-bottom:5px;
    }
    .wrap_anuncios li span {
        padding:0 5px;
    }
    .wrap_anuncios .link_btn {
        background:#FED700;
        color:black;
        font-weight:bold;
        font-size:15px;
        padding:10px 15px;
        border-radius:5px;
        margin:10px 0 0;
        display:inline-block;
        transition:.27s ease;
    }
</style>
<?php
}

add_action('wp_head','js_mask_fortram');
add_action('admin_head','js_mask_fortram');

add_image_size( 'onnet_1', 470, 355, true );


function getEstado($i){
    $estados = array(
        'AC'=>'Acre',
        'AL'=>'Alagoas',
        'AP'=>'Amapá',
        'AM'=>'Amazonas',
        'BA'=>'Bahia',
        'CE'=>'Ceará',
        'DF'=>'Distrito Federal',
        'ES'=>'Espírito Santo',
        'GO'=>'Goiás',
        'MA'=>'Maranhão',
        'MT'=>'Mato Grosso',
        'MS'=>'Mato Grosso do Sul',
        'MG'=>'Minas Gerais',
        'PA'=>'Pará',
        'PB'=>'Paraíba',
        'PR'=>'Paraná',
        'PE'=>'Pernambuco',
        'PI'=>'Piauí',
        'RJ'=>'Rio de Janeiro',
        'RN'=>'Rio Grande do Norte',
        'RS'=>'Rio Grande do Sul',
        'RO'=>'Rondônia',
        'RR'=>'Roraima',
        'SC'=>'Santa Catarina',
        'SP'=>'São Paulo',
        'SE'=>'Sergipe',
        'TO'=>'Tocantins'
    );
    return $estados[$i];
}

// Limit media library access
  
add_filter( 'ajax_query_attachments_args', 'wpb_show_current_user_attachments' );
 
function wpb_show_current_user_attachments( $query ) {
    $user_id = get_current_user_id();
    if ( $user_id && !current_user_can('activate_plugins') && !current_user_can('edit_others_posts
') ) {
        $query['author'] = $user_id;
    }
    return $query;
} 

add_action( 'show_user_profile', 'ftr_pagseguro_fields' );
add_action( 'edit_user_profile', 'ftr_pagseguro_fields' );

function ftr_pagseguro_fields( $user ) { ?>
    <h3><?php _e("Informações do PagSeguro", "blank"); ?></h3>

    <table class="form-table">
    <tr>
        <th><label for="ftr_email_pagseguro"><?php _e("E-mail PagSeguro"); ?></label></th>
        <td>
            <input type="text" name="ftr_email_pagseguro" id="ftr_email_pagseguro" value="<?php echo esc_attr( get_the_author_meta( 'ftr_email_pagseguro', $user->ID ) ); ?>" class="regular-text" /><br />
            <span class="description"><?php _e("E-mail da sua conta do PagSeguro."); ?></span>
        </td>
    </tr>
    <tr>
        <th><label for="ftr_token_pagseguro"><?php _e("Token PagSeguro"); ?></label></th>
        <td>
            <input type="text" name="ftr_token_pagseguro" id="ftr_token_pagseguro" value="<?php echo esc_attr( get_the_author_meta( 'ftr_token_pagseguro', $user->ID ) ); ?>" class="regular-text" /><br />
            <span class="description"><?php _e("Token do PagSeguro."); ?></span>
        </td>
    </tr>
    </table>
<?php }

add_action( 'personal_options_update', 'ftr_pagseguro_fields_handler' );
add_action( 'edit_user_profile_update', 'ftr_pagseguro_fields_handler' );

function ftr_pagseguro_fields_handler( $user_id ) {
    if ( !current_user_can( 'edit_user', $user_id ) ) { 
        return false; 
    }
    update_user_meta( $user_id, 'ftr_email_pagseguro', $_POST['ftr_email_pagseguro'] );
    update_user_meta( $user_id, 'ftr_token_pagseguro', $_POST['ftr_token_pagseguro'] );
}

//AUTHOR ID BY POST ID

function authorByPostID($idp){
    $author_id = get_post_field ('post_author', $idp);
    return $author_id;
}

function getPostType($idp){
    $author_id = get_post_field ('post_type', $idp);
    return $author_id;
}