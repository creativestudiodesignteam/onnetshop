<?php
function updateCountServicos(){

    $users = get_users( array( 'fields' => array( 'ID' ) ) );
    $options = get_option( 'ftr_settings' );
    $id_p = $options['ftr_id_servico'];
    // loop trough each author
    foreach ($users as $user)
    {
        update_user_meta( $user->ID, 'qtde_servicos_comprados', intval( countComprados($id_p, $user->ID) ) );

        //POSTADOS
        $args = array(
            'post_type' => 'servico',
            'author'    => $user->ID,
            'post_status' => array('publish', 'pending', 'draft', 'auto-draft', 'future', 'private', 'inherit', 'trash'),
            'posts_per_page' => -1
        );
    
        $query = new WP_Query($args);
    
        $count = $query->found_posts;
        update_user_meta( $user->ID, 'qtde_servicos_postados', intval( $count ) );
    }

}

add_action('wp_head','updateCountServicos');
add_action('admin_head','updateCountServicos');