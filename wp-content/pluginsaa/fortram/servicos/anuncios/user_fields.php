<?php
//CUSTOM USER FIELDS

add_action( 'show_user_profile', 'user_fields_servicos' );
add_action( 'edit_user_profile', 'user_fields_servicos' );

function user_fields_servicos( $user ) {
	$qtde_postados = get_the_author_meta( 'qtde_servicos_postados', $user->ID );
	$qtde_comprados = get_the_author_meta( 'qtde_servicos_comprados', $user->ID );
	?>
	<h3><?php esc_html_e( 'Anúncios de Serviços', 'fortram' ); ?></h3>

	<table class="form-table">
		<tr>
			<th><label for="qtde_servicos"><?php esc_html_e( 'Anúncios de Serviços Comprados', 'fortram' ); ?></label></th>
			<td>
				<input type="number"
			       min="0"
			       max="999"
			       step="1"
			       id="qtde_servicos_comprados"
			       name="qtde_servicos_comprados"
			       value="<?php echo esc_attr( $qtde_comprados ); ?>"
			       class="regular-text"
			       disabled
				/>
			</td>
		</tr>
		<tr>
			<th><label for="qtde_servicos"><?php esc_html_e( 'Anúncios de Serviços Postados', 'fortram' ); ?></label></th>
			<td>
				<input type="number"
			       min="0"
			       max="999"
			       step="1"
			       id="qtde_servicos_postados"
			       name="qtde_servicos_postados"
			       value="<?php echo esc_attr( $qtde_postados ); ?>"
			       class="regular-text"
			       disabled
				/>
			</td>
		</tr>
	</table>
	<?php
}
// add_action( 'personal_options_update', 'update_user_fields_servicos' );
// add_action( 'edit_user_profile_update', 'update_user_fields_servicos' );

// function update_user_fields_servicos( $user_id ) {
// 	if ( ! current_user_can( 'edit_user', $user_id ) ) {
// 		return false;
// 	}

// 	if ( ! empty( $_POST['qtde_servicos_postados'] ) ) {
// 		update_user_meta( $user_id, 'qtde_servicos_postados', intval( $_POST['qtde_servicos_postados'] ) );
// 	}
// }