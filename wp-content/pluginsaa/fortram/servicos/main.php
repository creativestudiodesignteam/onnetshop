<?php

require_once(dirname(__FILE__) . '/post_type.php');
require_once(dirname(__FILE__) . '/front.php');
require_once(dirname(__FILE__) . '/busca.php');
require_once(dirname(__FILE__) . '/widget.php');

require_once(dirname(__FILE__) . '/anuncios/main.php');

function servicos_sidebar() {
    register_sidebar(
        array (
            'name' => __( 'Serviços', 'fortram' ),
            'id' => 'servicos_sidebar',
            'description' => __( 'Página Serviços', 'fortram' ),
            'before_widget' => '<div class="widget-content" style="margin-bottom:30px;">',
            'after_widget' => "</div>",
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>',
        )
    );
}
add_action( 'widgets_init', 'servicos_sidebar' );


//SELECT CAGEGORIAS

function select_categorias($i){
    $cats = array(
		'Alimentação' => esc_html__( 'Alimentação', 'fortram' ),
        'Animação de festas' => esc_html__( 'Animação de festas', 'fortram' ),
        'Ar Condicionado e Ventilação' => esc_html__( 'Ar Condicionado e Ventilação', 'fortram' ),
        'Bikeboy' => esc_html__( 'Bikeboy', 'fortram' ),
        'Cabeleireiro' => esc_html__( 'Cabeleireiro', 'fortram' ),
        'Carpinteiro' => esc_html__( 'Carpinteiro', 'fortram' ),
        'Carreto' => esc_html__( 'Carreto', 'fortram' ),
        'Chaveiro' => esc_html__( 'Chaveiro', 'fortram' ),
        'Conserto de eletrodomésticos' => esc_html__( 'Conserto de eletrodomésticos', 'fortram' ),
        'Costureiros' => esc_html__( 'Costureiros', 'fortram' ),
        'Cuidador de Idoso' => esc_html__( 'Cuidador de Idoso', 'fortram' ),
        'Danças' => esc_html__( 'Danças', 'fortram' ),
        'Decoração de Festas' => esc_html__( 'Decoração de Festas', 'fortram' ),
        'Depilação' => esc_html__( 'Depilação', 'fortram' ),
        'Diarista' => esc_html__( 'Diarista', 'fortram' ),
        'DJ' => esc_html__( 'DJ', 'fortram' ),
        'Eletricista' => esc_html__( 'Eletricista', 'fortram' ),
        'Encanador' => esc_html__( 'Encanador', 'fortram' ),
        'Esportes' => esc_html__( 'Esportes', 'fortram' ),
        'Esteticista' => esc_html__( 'Esteticista', 'fortram' ),
        'Fotógrafo' => esc_html__( 'Fotógrafo', 'fortram' ),
        'Garçom' => esc_html__( 'Garçom', 'fortram' ),
        'Idioma' => esc_html__( 'Idioma', 'fortram' ),
        'Jardineiro' => esc_html__( 'Jardineiro', 'fortram' ),
        'Lavagem de Carro' => esc_html__( 'Lavagem de Carro', 'fortram' ),
        'Manicure' => esc_html__( 'Manicure', 'fortram' ),
        'Manutenção' => esc_html__( 'Manutenção', 'fortram' ),
        'Maquiador' => esc_html__( 'Maquiador', 'fortram' ),
        'Marceneiro' => esc_html__( 'Marceneiro', 'fortram' ),
        'Massagens e Terapias' => esc_html__( 'Massagens e Terapias', 'fortram' ),
        'Motoboy' => esc_html__( 'Motoboy', 'fortram' ),
        'Motorista' => esc_html__( 'Motorista', 'fortram' ),
        'Música' => esc_html__( 'Música', 'fortram' ),
        'Pedreiro' => esc_html__( 'Pedreiro', 'fortram' ),
        'Personal Organizer' => esc_html__( 'Personal Organizer', 'fortram' ),
        'Personal stylist' => esc_html__( 'Personal stylist', 'fortram' ),
        'Personal trainer' => esc_html__( 'Personal trainer', 'fortram' ),
        'Pets' => esc_html__( 'Pets', 'fortram' ),
        'Pintor' => esc_html__( 'Pintor', 'fortram' ),
        'Reforço' => esc_html__( 'Reforço', 'fortram' ),
        'Serralheiro' => esc_html__( 'Serralheiro', 'fortram' ),
        'Tapeceiro' => esc_html__( 'Tapeceiro', 'fortram' ),
        'Técnico de Informática' => esc_html__( 'Técnico de Informática', 'fortram' ),
        'Vidraceiro' => esc_html__( 'Vidraceiro', 'fortram' ),
	);
?>
<?php
    foreach($cats as $key => $value):
?>
        <option value="<?php echo $key; ?>" <?php if($i == $key){ echo 'selected'; }?>><?php echo $value; ?></option>
<?php
    endforeach;
?>
<?php
}

function busca_servicos(){
    
if(isset($_GET['categoria'])) {
    $categoria = $_GET['categoria'];
}    
if(isset($_GET['cidade'])) {
    $cidade = $_GET['cidade'];
}    
if(isset($_GET['estado'])) {
    $estado = $_GET['estado'];
}
?>
<form method="get" action="<?php get_site_url(); ?>/servicos/busca/">
    <div style="margin-bottom:10px;">
        <select name="categoria" style="padding:10px 15px;border-radius:30px; width:100%; border:1px solid #e0e0e0;">
            <option value="" selected>Categoria</option>
            <option value="" ></option>
            <?php echo select_categorias($i = $categoria); ?>
        </select>
    </div>
    <div style="margin-bottom:10px;">
        <input type="text" name="cidade" placeholder="Cidade" style="padding:10px 15px;border-radius:30px; width:100%; border:1px solid #e0e0e0;" <?php if(!empty($cidade)){ echo 'value="'.$cidade.'"'; } ?>>
    </div>
    <div style="margin-bottom:10px;">
        <select name="estado" style="padding:10px 15px;border-radius:30px; width:100%; border:1px solid #e0e0e0;">
            <option value="" selected>Estado</option>
            <option value="" ></option>
            <option value="AC" <?php if($estado == 'AC'){ echo 'selected'; } ?>>Acre</option>
            <option value="AL" <?php if($estado == 'AL'){ echo 'selected'; } ?>>Alagoas</option>
            <option value="AP" <?php if($estado == 'AP'){ echo 'selected'; } ?>>Amapá</option>
            <option value="AM" <?php if($estado == 'AM'){ echo 'selected'; } ?>>Amazonas</option>
            <option value="BA" <?php if($estado == 'BA'){ echo 'selected'; } ?>>Bahia</option>
            <option value="CE" <?php if($estado == 'CE'){ echo 'selected'; } ?>>Ceará</option>
            <option value="DF" <?php if($estado == 'DF'){ echo 'selected'; } ?>>Distrito Federal</option>
            <option value="ES" <?php if($estado == 'ES'){ echo 'selected'; } ?>>Espírito Santo</option>
            <option value="GO" <?php if($estado == 'GO'){ echo 'selected'; } ?>>Goiás</option>
            <option value="MA" <?php if($estado == 'MA'){ echo 'selected'; } ?>>Maranhão</option>
            <option value="MT" <?php if($estado == 'MT'){ echo 'selected'; } ?>>Mato Grosso</option>
            <option value="MS" <?php if($estado == 'MS'){ echo 'selected'; } ?>>Mato Grosso do Sul</option>
            <option value="MG" <?php if($estado == 'MG'){ echo 'selected'; } ?>>Minas Gerais</option>
            <option value="PA" <?php if($estado == 'PA'){ echo 'selected'; } ?>>Pará</option>
            <option value="PB" <?php if($estado == 'PB'){ echo 'selected'; } ?>>Paraíba</option>
            <option value="PR" <?php if($estado == 'PR'){ echo 'selected'; } ?>>Paraná</option>
            <option value="PE" <?php if($estado == 'PE'){ echo 'selected'; } ?>>Pernambuco</option>
            <option value="PI" <?php if($estado == 'PI'){ echo 'selected'; } ?>>Piauí</option>
            <option value="RJ" <?php if($estado == 'RJ'){ echo 'selected'; } ?>>Rio de Janeiro</option>
            <option value="RN" <?php if($estado == 'RN'){ echo 'selected'; } ?>>Rio Grande do Norte</option>
            <option value="RS" <?php if($estado == 'RS'){ echo 'selected'; } ?>>Rio Grande do Sul</option>
            <option value="RO" <?php if($estado == 'RO'){ echo 'selected'; } ?>>Rondônia</option>
            <option value="RR" <?php if($estado == 'RR'){ echo 'selected'; } ?>>Roraima</option>
            <option value="SC" <?php if($estado == 'SC'){ echo 'selected'; } ?>>Santa Catarina</option>
            <option value="SP" <?php if($estado == 'SP'){ echo 'selected'; } ?>>São Paulo</option>
            <option value="SE" <?php if($estado == 'SE'){ echo 'selected'; } ?>>Sergipe</option>
            <option value="TO" <?php if($estado == 'TO'){ echo 'selected'; } ?>>Tocantins</option>
        </select>
    </div>
    <div style="margin-bottom:10px; text-align:left;">
        <input type="submit" value="FILTRAR"> <?php if(!empty($negocio) || !empty($cidade) || !empty($estado)){ echo '<a href="'.get_site_url().'/servicos/" style="color:darkred; font-size:12px; margin-left:10px;">LIMPAR FILTROS</a>'; } ?>
    </div>
</form>
<?php
}

add_shortcode('busca_servicos','busca_servicos');

function remover_servicos() {
  global $wpdb;
  $wpdb->query($wpdb->prepare("UPDATE on_posts SET post_status='trash' WHERE post_type='servico' AND post_date < DATE_SUB(NOW(), INTERVAL 30 DAY);"));
}

add_action('wp_head','remover_servicos');
add_action('admin_head','remover_servicos');