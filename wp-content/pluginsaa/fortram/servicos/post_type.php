<?php

// Register Custom Post Type
function pt_servicos() {

	$labels = array(
		'name'                  => _x( 'Serviços', 'Post Type General Name', 'fortram' ),
		'singular_name'         => _x( 'Serviço', 'Post Type Singular Name', 'fortram' ),
		'menu_name'             => __( 'Serviços', 'fortram' ),
		'name_admin_bar'        => __( 'Serviços', 'fortram' ),
		'archives'              => __( 'Arquivo de Serviços', 'fortram' ),
		'attributes'            => __( 'Atributos', 'fortram' ),
		'parent_item_colon'     => __( 'Pai', 'fortram' ),
		'all_items'             => __( 'Todo os Itens', 'fortram' ),
		'add_new_item'          => __( 'Adicionar Novo Serviço', 'fortram' ),
		'add_new'               => __( 'Adicionar Serviço', 'fortram' ),
		'new_item'              => __( 'Novo Serviço', 'fortram' ),
		'edit_item'             => __( 'Editar Serviço', 'fortram' ),
		'update_item'           => __( 'Atualizar Serviço', 'fortram' ),
		'view_item'             => __( 'Ver Serviço', 'fortram' ),
		'view_items'            => __( 'Ver Serviços', 'fortram' ),
		'search_items'          => __( 'Buscar Serviços', 'fortram' ),
		'not_found'             => __( 'Nada Encontrado', 'fortram' ),
		'not_found_in_trash'    => __( 'Nada Encontrado', 'fortram' ),
		'featured_image'        => __( 'Imagem Destacada', 'fortram' ),
		'set_featured_image'    => __( 'Definir Imagem Destacada', 'fortram' ),
		'remove_featured_image' => __( 'Remover Imagem Destacada', 'fortram' ),
		'use_featured_image'    => __( 'Usar como Imagem Destacada', 'fortram' )
	);
	$args = array(
		'label'                 => __( 'Serviço', 'fortram' ),
		'description'           => __( 'Serviços', 'fortram' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'thumbnail', 'custom-fields', 'page-attributes' ),
		'hierarchical'          => true,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-hammer',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'servico', $args );

}
add_action( 'init', 'pt_servicos', 0 );

function servico_to_servicos() {
    if( is_post_type_archive( 'servico' ) ) {
        wp_redirect( home_url( '/servicos/' ), 301 );
        exit();
    }
}
add_action( 'template_redirect', 'servico_to_servicos' );


//METABOX

function metabox_servicos( $meta_boxes ) {
	$prefix = 'servicos_';

	$meta_boxes[] = array(
		'id' => 'metabox_servicos',
		'title' => esc_html__( 'Informações', 'fortram' ),
		'post_types' => 'servico',
		'context' => 'after_title',
		'priority' => 'default',
		'autosave' => 'false',
		'fields' => array(
			array(
				'id' => $prefix . 'cidade',
				'type' => 'text',
				'name' => esc_html__( 'Cidade', 'fortram' ),
			),
			array(
				'id' => $prefix . 'estado',
				'name' => esc_html__( 'Estado', 'fortram' ),
				'type' => 'select',
				'placeholder' => esc_html__( 'Selecione', 'fortram' ),
				'options' => array(
					'AC' => esc_html__( 'Acre', 'fortram' ),
                    'AL' => esc_html__( 'Alagoas', 'fortram' ),
                    'AP' => esc_html__( 'Amapá', 'fortram' ),
                    'AM' => esc_html__( 'Amazonas', 'fortram' ),
                    'BA' => esc_html__( 'Bahia', 'fortram' ),
                    'CE' => esc_html__( 'Ceará', 'fortram' ),
                    'DF' => esc_html__( 'Distrito Federal', 'fortram' ),
                    'ES' => esc_html__( 'Espírito Santo', 'fortram' ),
                    'GO' => esc_html__( 'Goiás', 'fortram' ),
                    'MA' => esc_html__( 'Maranhão', 'fortram' ),
                    'MT' => esc_html__( 'Mato Grosso', 'fortram' ),
                    'MS' => esc_html__( 'Mato Grosso do Sul', 'fortram' ),
                    'MG' => esc_html__( 'Minas Gerais', 'fortram' ),
                    'PR' => esc_html__( 'Paraná', 'fortram' ),
                    'PB' => esc_html__( 'Paraíba', 'fortram' ),
                    'PA' => esc_html__( 'Pará', 'fortram' ),
                    'PE' => esc_html__( 'Pernambuco', 'fortram' ),
                    'PI' => esc_html__( 'Piauí', 'fortram' ),
                    'RN' => esc_html__( 'Rio Grande do Norte', 'fortram' ),
                    'RS' => esc_html__( 'Rio Grande do Sul', 'fortram' ),
                    'RJ' => esc_html__( 'Rio de Janeiro', 'fortram' ),
                    'RO' => esc_html__( 'Rondônia', 'fortram' ),
                    'RR' => esc_html__( 'Roraima', 'fortram' ),
                    'SC' => esc_html__( 'Santa Catarina', 'fortram' ),
                    'SE' => esc_html__( 'Sergipe', 'fortram' ),
                    'SP' => esc_html__( 'São Paulo', 'fortram' ),
                    'TO' => esc_html__( 'Tocantins', 'fortram' ),

				),
			),
			array(
				'id' => $prefix . 'categoria',
				'name' => esc_html__( 'Categoria', 'fortram' ),
				'type' => 'select',
				'placeholder' => esc_html__( 'Selecione', 'fortram' ),
				'options' => array(
					'Alimentação' => esc_html__( 'Alimentação', 'fortram' ),
                    'Animação de festas' => esc_html__( 'Animação de festas', 'fortram' ),
                    'Ar Condicionado e Ventilação' => esc_html__( 'Ar Condicionado e Ventilação', 'fortram' ),
                    'Bikeboy' => esc_html__( 'Bikeboy', 'fortram' ),
                    'Cabeleireiro' => esc_html__( 'Cabeleireiro', 'fortram' ),
                    'Carpinteiro' => esc_html__( 'Carpinteiro', 'fortram' ),
                    'Carreto' => esc_html__( 'Carreto', 'fortram' ),
                    'Chaveiro' => esc_html__( 'Chaveiro', 'fortram' ),
                    'Conserto de eletrodomésticos' => esc_html__( 'Conserto de eletrodomésticos', 'fortram' ),
                    'Costureiros' => esc_html__( 'Costureiros', 'fortram' ),
                    'Cuidador de Idoso' => esc_html__( 'Cuidador de Idoso', 'fortram' ),
                    'Danças' => esc_html__( 'Danças', 'fortram' ),
                    'Decoração de Festas' => esc_html__( 'Decoração de Festas', 'fortram' ),
                    'Depilação' => esc_html__( 'Depilação', 'fortram' ),
                    'Diarista' => esc_html__( 'Diarista', 'fortram' ),
                    'DJ' => esc_html__( 'DJ', 'fortram' ),
                    'Eletricista' => esc_html__( 'Eletricista', 'fortram' ),
                    'Encanador' => esc_html__( 'Encanador', 'fortram' ),
                    'Esportes' => esc_html__( 'Esportes', 'fortram' ),
                    'Esteticista' => esc_html__( 'Esteticista', 'fortram' ),
                    'Fotógrafo' => esc_html__( 'Fotógrafo', 'fortram' ),
                    'Garçom' => esc_html__( 'Garçom', 'fortram' ),
                    'Idioma' => esc_html__( 'Idioma', 'fortram' ),
                    'Jardineiro' => esc_html__( 'Jardineiro', 'fortram' ),
                    'Lavagem de Carro' => esc_html__( 'Lavagem de Carro', 'fortram' ),
                    'Manicure' => esc_html__( 'Manicure', 'fortram' ),
                    'Manutenção' => esc_html__( 'Manutenção', 'fortram' ),
                    'Maquiador' => esc_html__( 'Maquiador', 'fortram' ),
                    'Marceneiro' => esc_html__( 'Marceneiro', 'fortram' ),
                    'Massagens e Terapias' => esc_html__( 'Massagens e Terapias', 'fortram' ),
                    'Motoboy' => esc_html__( 'Motoboy', 'fortram' ),
                    'Motorista' => esc_html__( 'Motorista', 'fortram' ),
                    'Música' => esc_html__( 'Música', 'fortram' ),
                    'Pedreiro' => esc_html__( 'Pedreiro', 'fortram' ),
                    'Personal Organizer' => esc_html__( 'Personal Organizer', 'fortram' ),
                    'Personal stylist' => esc_html__( 'Personal stylist', 'fortram' ),
                    'Personal trainer' => esc_html__( 'Personal trainer', 'fortram' ),
                    'Pets' => esc_html__( 'Pets', 'fortram' ),
                    'Pintor' => esc_html__( 'Pintor', 'fortram' ),
                    'Reforço' => esc_html__( 'Reforço', 'fortram' ),
                    'Serralheiro' => esc_html__( 'Serralheiro', 'fortram' ),
                    'Tapeceiro' => esc_html__( 'Tapeceiro', 'fortram' ),
                    'Técnico de Informática' => esc_html__( 'Técnico de Informática', 'fortram' ),
                    'Vidraceiro' => esc_html__( 'Vidraceiro', 'fortram' ),
				),
			),
			array(
				'id' => $prefix . 'experiencia',
				'type' => 'textarea',
				'name' => esc_html__( 'Experiência', 'fortram' ),
				'desc' => esc_html__( '250 caracteres restantes.', 'fortram' ),
				'rows' => 4,
				'class' => 'campo_descricao',
				'placeholder' => 'Descreva sua experiência na área.',
			),
// 			array(
// 				'id' => $prefix . 'preco',
// 				'type' => 'number',
// 				'name' => esc_html__( 'Preço', 'fortram' ),
// 				'min' => '5',
// 				'step' => '5',
// 			),
// 			array(
// 				'id' => $prefix . 'periodo',
// 				'name' => esc_html__( 'Perído', 'fortram' ),
// 				'type' => 'select',
// 				'placeholder' => esc_html__( 'Selecione', 'fortram' ),
// 				'options' => array(
// 					'Hora' => esc_html__( 'Hora', 'fortram' ),
//                     'Dia' => esc_html__( 'Dia', 'fortram' ),
//                     '' => esc_html__( 'Nenhum', 'fortram' ),
// 				),
// 				'desc' => esc_html__( 'Defina se o preço cobrado é por dia, por hora, ou nenhum.', 'fortram' ),
// 			),
			array(
				'id' => $prefix . 'whatsapp',
				'type' => 'text',
				'name' => esc_html__( 'WhatsApp', 'fortram' ),
				'desc' => esc_html__( 'Número do WhatsApp para contato.', 'fortram' ),
				'class' => 'ftr_celular',
			),
			array(
				'id' => $prefix . 'endereco',
				'type' => 'text',
				'name' => esc_html__( 'Endereço', 'fortram' ),
				'desc' => esc_html__( 'Digite o endereço e selecione o local mais próximo no mapa.', 'fortram' ),
			),
			array(
                'id'            => $prefix . 'mapa',
                'name'          => 'Mapa',
                'type'          => 'osm',
                'zoom'          => 14,
                'height'        => '300px',
                'address_field' => 'servicos_endereco',
            ),
		),
	);

	return $meta_boxes;
}
add_filter( 'rwmb_meta_boxes', 'metabox_servicos' );

//SINGLE TEMPLATE

function single_servico( $template ) {
    global $post;

    if ( 'servico' === $post->post_type && locate_template( array( 'single-servico.php' ) ) !== $template ) {
        return plugin_dir_path( __FILE__ ) . 'single-servico.php';
    }

    return $template;
}

add_filter( 'single_template', 'single_servico' );