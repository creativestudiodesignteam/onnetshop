<?php

function grid_servicos(){
    $query_imoveis = new WP_Query( array( 
	'posts_per_page' => -1,
	'post_type' => 'servico',
	'meta_query' => array(
        'relation' => 'AND',
        array(
            'key' => 'ocultar',
            'value' => '1',
            'compare' => '!='
            ),
        )
) );
 
if ( $query_imoveis->have_posts() ) {
?>
<style>
    .tag_negocio {
        background:#FED700;
        color:black;
        padding:0px 15px;
        font-weight:bold;
        text-transform:uppercase;
        position:relative;
        top:10px;
        left:10px;
        display:inline-block;
        border-radius:50px;
        font-size:10px;
    }
    .tag_preco {
        background:#429A45;
        color:black;
        padding:0px 15px;
        font-weight:bold;
        text-transform:uppercase;
        position:relative;
        top:10px;
        left:10px;
        display:inline-block;
        border-radius:50px;
        font-size:13px;
    }
    .img_imovel {
        height:150px;
    }
    @media (max-width:768px){
        .img_imovel {
            height:250px;
        }
    }
</style>
<div class="row">
<?php
    $i = 1;
	while ( $query_imoveis->have_posts() ) {
		$query_imoveis->the_post();
		$featured_img_url = get_the_post_thumbnail_url(get_the_ID(),'full');
?>
    
		<div class="col-sm-4 col-xs-12" style="margin-bottom:20px;">
		    <a href="<?php echo get_the_permalink(); ?>">
		    <div style="background:rgba(0,0,0,0.03);">
                <div style="background:url('<?php echo $featured_img_url; ?>') center center;background-size:cover;" class="img_imovel">
                    <?php //<div class="tag_negocio">R$echo rwmb_meta('servicos_preco'); ? ?php if(!empty(rwmb_meta('servicos_periodo'))){ echo '/ ' . rwmb_meta('servicos_periodo'); } </div> ?>
                </div>
    		    <div style="padding:15px;">
                    <h6 style="font-weight:bold;"><?php echo get_the_title(); ?></h6>
                    <p style="color:#333e48 !important; font-size:14px;line-height:1.3;"><?php echo rwmb_meta('servicos_categoria'); ?></p>
                    <p style="color:#333e48 !important; font-size:13px;line-height:1.3;margin-bottom:0;"><i class="fas fa-map-marker-alt" style="color:#FED700;"></i> <?php echo '<strong>' . rwmb_meta('servicos_cidade') . ' - ' . rwmb_meta('servicos_estado') . '</strong>'; ?></p>
                </div>
		    </div>
		    </a>
		</div>
<?php
        $i++;
        if( $i%4 == 0 && $i != 1 ) {
            echo '</div>
            <div class="row">';
        }
	}
?>
</div>
<?php
	wp_reset_postdata();
} 
}

add_shortcode('grid_servicos','grid_servicos');