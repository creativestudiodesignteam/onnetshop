<?php
add_action( 'admin_menu', 'ftr_add_admin_menu' );
add_action( 'admin_init', 'ftr_settings_init' );


function ftr_add_admin_menu(  ) { 

	add_menu_page( 'ONnetshop', 'ONnetshop', 'manage_options', 'on_net_shop', 'ftr_options_home', '', '2' );
	add_submenu_page( 'on_net_shop', 'Anúncios', 'Anúncios', 'manage_options', 'on_net_shop_anuncios', 'ftr_options_page' );

}

add_action('admin_head','css_admin_ftr');
function css_admin_ftr(){
?>
<style>
    .ftr_admin_nav li {
        list-style:none !important;
        display:inline-block;
        margin-right:-3px;
    }
    .ftr_admin_nav li a {
        border:1px solid #A3A6A8;
        background:rgba(0,0,0,0.1);
        padding:10px 15px;
        position:relative !important;
        z-index:99 !important;
        text-decoration:none !important;
        font-weight:bold;
        color:#222222 !important;
    }
    .ftr_admin_nav li.active a {
        border:1px solid #A3A6A8;
        border-bottom:1px solid #E3E7EA !important;
        background:#E3E7EA;
        padding:10px 15px;
    }
    .border-separator {
        border-bottom:1px solid #A3A6A8;
        width:100%;
        margin-top:-10px;
        position:relative;
        z-index:1 !important;
    }
</style>
<?php
}

function ftr_options_home(){
		?>
        <div class="wrap"></div>

			<h2>ONnetshop</h2>
            <br>
			<ul class="ftr_admin_nav">
			    <li style="padding-left:15px;"></li>
			    <li class="active"><a href="?page=on_net_shop">ONnetshop</a></li>
			    <li><a href="?page=on_net_shop_anuncios">Configurações de Anúncios</a></li>
			</ul>
			<div class="border-separator"></div>
            <p>Bem vindo :) Navegue no menu acima para configurar algumas funcionalidades do site.</p>
		</div>
		<?php
}


function ftr_settings_init(  ) { 

	register_setting( 'pluginPage', 'ftr_settings' );

	add_settings_section(
		'ftr_pluginPage_section', 
		__( 'Configurações de Anúncios', 'fortram' ), 
		'ftr_settings_section_callback', 
		'pluginPage'
	);

	add_settings_field( 
		'ftr_id_imovel', 
		__( 'ID Woo - Anúncio Imóvel', 'fortram' ), 
		'ftr_id_imovel_render', 
		'pluginPage', 
		'ftr_pluginPage_section' 
	);

	add_settings_field( 
		'ftr_id_servico', 
		__( 'ID Woo - Anúncio Serviço', 'fortram' ), 
		'ftr_id_servico_render', 
		'pluginPage', 
		'ftr_pluginPage_section' 
	);

	add_settings_field( 
		'ftr_id_produto', 
		__( 'ID Woo - Anúncio Produto Usado', 'fortram' ), 
		'ftr_id_produto_render', 
		'pluginPage', 
		'ftr_pluginPage_section' 
	);


}


function ftr_id_imovel_render(  ) { 

	$options = get_option( 'ftr_settings' );
	?>
	<input type='text' name='ftr_settings[ftr_id_imovel]' value='<?php echo $options['ftr_id_imovel']; ?>'>
	<?php

}


function ftr_id_servico_render(  ) { 

	$options = get_option( 'ftr_settings' );
	?>
	<input type='text' name='ftr_settings[ftr_id_servico]' value='<?php echo $options['ftr_id_servico']; ?>'>
	<?php

}


function ftr_id_produto_render(  ) { 

	$options = get_option( 'ftr_settings' );
	?>
	<input type='text' name='ftr_settings[ftr_id_produto]' value='<?php echo $options['ftr_id_produto']; ?>'>
	<?php

}


function ftr_settings_section_callback(  ) { 

	echo __( 'Defina o ID do produto que será vendido como espaço para anunciar imóvel, serviço ou negócio e produto usado.<br><strong>Atenção:</strong> Mudar o ID do produto poderá causar falha no sistema, uma vez que, usuários que compraram o espaço de anúncio através de um produto com ID diferente, a contagem de espaços disponíveis não funcionará.', 'fortram' );

}


function ftr_options_page(  ) { 

		?>
		<form action='options.php' method='post'>

			<h2>ONnetshop</h2>
            <br>
			<ul class="ftr_admin_nav">
			    <li style="padding-left:15px;"></li>
			    <li><a href="?page=on_net_shop">ONnetshop</a></li>
			    <li class="active"><a href="?page=on_net_shop_anuncios">Configurações de Anúncios</a></li>
			</ul>
			<div class="border-separator"></div>

			<?php
			settings_fields( 'pluginPage' );
			do_settings_sections( 'pluginPage' );
			submit_button();
			?>

		</form>
		<?php

}