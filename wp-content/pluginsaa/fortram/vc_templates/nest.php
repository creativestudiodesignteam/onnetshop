<?php

vc_map( array(
    "name" => __("Team Wrapper", "my-text-domain"),
    "base" => "team_wrapper",
    'category' =>__('Team', 'rhthm'),
    "as_parent" => array('only' => 'team_mate', 'only' => 'vc_row'), // Use only|except attributes to limit child shortcodes (separate multiple values with comma)
    "content_element" => true,
    "show_settings_on_create" => false,
    "is_container" => true,
    "js_view" => 'VcColumnView',
    "params" => array(
        // add params same as with any other content element
        array(
            "type" => "textfield",
            "heading" => __("Link do Box", "rhthm"),
            "param_name" => "link",
            "description" => __("Link do Box", "rhthm"),
            'holder' => 'div',
            'class' => 'link-class',
        ),
    ),
) );


vc_map( array(
    "name" => __("Team Mate", "rhthm"),
    "base" => "team_mate",
    'category' =>__('Team', 'rhthm'),
    "content_element" => true,
    "as_child" => array('only' => 'team_wrapper'), // Use only|except attributes to limit parent (separate multiple values with comma)
    "params" => array(
        // add params same as with any other content element
        array(
            "type" => "textfield",
            "heading" => __("Name", "rhthm"),
            "param_name" => "name",
            "description" => __("Name", "rhthm"),
            'holder' => 'div',
            'class' => 'text-class',
        ),
        array(
            "type" => "textfield",
            "heading" => __("Status", "rhthm"),
            "param_name" => "status",
            "description" => __("Status", "rhthm"),
            'holder' => 'div',
            'class' => 'text-class',
        )
        ,array(
            "type" => "attach_image",
            "heading" => __("Profile Image", "rhthm"),
            "param_name" => "profile_image",
            "description" => __("Profile Image", "rhthm")
        )
        ,array(
            "type" => "textarea",
            "heading" => __("Description", "rhthm"),
            "param_name" => "description",
            "description" => __("Description", "rhthm")
        )
    )
) );


function team_wrapper_block_function( $atts, $content ) {

    extract( shortcode_atts( array(
        'el_class' =>  '',
        'link' => ''
    ), $atts ) );

    ob_start();
    	echo '<a href="'.$link.'">'.do_shortcode($content).'</div>';
    $content = ob_get_contents();
    ob_get_clean();
    return $content;
}
add_shortcode( 'team_wrapper', 'team_wrapper_block_function' );

function team_mate_block_function( $atts, $content ) {
    extract( shortcode_atts( array(
        'name' =>  '',
        'status' =>  '',
        'profile_image' =>  '',
        'description' =>  ''
    ), $atts ) );

    ob_start();
    ?>
    	<div class="col-md-3 col-sm-4">
        <div class="team-thumb">
            <div class="team-img">
            	<?php $banner = wp_get_attachment_url($profile_image, 'full');  ?>
              <img src="<?php echo $banner ; ?>" alt="">
            </div>
            <div class="team-caption">
                <div class="team-header">
                    <h3 class="name"><?php echo $name; ?></h3>
                    <h4 class="post-title"><?php echo $status; ?></h4>
                </div>
                <p><?php echo $description; ?></p>
            </div>
        </div>
      </div>
    <?php
    $content = ob_get_contents();
  
    ob_get_clean();

    return $content;
}
add_shortcode( 'team_mate', 'team_mate_block_function' );

if ( class_exists( 'WPBakeryShortCodesContainer' ) ) {
    class WPBakeryShortCode_Team_Wrapper extends WPBakeryShortCodesContainer {
    }
}
if ( class_exists( 'WPBakeryShortCode' ) ) {
    class WPBakeryShortCode_Team_Mate extends WPBakeryShortCode {
    }
}