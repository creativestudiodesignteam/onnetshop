<?php

function grid_usado_busca(){
    
if(isset($_GET['tag'])) {
    $categoria = $_GET['tag'];
    $busca_negocio = array(
        'key' => 'usado_tags',
        'value' => $categoria,
        'compare' => 'LIKE'
        );
}    
if(isset($_GET['cidade'])) {
    $cidade = $_GET['cidade'];
    $busca_cidade = array(
        'key' => 'usado_cidade',
        'value' => $cidade,
        'compare' => 'LIKE'
        );
}    
if(isset($_GET['estado'])) {
    $estado = $_GET['estado'];
    $busca_estado = array(
        'key' => 'usado_estado',
        'value' => $estado,
        'compare' => 'LIKE'
        );
}

$busca = array(
    'relation' => 'AND',
    $busca_negocio,
    $busca_cidade,
    $busca_estado,
    array(
        'key' => 'ocultar',
        'value' => '1',
        'compare' => '!='
        )
    );

    $query_usadoss = new WP_Query( array( 
	'posts_per_page' => -1,
	'post_type' => 'usado',
	'meta_query' => $busca,
) );
 
if ( $query_usadoss->have_posts() ) {
?>
<style>
    .tag_negocio {
        background:#FED700;
        color:black;
        padding:0px 15px;
        font-weight:bold;
        text-transform:uppercase;
        position:relative;
        top:10px;
        left:10px;
        display:inline-block;
        border-radius:50px;
        font-size:10px;
    }
    .tag_preco {
        background:#429A45;
        color:black;
        padding:0px 15px;
        font-weight:bold;
        text-transform:uppercase;
        position:relative;
        top:10px;
        left:10px;
        display:inline-block;
        border-radius:50px;
        font-size:13px;
    }
    .img_imovel {
        height:150px;
    }
    @media (max-width:768px){
        .img_imovel {
            height:250px;
        }
    }
</style>
<div class="row">
<?php
    $i = 1;
	while ( $query_usadoss->have_posts() ) {
		$query_usadoss->the_post();
		$featured_img_url = get_the_post_thumbnail_url(get_the_ID(),'full');
?>
    
		<div class="col-sm-4 col-xs-12" style="margin-bottom:20px;">
		    <a href="<?php echo get_the_permalink(); ?>">
		    <div style="background:rgba(0,0,0,0.03);">
                <div style="background:url('<?php echo $featured_img_url; ?>') center center;background-size:cover;" class="img_imovel">
                    <div class="tag_negocio">R$<?php echo rwmb_meta('usado_preco'); ?></div>
                </div>
    		    <div style="padding:15px;">
                    <h6 style="font-weight:bold;"><?php echo get_the_title(); ?></h6>
                    <p style="color:#333e48 !important; font-size:13px;line-height:1.3;margin-bottom:0;"><i class="fas fa-map-marker-alt" style="color:#FED700;"></i> <?php echo '<strong>' . rwmb_meta('usado_cidade') . ' - ' . rwmb_meta('usado_estado') . '</strong>'; ?></p>
                </div>
		    </div>
		    </a>
		</div>
<?php
        $i++;
        if( $i%4 == 0 && $i != 1 ) {
            echo '</div>
            <div class="row">';
        }
	}
?>
</div>
<?php
	wp_reset_postdata();
} else {
    echo "<strong>Nenhum resultado obtido.</strong>";
}
}

add_shortcode('grid_usado_busca','grid_usado_busca');