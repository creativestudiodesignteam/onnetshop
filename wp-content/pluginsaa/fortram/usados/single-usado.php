<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package electro
 */

global $post;
$page_meta_values = get_post_meta( $post->ID, '_electro_page_metabox', true );

$header_style = '';
if ( isset( $page_meta_values['site_header_style'] ) && ! empty( $page_meta_values['site_header_style'] ) ) {
	$header_style = $page_meta_values['site_header_style'];
}

electro_get_header( $header_style ); ?>

<style>
    .hide {
        display:none !important;
    }
    .show-modal {
        display:flex !important;
    }
</style>

<div class="holder-modal hide" style="height:100vh;align-items:center;justify-content:center;background:rgba(0,0,0,0.8);position:fixed;width:100vw;z-index:999999;top:0;left:0;text-align:center;">
    
    <div style="font-size:50px;color:white;position:fixed;top:30px;right:30px;cursor:pointer;">&times;</div>
    
    <img src="" id="img-open" style="height:calc(100vh - 200px); width:auto !important;max-width:inherit !important;pointer-events:none !important;">
    
</div>
<div id="primary" class="content" style="width:100%;">
	<main id="main" class="site-main">

	<?php 

	while ( have_posts() ) : the_post(); 

		do_action( 'electro_page_before' );

$additional_post_classes = apply_filters( 'electro_additional_post_classes', array() );

    $wpp = rwmb_meta('usado_whatsapp');
    $wpp_numbers = preg_replace("/[^0-9]/", "", $wpp );;
    $link_wpp = 'https://api.whatsapp.com/send?phone=55' . $wpp_numbers;
?>

<article id="post-<?php the_ID(); ?>" <?php post_class( $additional_post_classes ); ?> style="padding-left:15px;padding-right:15px;">
<?php
global $current_user;
get_currentuserinfo();
if ($post->post_author == $current_user->ID || current_user_can( 'manage_options' ))  { ?>
[ <?php edit_post_link('Editar Anúncio'); ?> ]
<?php } ?>
<div class="row">
    
    <div class="col-sm-5">
        
<?php
$destaque_full = get_the_post_thumbnail_url($post->ID, 'full'); 
$destaque_thumb = get_the_post_thumbnail_url($post->ID, 'onnet_1'); 
?>

<a class="img_modal" data-modal="<?php echo $destaque_full; ?>"><img style="width:100% !important;height:auto !important;cursor:pointer;" src="<?php echo $destaque_thumb; ?>"></a>
        
    </div>
    
    <div class="col-sm-7">
        
        <div class="row">
            <div class="col-sm-7">
                <div class="titulo_usado" style="margin-bottom:20px;">
                <h1 style="font-size:25px;"><?php the_title(); ?></h1>
                <h6><strong>Anunciado por:</strong> <?php the_author_meta('display_name'); ?></h6>
                <a href="<?php echo $link_wpp; ?>" target="_blank" style="color:white !important;padding:5px 15px;border-radius:50px;background:#1EBEA5; font-weight:bold; font-size:15px !important; display:inline-block;"><i class="fab fa-whatsapp" style="margin-right:10px;"></i> ENTRAR EM CONTATO</a>
                </div>
                
                <div class="descricao_usado" style="margin-top:30px; margin-bottom:30px;">
                    <p><span style="text-transform:uppercase;font-weight:bold;letter-spacing:1px;">Descrição do Produto:</span><br>
                    <?php echo  rwmb_meta('usado_descricao'); ?></p>
                    <p><span style="text-transform:uppercase;font-weight:bold;letter-spacing:1px;">Tags:</span><br>
                    <?php $tags_meta =  rwmb_meta('usado_tags'); 
                    $tags = explode(", ", $tags_meta);
                    foreach ($tags as &$tag) {
                        $tag = "<a href=\"".home_url()."/usados/busca/?tag=".$tag."&cidade=".rwmb_meta('usado_cidade')."&estado=".rwmb_meta('usado_estado')."\">$tag</a>";
                    }
                    echo implode(", ", $tags); ?></p>
                </div>
            </div>
        
            <div class="col-md-5 col-xs-12">
                <div style="background:rgba(0,0,0,0.05);padding:15px; width:100%; margin-bottom:10px;border-top:3px solid #FED700;">
                    <span style="font-weight:bold;text-transform:uppercase;letter-spacing:2px;font-size:13px;">PREÇO</span><br>
                    <span style="font-size:150%;">R$<?php echo rwmb_meta('usado_preco'); ?></span>
                    <hr>
                    <span style="font-weight:bold;text-transform:uppercase;letter-spacing:2px;font-size:13px;">LOCALIZAÇÃO</span><br>
                    <span style="font-size:150%;"><?php echo rwmb_meta("usado_cidade"); ?> - <strong><?php echo rwmb_meta("usado_estado"); ?></strong></span>
                </div>
            </div>
        </div>
        
    </div>
</div>

<div class="row" style="margin-top:50px;">
    <div class="col-xs-12">
<?php
$images = rwmb_meta( 'usadogaleria', array( 'size' => 'thumbnail' ) );
foreach ( $images as $image ) {
    //echo '<div class="col-sm-2 col-xs-4" style="padding:0 15px; margin-bottom:15px;"><a class="img_modal" data-modal="', $image['full_url'], '"><img style="width:100% !important;height:auto !important;cursor:pointer;" src="', $image['url'], '"></a></div>';
?>
<div class="col-sm-2 col-xs-4" style="padding:0 15px; margin-bottom:15px;">
<a rel="gallery-1" href="<?php echo $image['full_url']; ?>" class="swipebox">
	<img src="<?php echo $image['url']; ?>" alt="image">
</a>
</div>
<?php
}
?>
    </div>
</div>
	
</article><!-- #post-## -->

<?php

		/**
		 * @hooked electro_display_comments - 10
		 */
		do_action( 'electro_page_after' );

	endwhile; // end of the loop.

	?>

	</main><!-- #main -->
</div><!-- #primary -->

<script>
    
    jQuery(document).ready(function($){

$('.owl-carousel').owlCarousel({
    loop:true,
    margin:10,
    nav:true,
    items:1,
});

$( '.swipebox' ).swipebox( {
		removeBarsOnMobile : false, // false will show top bar on mobile devices
		hideBarsDelay : 3000, // delay before hiding bars on desktop
		loopAtEnd: true // true will return to the first image after the last image is reached
	} );

    
$('.img_modal').click(function(){
    $("#img-open").attr('src',$(this).attr('data-modal'));
    $(".holder-modal").removeClass("hide");
    $(".holder-modal").addClass("show-modal").fadeOut(0).fadeIn(500);
    $("#img-open").fadeOut(0).fadeIn(500);
});

$('.holder-modal').click(function(){
    $(this).removeClass("show-modal");
    $(this).addClass("hide");
    $('#img-open').attr('src','');
});

    });
    
    
</script>

<?php

if(rwmb_meta('ocultar') == 1){
    wp_redirect( get_site_url().'/usados/' ); exit;
}

?>

<?php get_footer();