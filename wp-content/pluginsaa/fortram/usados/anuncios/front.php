<?php
function front_usado(){
    
if ( is_user_logged_in() ) {
    
} else {
    wp_redirect( get_site_url() . '/painel/' ); exit;
}

$args = array(
    'post_type' => 'usado',
    'author'    => get_current_user_id(),
    'post_status' => array('publish', 'pending', 'draft', 'auto-draft', 'future', 'private', 'inherit', 'trash'),
    'posts_per_page' => -1
);

$query = new WP_Query($args);

$count = $query->found_posts;


$args2 = array(
    'post_type' => 'usado',
    'author'    => get_current_user_id(),
    'post_status' => array('publish'),
    'posts_per_page' => -1
);

$query2 = new WP_Query($args2);

$count2 = $query2->found_posts;

    
	$qtde_postados = get_the_author_meta( 'qtde_usados_postados', get_current_user_id() );
	$qtde_comprados = get_the_author_meta( 'qtde_usados_comprados', get_current_user_id() );
	
	$qtde_postar = $qtde_comprados - $qtde_postados;
	
if(isset($_COOKIE['msg_u']) && $_COOKIE['msg_u'] == 1) {
?>
<div class="msg_onet" style="background:rgba(0,0,0,0.08);border-left:4px solid #FED700;padding:5px 15px;margin-bottom:10px;">O anúncio foi publicado/alterado com sucesso.</div>
<?php
setcookie('msg_u', '0', time() + (5000), "/"); // 86400 = 1 day
}
?>
<h4><strong>Anúncios de Produtos Usados</strong></h4>
<?php if($count >= 0){ ?>
<p>Você já anunciou <strong><?php echo $count; ?></strong>* <?php if($count == 1){ echo 'produto usado'; } else { echo 'produtos usados'; } ?>, <strong><?php if($count2 == 1){ echo 'possui ' . $count2 . ' anúncio ativo'; } if($count2 > 1) { echo 'possui ' . $count2 . ' anúncios ativos'; } if($count2 == '0'){ echo 'atualmente não possui anúncios ativos'; }  ?></strong>, e ainda pode anunciar <strong><?php echo $qtde_postar; ?></strong>.<br>
<a href="<?php echo get_site_url(); ?>/comprar-espaco/">Clique aqui para adquirir mais espaços para anunciar.</a></p>
<?php } ?>
<hr>
<?php if($qtde_postar >= 1){ ?><a style="background:#FED700;padding: 10px 15px;border-radius:5px;font-size: 15px;color: black !important;margin-bottom: 10px;display: inline-block; font-weight:bold;" href="<?php echo get_site_url(); ?>/usados/novo/">CRIAR ANÚNCIO</a><br><?php } ?>
<?php if($count2 != 0){ echo do_shortcode('[mb_frontend_dashboard post_type="usado" edit_page="6672"]'); }?>
<br>
<small>*Esta contagem inclui anúncios já expirados.</small>
<?php
}

add_shortcode('front_usado','front_usado');

function front_add_usado(){
    
if ( is_user_logged_in() ) {
    
} else {
    wp_redirect( get_site_url() . '/painel/' ); exit;
}

	$qtde_postados = get_the_author_meta( 'qtde_usados_postados', get_current_user_id() );
	$qtde_comprados = get_the_author_meta( 'qtde_usados_comprados', get_current_user_id() );
	
	$qtde_postar = $qtde_comprados - $qtde_postados;
	
    if(!empty($_GET['rwmb_frontend_field_post_id'])){
        if (authorByPostID($_GET['rwmb_frontend_field_post_id']) == get_current_user_id() && getPostType($_GET['rwmb_frontend_field_post_id']) == 'usado'){
            echo do_shortcode('[mb_frontend_form post_type="usado" id="metabox_usados,galeria_usado" post_fields="title,thumbnail"]');
        } else {
            wp_redirect( get_site_url().'/usados/gerenciar/' ); exit;
        }
    }
    if(empty($_GET['rwmb_frontend_field_post_id']) && $qtde_postar >= 1){
        echo do_shortcode('[mb_frontend_form post_type="usado" id="metabox_usados,galeria_usado" post_fields="title,thumbnail"]');
    }
    if(empty($_GET['rwmb_frontend_field_post_id']) && $qtde_postar < 1){
?>
<h5>Você não possui espaços para anunciar produtos usados.<br>
Você pode adquirir mais clicando no botão abaixo:</h5>
<a style="background:#FED700;padding: 10px 15px;border-radius:5px;font-size: 15px;color: black !important;margin-bottom: 10px;display: inline-block; font-weight:bold;" href="<?php echo get_site_url(); ?>/usados/novo/">COMPRAR ESPAÇO PARA ANUNCIAR</a>
<?php
    }
    if(!empty($_GET['rwmb-form-submitted'])){
        echo 'Você será redirecionado.';
        $cookie_name = "msg_u";
        $cookie_value = "1";
        setcookie($cookie_name, $cookie_value, time() + (5000), "/"); // 86400 = 1 day
        wp_redirect( get_site_url().'/usados/gerenciar/' ); exit;
    }
}

add_shortcode('front_add_usado','front_add_usado');