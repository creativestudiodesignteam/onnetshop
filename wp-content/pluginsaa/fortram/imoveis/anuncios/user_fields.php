<?php
//CUSTOM USER FIELDS

add_action( 'show_user_profile', 'user_fields_imoveis' );
add_action( 'edit_user_profile', 'user_fields_imoveis' );

function user_fields_imoveis( $user ) {
	$qtde_postados = get_the_author_meta( 'qtde_imoveis_postados', $user->ID );
	$qtde_comprados = get_the_author_meta( 'qtde_imoveis_comprados', $user->ID );
	?>
	<h3><?php esc_html_e( 'Anúncios de Imóveis', 'fortram' ); ?></h3>

	<table class="form-table">
		<tr>
			<th><label for="qtde_imoveis"><?php esc_html_e( 'Anúncios de Imóveis Comprados', 'fortram' ); ?></label></th>
			<td>
				<input type="number"
			       min="0"
			       max="999"
			       step="1"
			       id="qtde_imoveis_comprados"
			       name="qtde_imoveis_comprados"
			       value="<?php echo esc_attr( $qtde_comprados ); ?>"
			       class="regular-text"
			       disabled
				/>
			</td>
		</tr>
		<tr>
			<th><label for="qtde_imoveis"><?php esc_html_e( 'Anúncios de Imóveis Postados', 'fortram' ); ?></label></th>
			<td>
				<input type="number"
			       min="0"
			       max="999"
			       step="1"
			       id="qtde_imoveis_postados"
			       name="qtde_imoveis_postados"
			       value="<?php echo esc_attr( $qtde_postados ); ?>"
			       class="regular-text"
			       disabled
				/>
			</td>
		</tr>
	</table>
	<?php
}
// add_action( 'personal_options_update', 'update_user_fields_imoveis' );
// add_action( 'edit_user_profile_update', 'update_user_fields_imoveis' );

// function update_user_fields_imoveis( $user_id ) {
// 	if ( ! current_user_can( 'edit_user', $user_id ) ) {
// 		return false;
// 	}

// 	if ( ! empty( $_POST['qtde_imoveis_postados'] ) ) {
// 		update_user_meta( $user_id, 'qtde_imoveis_postados', intval( $_POST['qtde_imoveis_postados'] ) );
// 	}
// }