<?php

require_once(dirname(__FILE__) . '/post_type.php');
require_once(dirname(__FILE__) . '/front.php');
require_once(dirname(__FILE__) . '/busca.php');
require_once(dirname(__FILE__) . '/widget.php');
require_once(dirname(__FILE__) . '/vendor.php');

require_once(dirname(__FILE__) . '/anuncios/main.php');

//SLB.JS

add_action('wp_enqueue_scripts', function () {
    wp_enqueue_script('jquery-fancybox', get_site_url() . '/wp-content/plugins/fortram/swipebox/js/jquery.swipebox.min.js', array(), '1.1', true);
    wp_enqueue_style( 'css-fancybox', get_site_url() . '/wp-content/plugins/fortram/swipebox/css/swipebox.css?v=111' );
});

function imoveis_sidebar() {
    register_sidebar(
        array (
            'name' => __( 'Imóveis', 'fortram' ),
            'id' => 'imoveis_sidebar',
            'description' => __( 'Página Imóveis', 'fortram' ),
            'before_widget' => '<div class="widget-content" style="margin-bottom:30px;">',
            'after_widget' => "</div>",
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>',
        )
    );
}
add_action( 'widgets_init', 'imoveis_sidebar' );

function busca_imoveis(){
    
if(isset($_GET['negocio'])) {
    $negocio = $_GET['negocio'];
}    
if(isset($_GET['cidade'])) {
    $cidade = $_GET['cidade'];
}    
if(isset($_GET['estado'])) {
    $estado = $_GET['estado'];
}
?>
<form method="get" action="<?php get_site_url(); ?>/imoveis/busca/">
    <div style="margin-bottom:10px;">
        <select name="negocio" style="padding:10px 15px;border-radius:30px; width:100%; border:1px solid #e0e0e0;">
            <option value="" selected>Tipo de Negócio</option>
            <option value=""></option>
            <option value="aluguel" <?php if($negocio == 'aluguel'){ echo 'selected'; } ?>>Aluguel</option>
            <option value="venda" <?php if($negocio == 'venda'){ echo 'selected'; } ?>>Venda</option>
        </select>
    </div>
    <div style="margin-bottom:10px;">
        <input type="text" name="cidade" placeholder="Cidade" style="padding:10px 15px;border-radius:30px; width:100%; border:1px solid #e0e0e0;" <?php if(!empty($cidade)){ echo 'value="'.$cidade.'"'; } ?>>
    </div>
    <div style="margin-bottom:10px;">
        <select name="estado" style="padding:10px 15px;border-radius:30px; width:100%; border:1px solid #e0e0e0;">
            <option value="" selected>Estado</option>
            <option value=""></option>
            <option value="AC" <?php if($estado == 'AC'){ echo 'selected'; } ?>>Acre</option>
            <option value="AL" <?php if($estado == 'AL'){ echo 'selected'; } ?>>Alagoas</option>
            <option value="AP" <?php if($estado == 'AP'){ echo 'selected'; } ?>>Amapá</option>
            <option value="AM" <?php if($estado == 'AM'){ echo 'selected'; } ?>>Amazonas</option>
            <option value="BA" <?php if($estado == 'BA'){ echo 'selected'; } ?>>Bahia</option>
            <option value="CE" <?php if($estado == 'CE'){ echo 'selected'; } ?>>Ceará</option>
            <option value="DF" <?php if($estado == 'DF'){ echo 'selected'; } ?>>Distrito Federal</option>
            <option value="ES" <?php if($estado == 'ES'){ echo 'selected'; } ?>>Espírito Santo</option>
            <option value="GO" <?php if($estado == 'GO'){ echo 'selected'; } ?>>Goiás</option>
            <option value="MA" <?php if($estado == 'MA'){ echo 'selected'; } ?>>Maranhão</option>
            <option value="MT" <?php if($estado == 'MT'){ echo 'selected'; } ?>>Mato Grosso</option>
            <option value="MS" <?php if($estado == 'MS'){ echo 'selected'; } ?>>Mato Grosso do Sul</option>
            <option value="MG" <?php if($estado == 'MG'){ echo 'selected'; } ?>>Minas Gerais</option>
            <option value="PA" <?php if($estado == 'PA'){ echo 'selected'; } ?>>Pará</option>
            <option value="PB" <?php if($estado == 'PB'){ echo 'selected'; } ?>>Paraíba</option>
            <option value="PR" <?php if($estado == 'PR'){ echo 'selected'; } ?>>Paraná</option>
            <option value="PE" <?php if($estado == 'PE'){ echo 'selected'; } ?>>Pernambuco</option>
            <option value="PI" <?php if($estado == 'PI'){ echo 'selected'; } ?>>Piauí</option>
            <option value="RJ" <?php if($estado == 'RJ'){ echo 'selected'; } ?>>Rio de Janeiro</option>
            <option value="RN" <?php if($estado == 'RN'){ echo 'selected'; } ?>>Rio Grande do Norte</option>
            <option value="RS" <?php if($estado == 'RS'){ echo 'selected'; } ?>>Rio Grande do Sul</option>
            <option value="RO" <?php if($estado == 'RO'){ echo 'selected'; } ?>>Rondônia</option>
            <option value="RR" <?php if($estado == 'RR'){ echo 'selected'; } ?>>Roraima</option>
            <option value="SC" <?php if($estado == 'SC'){ echo 'selected'; } ?>>Santa Catarina</option>
            <option value="SP" <?php if($estado == 'SP'){ echo 'selected'; } ?>>São Paulo</option>
            <option value="SE" <?php if($estado == 'SE'){ echo 'selected'; } ?>>Sergipe</option>
            <option value="TO" <?php if($estado == 'TO'){ echo 'selected'; } ?>>Tocantins</option>
        </select>
    </div>
    <div style="margin-bottom:10px; text-align:left;">
        <input type="submit" value="FILTRAR"> <?php if(!empty($negocio) || !empty($cidade) || !empty($estado)){ echo '<a href="'.get_site_url().'/imoveis/" style="color:darkred; font-size:12px; margin-left:10px;">LIMPAR FILTROS</a>'; } ?>
    </div>
</form>
<?php
}

add_shortcode('busca_imoveis','busca_imoveis');


function remover_imoveis() {
  global $wpdb;
  $wpdb->query($wpdb->prepare("UPDATE on_posts SET post_status='trash' WHERE post_type='imovel' AND post_date < DATE_SUB(NOW(), INTERVAL 30 DAY);"));
}

add_action('wp_head','remover_imoveis');
add_action('admin_head','remover_imoveis');

function countAtivos($pt){
    $args = array(
        'post_type' => $pt,
        'author'    => get_current_user_id(),
        'post_status' => array('publish'),
        'posts_per_page' => -1
    );
    
    $query = new WP_Query($args);
    
    $count = $query->found_posts;
    
    return $count;
}

function countExpirados($pt){
    $args = array(
        'post_type' => $pt,
        'author'    => get_current_user_id(),
        'post_status' => array('trash'),
        'posts_per_page' => -1
    );
    
    $query = new WP_Query($args);
    
    $count = $query->found_posts;
    
    return $count;
}

function countDisponiveis($pt){
    $qtde_postados = get_the_author_meta( 'qtde_'.$pt.'_postados', get_current_user_id() );
	$qtde_comprados = get_the_author_meta( 'qtde_'.$pt.'_comprados', get_current_user_id() );
	
	$qtde_postar = intval($qtde_comprados) - intval($qtde_postados);
	
	return $qtde_postar;
}