<?php
if ( $args['id'] ) {
			$kas_map_info = '';
			
			// Generate json for map markers....
	        foreach ( $args['id'] as $seller ) {
	   			global $WCMp;
	            $store_info = $WCMp->user->get_vendor_fields($seller['id']);
	            $banner_url  = $store_info['vendor_banner']['value'];
	            
	            $store_location = get_user_meta($seller['id'], 'kas_google_location', true);
	            if (!empty($store_location) && isset($store_location)) {
	            	$locations = explode( ',', $store_location );
		        	$def_lat = $locations[0];
            		$def_long = $locations[1];
	            }
		        $store_name = isset( $store_info['vendor_page_title']['value'] ) ? esc_html( $store_info['vendor_page_title']['value'] ) : __( 'N/A', $this->kas_filter );
		        $store_url  = trailingslashit(get_home_url()).trailingslashit('vendor').$store_info['vendor_page_slug']['value'];
		        
            	       
	            
            	if (!empty($def_lat) && !empty($def_long) && isset($def_lat) && isset($def_long)) {
	            
		            $kas_map_info .= "['<div class=\"kas_map_info\">";
                    
	                $kas_map_info .= "<a href=\"".$store_url."\"><img class=\"kas_map_imp\" src=\"".esc_url( $banner_url )."\" alt=\"".esc_attr( $store_name )."\"></a><br>";
                    
		            $kas_map_info .= "<p><a href=\"".$store_url."\">".$store_name."</a></p><br><div class=\"kas_map_left\">".$store_info['vendor_address_1']['value'].'<br>'.$store_info['vendor_address_2']['value'].'<br>'.$store_info['vendor_city']['value'].'<br>'.$store_info['vendor_state']['value'].'<br>'.$store_info['vendor_postcode']['value'];
		            $kas_map_info .= "</div><div class=\"kas_map_right\"><abbr title=\"Phone Number\">P:</abbr>".esc_html( $store_info['vendor_phone']['value'] )."<br><a  target=\"_blank\" href=\"http://maps.google.com/maps?saddr=KAS_USER_POSITION&daddr=".$def_lat.",".$def_long."\">Get Direction</a><br><br><a class=\"kas_btn\" href=\"".$store_url."\">Visit Store</a></div></div>','";
		            $kas_map_info .= $def_lat."','";
		            $kas_map_info .= $def_long."'],";
	            }
  
	        }
	        
	        
	        $kas_map_info = substr ( $kas_map_info, 0, - 1 );
			$kas_map_info = '<script type="text/javascript"> var locations = [' . $kas_map_info . '];</script>';
			
			// json for map markers
			echo $kas_map_info;
	
?>			
	<div id="map" style="width: 100%; height:<?php echo esc_attr( get_option('kas-map-height') );?>px;"></div>
    <script type="text/javascript">

        window.map = new google.maps.Map(document.getElementById('map'), {
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });

        var infowindow = new google.maps.InfoWindow();

        var bounds = new google.maps.LatLngBounds();

        for (i = 0; i < locations.length; i++) {
            marker = new google.maps.Marker({
                position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                map: map
            });

            bounds.extend(marker.position);

            google.maps.event.addListener(marker, 'click', (function (marker, i) {
                return function () {
                	var res = locations[i][0].replace("KAS_USER_POSITION", ''); 
                    infowindow.setContent(res);
                    infowindow.open(map, marker);
                }
            })(marker, i));
        }

        map.fitBounds(bounds);

        var listener = google.maps.event.addListener(map, "idle", function () {
            map.setZoom(<?php echo get_option('kas-map-zoom');?>);
            google.maps.event.removeListener(listener);
        });
           

  </script> 			
	<br>	
<?php }else {?>
    <p class="dokan-error"><?php _e( 'No seller found for map!',$this->kas_filter); ?></p>
<?php } ?>	