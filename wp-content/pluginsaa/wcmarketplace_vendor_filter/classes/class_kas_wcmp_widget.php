<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/**
 * Get all required data from Dokan and woocommerce
 *
 *
 * @link       http://mamukb.com
 * @since      1.2.0
 *
 * @package    Kas_WCMP_Filter
 * @subpackage kas_WCMP_filter/classes
 */

/**
 *
 *
 * @since      1.2.0
 * @package    Kas_WCMP_Filter
 * @subpackage kas_WCMP_filter/classes
 * @author     Syed Muhammad Shafiq <shafiq_shaheen@hotmail.com>
 */
class Kas_WCMP_Filter_Widget extends WP_Widget {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.2.0
	 * @access   private
	 * @var      string    $kas_filter    The ID of this plugin.
	 */
	private $kas_filter;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.2.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Register widget with WordPress.
	 *
	 * @since    1.2.0
	 * @param      string    $kas_filter       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	function __construct( $kas_filter, $version ) {
		parent::__construct(
			$this->kas_filter, // Base ID
			esc_html__( 'Search Vendor', $this->kas_filter ), // Name
			array( 'description' => esc_html__( 'WCMarketplace Vendor Filter', $this->kas_filter ), ) // Args
		);
		$this->kas_filter = $kas_filter;
		$this->version = $version;
		$this->load_dependencies();		
	}
	
	

	/**
	 * Load the required dependencies for this class.
	 *
	 * Include the following files :
	 * 
	 * - Kas_Dokan_Vendor_Filter_WCVData. Collect and get all required wcvendors saller information.
	 *
	 *
	 * @since    1.0.6
	 * @access   private
	 */
	private function load_dependencies() {
		/**
		 * The class responsible for getting dokan data to menupulate fields
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'classes/class_kas_wcmp_wcmpdata.php';

	}		
	
	

	/**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args     Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {
		echo $args['before_widget'];
		if ( ! empty( $instance['title'] ) ) {
			echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ) . $args['after_title'];
		}
		
		// get data from wcmp
		
		$kas_data = new Kas_WCMP_WCMPData($this->kas_filter, $this->version);
		
		$kas_wcv_data = $kas_data->kas_wcmp_data();
		$kas_wcv_countries = $kas_data->kas_wcmp_countries();
		$kas_wcv_states = $kas_data->kas_wcmp_states();
		$kas_wcv_cities = $kas_data->kas_wcmp_cities();
		$kas_wcv_zips = $kas_data->kas_wcmp_zips();
		$kas_wcv_categories = $kas_data->kas_wcmp_category();
		$kas_wcv_stores = $kas_data->kas_wcmp_stores();
		
		$args = array(
			'data'	=> $kas_wcv_data,
            'countries' => $kas_wcv_countries,
            'states' => $kas_wcv_states,
            'cities' => $kas_wcv_cities,
			'zips'	=> $kas_wcv_zips,
			'categories'	=> $kas_wcv_categories,
            'stores' => $kas_wcv_stores,
		);
		
		$this->kas_widget_temp('form',$args);		
		
		echo $args['after_widget'];
	}

	/**
	 * Back-end widget form.
	 *
	 * @see WP_Widget::form()
	 *
	 * @param array $instance Previously saved values from database.
	 */
	public function form( $instance ) {
		$title = ! empty( $instance['title'] ) ? $instance['title'] : esc_html__( 'WCMarketplace Vendors Filter', $this->kas_filter );
		?>
		<p>
		<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_attr_e( 'Title:', $this->kas_filter ); ?></label> 
		<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
		</p>
		<?php 
	}

	/**
	 * Sanitize widget form values as they are saved.
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 *
	 * @return array Updated safe values to be saved.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';

		return $instance;
	}
	
	/**
	 * Render WC Marketplace vendor filter html.
	 *
	 * @since    1.2.4
	 */	
	public function kas_widget_temp($name = '', $args){
		switch ($name) {
			case 'form':
				include_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/partials/kas_wcmp_filter_widget.php';
				break;
			default:
				include_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/partials/kas_wcmp_filter_widget.php';
				break;
		}
	}	
}