<?php
/*
 * Plugin Name: WC-Marketplace Vendor Filter
 * Plugin URI: https://codecanyon.net/item/wc-marketplace-vendor-filter/19664482?ref=kas5986
 * Description: add-on for WC-Marketplace - Multi-vendor Marketplace allow you to filter Store by Location and more...
 * Version: 1.2.1
 * Author: Syed Muhammad Shafiq
 * Author URI: http://www.support.mamukb.com
 * License: GPL v2+
 * License URI: https://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain: kas-wcmp-filter
 * Domain Path: 
 */
 
 if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/**
 * The code that runs during plugin activation.
 */
function activate_kas_wcmp_filter() {
	require_once plugin_dir_path( __FILE__ ) . 'classes/class_kas_wcmp_activation.php';
	Kas_WCMP_Activation::activate();
}

/**
 * The code that runs during plugin deactivation.
 */
function deactivate_kas_wcmp_filter() {
	require_once plugin_dir_path( __FILE__ ) . 'classes/class_kas_wcmp_deactivation.php';
	Kas_WCMP_Deactivation::deactivate();
}


register_activation_hook( __FILE__, 'activate_kas_wcmp_filter' );
register_deactivation_hook( __FILE__, 'deactivate_kas_wcmp_filter' );

/**
 * Load all the function file for overall files
 */
require plugin_dir_path( __FILE__ ) . 'includes/functions.php';

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'classes/class_kas_wcmp_filter.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_kas_wcmp_filter(){

	$plugin = new Kas_WCMP_Filter();
	$plugin->run();

}
run_kas_wcmp_filter();


