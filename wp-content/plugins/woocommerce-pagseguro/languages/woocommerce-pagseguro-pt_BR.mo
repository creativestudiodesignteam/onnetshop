��          �       \      \  �   ]  �   �     �     �     �     �     �     �                    *     7     J      X     y  <   �  %   �     �      �     �   �     ]     l     s     �     �  	   �     �     �     �     �     �       "        ?  C   T  (   �     �   * After clicking "Proceed to payment" you will have access to banking ticket which you can print and pay in your internet banking or in a lottery retailer. * After clicking "Proceed to payment" you will have access to the link that will take you to your bank's website, so you can make the payment in total security. Bank Transfer Banking Ticket Card Holder Birth Date Card Holder CPF Card Holder Name Card Holder Phone Card Number Credit Card Expiry (MM/YYYY) Installments Proceed to payment Security Code Select a number of installments. Select your bank: The order will be confirmed only after the payment approval. This purchase is being made in Brazil as recorded on the card Project-Id-Version: Claudio Sanches - PagSeguro for WooCommerce 2.14.0
Report-Msgid-Bugs-To: https://wordpress.org/support/plugin/woocommerce-pagseguro
POT-Creation-Date: 2019-09-20 22:53:10+00:00
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2020-06-02 14:33+0000
Last-Translator: ONnetshop
Language-Team: Português do Brasil
X-Generator: Loco https://localise.biz/
Language: pt_BR
Plural-Forms: nplurals=2; plural=n != 1;
X-Loco-Version: 2.3.4; wp-5.3.2 * Depois de clicar em "Finalizar pagamento", você terá acesso ao boleto bancário, que poderá ser impresso e pago no seu Internet Banking ou em uma casa loterica. Depois de clicar em "Finalizar pagamento", você terá acesso ao link que o levará ao site do seu banco, para que você possa efetuar o pagamento com total segurança.
 Transferência Boleto Data de Nascimento CPF do dono do cartão Nome Completo Telefone  Número do Cartão Cartão de Crédito Validade Parcelamento Finalizar Pagamento Código de Segurança Selecione a quantidade de parcelas Selecione seu banco: O pedido será confirmado somente após a aprovação do pagamento. Essa compra está sendo feita do Brasil. como está no cartão 