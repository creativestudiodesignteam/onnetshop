<?php

class Auxiliar {

    private static function getArrayPalavras($Texto) {
        return explode(' ', trim($Texto));
    }


    public static function executarCURL($URL, $Metodo = "GET", $ArrayParametrosPOST = Array(), $UsarProxy = false) {

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $URL);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $Metodo);

        //Proxy
        if($UsarProxy){
            $super_proxy = 'zproxy.lum-superproxy.io';
            $username = 'lum-customer-hl_a45fc997-zone-zona_brasil-route_err-pass_dyn';
            $password = '6obstnciijfd';
            $port = 22225;

            $session = empty($_SESSION['session_proxy'])?mt_rand():$_SESSION['session_proxy'];
            $_SESSION['session_proxy'] = $session;
            
            curl_setopt($ch, CURLOPT_PROXY, "http://$super_proxy:$port");
            curl_setopt($ch, CURLOPT_PROXYUSERPWD, "$username-country-br-session-$session:$password");
        }

        if ($Metodo == "POST") {
            curl_setopt($ch, CURLOPT_POSTFIELDS, $ArrayParametrosPOST);
        }

        $result = curl_exec($ch);
        curl_close($ch);

        return $result;
    }


    public static function getNodes($Result, $Query) {
        $dom = new DOMDocument();
        @$dom->loadHTML($Result);

        $finder = new DomXPath($dom);
        return $finder->query($Query);
    }

    public static function exportarJSON($Array) {
        return json_encode($Array, JSON_UNESCAPED_UNICODE);
    }

}
