<?php

/**
 * Plugin Name: WC Loggi para WooCommerce
 * Plugin URI: https://woocommerce-marketplace.com.br
 * Description: Método de Frete Loggi para WooCommerce
 * Version: 1.3.8
 * Author: WooCommerce Marketplace Brasil
 * Author URI: https://woocommerce-marketplace.com.br
 * Developer: Flavio Leonard Vargas
 * Developer URI: https://woocommerce-marketplace.com.br
 * Requires at least: 4.1
 * Tested up to: 5.2.3
 * WC requires at least: 2.5.0
 * WC tested up to: 3.7.0
 * License: GPL-3.0+
 * License URI: http://www.gnu.org/licenses/gpl-3.0.html
 * Domain Path: /lang
 * Text Domain: WooCommerce-Marketplace-Brasil
 */
 

/**
 * Check if WooCommerce is active
 */
if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {

  function loggi_shipping_method_init() {
    if ( ! class_exists( 'WC_loggi_Shipping_Method' ) ) {
      class WC_loggi_Shipping_Method extends WC_Shipping_Method {
        /**
         * Constructor for loggi shipping class
         *
         * @access public
         * @return void
         */
        public function __construct( $instance_id = 0 ) {
          $this->instance_id        = absint( $instance_id );
          $this->id                 = 'loggi_shipping_method'; // Id for loggi shipping method. Should be uunique.
          $this->method_title       = __( 'Loggi' );  // Title shown in admin
          $this->method_description = __( 'Parâmetros para Loggi' ); // Description shown in admin

          $this->title              = $this->method_title;

          $this->supports           = array( 
              'shipping-zones',
              'instance-settings',
              'instance-settings-modal',
          );
          $this->init_form_fields();

          // Get settings
          $this->enabled            = 'yes';
          $this->title              = $this->get_option( 'title_back_end' );

          if ( $this->title == "") {
            $this->title = '[' . $this->get_option( 'shipping_class' ) . '] ' . $this->method_title;
            if ( $this->get_option( 'shipping_class' ) == "") {
              $this->title            = $this->method_title;
            }
          }

          $this->init();

        }

        /**
         * Init loggi settings
         *
         * @access public
         * @return void
         */
        function init() {
          // Load the settings API
          
          $this->init_settings(); // This is part of the settings API. Loads settings you previously init.

          // Save settings in admin if you have any defined
          add_action( 'woocommerce_update_options_shipping_' . $this->id, array( $this, 'process_admin_options' ) );
        }

        /**
         * Get shipping classes options.
         *
         * @return array
         */
        protected function get_shipping_classes_options() {
          $shipping_classes = WC()->shipping->get_shipping_classes();
          $options          = array(
            '' => __( '-- Select a shipping class --', 'loggi' ),
          );

          if ( ! empty( $shipping_classes ) ) {
            $options += wp_list_pluck( $shipping_classes, 'name', 'slug' );
          }

          return $options;
        }

        /**
         * Define settings field for this shipping
         * @return void 
         */
        function init_form_fields() { 

            $this->instance_form_fields = array(

            'title' => array(
                  'title'       => __( 'Título para Exibição no Site', 'loggi' ),
                  'type'        => 'text',
                  'description' => __( 'Título de exibição no site.', 'loggi' ),
                  'placeholder' => 'Título',
                  'default'     => __( 'Loggi', 'loggi' ),
                  'desc_tip'    => true
                  ),

            'title_back_end' => array(
                  'title' => __( 'Título para Exibição no Back-end', 'loggi' ),
                  'type' => 'text',
                  'description' => __( 'Título de exibição no Back-end. Caso deixe em branco, será exibido [shipping-class] Method Title', 'loggi' ),
                  'default' => __( '', 'loggi' )
                  ),

            'shipping_class' => array(
              'title'       => __( 'Shipping Class', 'loggi' ),
              'type'        => 'select',
              'description' => __( 'Select for which shipping class this method will be applied.', 'loggi' ),
              'desc_tip'    => true,
              'default'     => '',
              'class'       => 'wc-enhanced-select',
              'options'     => $this->get_shipping_classes_options(),
            ),

            'googleApiKey'      => array(
                'title'         => __( 'Google API Key', 'loggi' ),
                  'type'        => 'text',
                  'description' => __( 'Necessário habilitar as APIs Geocode e Distance Matrix do Google. Atenção! Esses serviços são cobrados. Consulte seu plano do Google para mais informações.', 'loggi' ),
                  'default'     => '',
                  'desc_tip'    => true
                  ),

            'cepOrig'           => array(
                  'title'       => __( 'CEP de origem', 'loggi' ),
                  'type'        => 'text',
                  'description' => __( 'CEP de origem das coletas. Formato 99999-999', 'loggi' ),
                  'placeholder' => '99999-999',
                  'default'     => '',
                  'desc_tip'    => true
                  ),           

            'custoFixo'         => array(
                  'title'       => __( 'Adicionar Custo Fixo', 'loggi' ),
                  'type'        => 'number',
                  'description' => __( 'Formato 9.99. Esse valor será somado ao cálculo do frete.', 'loggi' ),
                  'placeholder' => '9.99',
                  'default'     => '0',
                  'desc_tip'    => true
                  ),

            'custoPercentual'   => array(
                  'title'       => __( 'Adicionar custo Percentual', 'loggi' ),
                  'type'        => 'number',
                  'description' => __( 'Formato 9.99. Essa porcentagem será adicionada ao custo total do frete.', 'loggi' ),
                  'placeholder' => '9.99',
                  'default'     => '0',
                  'desc_tip'    => true
                  ),
           
            'taxaColeta'        => array(
                  'title'       => __( 'Taxa de Coleta', 'loggi' ),
                  'type'        => 'number',
                  'description' => __( 'Formato 9.99', 'loggi' ),
                  'placeholder' => '9.99',
                  'default'     => '4.00',
                  'desc_tip'    => true
                  ),

            'taxaEntrega'       => array(
                  'title'       => __( 'Taxa de Entrega', 'loggi' ),
                  'type'        => 'number',
                  'description' => __( 'Formato 9.99', 'loggi' ),
                  'placeholder' => '9.99',
                  'default'     => '4.00',
                  'desc_tip'    => true
                  ),

            'valorMinimo'       => array(
                  'title'       => __( 'Valor Mínimo', 'loggi' ),
                  'type'        => 'number',
                  'description' => __( 'Formato 9.99. A Loggi cobra um valor mínimo de corrida no valor de R$ 14,90.', 'loggi' ),
                  'placeholder' => '9.99',
                  'default'     => '14.90',
                  'desc_tip'    => true
                  ),

            'valorKm'           => array(
                  'title'       => __( 'Valor por Km', 'loggi' ),
                  'type'        => 'number',
                  'description' => __( 'Formato 9.99', 'loggi' ),
                  'placeholder' => '9.99',
                  'default'     => '2.00',
                  'desc_tip'    => true
                  ),

             'round'             => array(
                  'title'       => __( 'Arredondar valor do frete?', 'loggi' ),
                  'type'        => 'checkbox',
                  'label'       => 'sim/não',
                  'description' => __( 'Arredonda o valor do frete para cima para evitar centavos. Ex: R$ 19,76 sobe para R$ 20,00', 'loggi' ),
                  'default'     => 'no',
                  'desc_tip'    => true
                  ),           

            'prazoEntrega'             => array(
                  'title'       => __( 'Prazo de Entrega', 'loggi' ),
                  'type'        => 'number',
                  'description' => __( 'Formato 9. Corresponde ao número de dias a ser exibido no carrinho e checkout.', 'loggi' ),
                  'placeholder' => '',
                  'default'     => '8',
                  'desc_tip'    => true
                  ),

            'contagemDosDias' => array(
              'title' => __( 'Contagem dos Dias', 'loggi' ),              
              'type' => 'select',
              'options' => array(
                    'dias_corridos' => 'Dias Corridos',
                    'dias_uteis' => 'Dias Úteis'
              ),
              'default' => 'dias_corridos'
            ),

             'maxL'             => array(
                  'title'       => __( 'Largura Máxima', 'loggi' ),
                  'type'        => 'number',
                  'description' => __( 'Formato 9.99 (em cm)', 'loggi' ),
                  'placeholder' => 'Largura Máxima',
                  'default'     => '60',
                  'desc_tip'    => true
                  ),

             'maxC'             => array(
                  'title'       => __( 'Comprimento Máximo', 'loggi' ),
                  'type'        => 'number',
                  'description' => __( 'Formato 9.99 (em cm)', 'loggi' ),
                  'placeholder' => 'Comprimento Máximo',
                  'default'     => '50',
                  'desc_tip'    => true
                  ),

             'maxA'             => array(
                  'title'       => __( 'Altura Máxima', 'loggi' ),
                  'type'        => 'number',
                  'description' => __( 'Formato 9.99 (em cm)', 'loggi' ),
                  'placeholder' => 'Altura Máxima',
                  'default'     => '47',
                  'desc_tip'    => true
                  ),

             'maxPeso'          => array(
                  'title'       => __( 'Peso Máximo', 'loggi' ),
                  'type'        => 'number',
                  'description' => __( 'Formato 9.99 (em kg)', 'loggi' ),
                  'placeholder' => 'Peso Máximo',
                  'default'     => '25',
                  'desc_tip'    => true
                  ),

             'observacoes' => array(
                  'title' => __( 'Observações', 'loggi' ),              
                  'description' => __( 'Observações aparecem logo após o prazo de entrega', 'loggi' ),
                  'type' => 'textarea',
                  'desc_tip'    => true
              ),

            );
        }
 
        /**
         * calculate_shipping function.
         *
         * @access public
         * @param mixed $package
         * @return void
         */
        public function calculate_shipping( $package = array() ) {
          
          $totalFrete = 0;
          
          foreach ( $package['contents'] as $item_id => $item ) {

            $shipping_class = $this->get_option('shipping_class');
            $product     = $item['data'];

            if ( $shipping_class !== $product->get_shipping_class() && !empty($shipping_class) ) {
               return false;
            }

            $quantity        = $item['quantity'];
            $weight          = $product->get_weight() * $quantity;
            $width           = $product->width * $quantity;
            $height          = $product->height * $quantity;
            $length          = $product->length * $quantity;
            $quantity        = $item['quantity'];
            
            $googleApiKey    = $this->get_option('googleApiKey');
            
            $round           = $this->get_option('round'); //yes / no

            $useVendorCep    = $this->get_option('useVendorCep'); //yes / no
            
            $cepOrig       = $this->get_option('cepOrig');
                        
            $cepDest         = WC()->customer->get_shipping_postcode();

            if (!verifyCep($cepOrig) || !verifyCep($cepDest)) { return false; } // abort if either CEP orgin or destination are invalid

            $custoFixo       = $this->get_option('custoFixo');
            $custoPercentual = $this->get_option('custoPercentual');

            $taxaColeta      = $this->get_option('taxaColeta');
            $taxaEntrega     = $this->get_option('taxaEntrega');
            $valorMinimo     = $this->get_option('valorMinimo');
            $valorKm         = $this->get_option('valorKm');
            
            $titulo      = $this->get_option('title');
            if ( $titulo == '' ){
              $titulo = $this->method_title;
            }               
            
            $prazoEntrega    = $this->get_option('prazoEntrega');
            $contagemDosDias = $this->get_option('contagemDosDias');
            
            $maxL            = $this->get_option('maxL');
            $maxC            = $this->get_option('maxC');
            $maxA            = $this->get_option('maxA');
            $maxPeso         = $this->get_option('maxPeso');

            $observacoes     = $this->get_option('observacoes'); // textarea
     
            $medidaL = $width;
            $medidaC = $length;
            $medidaA = $height;
            $peso    = $weight;
            












            // multiplicar aqui o peso e medidas

            $LAC    = array($medidaL,$medidaC,$medidaA);
            $maxLAC = array($maxL,$maxC,$maxA);
            
            if (!check_max_size($LAC, $maxLAC) || $peso > $maxPeso) :
              return false;
            endif;
          } 

          $geo_origin = get_geo_location($cepOrig, $googleApiKey);
          if ($geo_origin['status'] != "OK") { return false; }

          $geo_destination = get_geo_location($cepDest, $googleApiKey);          
          if ($geo_destination['status'] != "OK") { return false; }

          $distance = get_driving_distance($geo_origin['latitude'], $geo_origin['longitude'], $geo_destination['latitude'], $geo_destination['longitude'], $googleApiKey);
          if ($distance == 0) { return false; }

          $title = $titulo;



          // Gerar quantidade a partir dos limites máximos de peso e medida

          $totalFrete = (($distance) * $valorKm);
          $totalFrete = $totalFrete + $taxaColeta + $taxaEntrega + $custoFixo;

          $custoPercentualResult = round( ( $totalFrete / 100 ) * $custoPercentual, 2 );

          $totalFrete = $totalFrete + $custoPercentualResult;
          if ($totalFrete < $valorMinimo) {
            $totalFrete = $valorMinimo;
          }

          if ($round == "yes") {
            $totalFrete = ceiling($totalFrete, 1);
          }

          // Meta Data
          if ($prazoEntrega > 1) {
             if ($contagemDosDias == 'dias_corridos') { $textoDosDias = 'dias'; }
             else { $textoDosDias = 'dias úteis'; }
          }
          else {
            if ($contagemDosDias == 'dias_corridos') { $textoDosDias = 'dia'; }
            else { $textoDosDias = 'dia útil'; }
          }

          $debug_data = 
            "[br][br][strong]Modo de Depuração Ativado.[br]" .
            "Listagem dos Parâmetros Utilizados[/strong][br]" .

            "[br][strong]Título para Exibição: [/strong]"            . $title .            
            "[br][strong]Google API Key: [/strong]"                  . $googleApiKey .
            "[br][strong]CEP de Origem: [/strong]"                   . $cepOrig .
            "[br][strong]CEP de Destino: [/strong]"                  . $cepDest .
            "[br][strong]Arredondamento: [/strong]"                  . $round . 
            "[br][strong]Custo Fixo: [/strong]"                      . "R$ " . $custoFixo . 
            "[br][strong]Custo Percentual: [/strong]"                . $custoPercentual . "%" .
            "[br][strong]Custo Percentual Resultado: [/strong]"      . "R$ " . $custoPercentualResult .
            "[br][strong]Taxa de Coleta: [/strong]"                  . "R$ " . $taxaColeta . 
            "[br][strong]Taxa de Entrega: [/strong]"                 . "R$ " . $taxaEntrega . 
            "[br][strong]Valor por Km: [/strong]"                    . "R$ " . $valorKm . 
            "[br][strong]Texto do Prazo: [/strong]"                  . $prazoEntregaText . 

            "[br][strong]Largura Máxima: [/strong]"                  . $maxL . "cm" . 
            "[br][strong]Comprimento Máximo: [/strong]"              . $maxC . "cm" .
            "[br][strong]Altura Máxima: [/strong]"                   . $maxA . "cm" .
            "[br][strong]Peso Máximo: [/strong]"                     . $maxPeso . "Kg" .

            "[br][strong]Largura do Produto: [/strong]"              . $medidaL . "cm" . 
            "[br][strong]Comprimento do Produto: [/strong]"          . $medidaC . "cm" .
            "[br][strong]Altura do Produto: [/strong]"               . $medidaA . "cm" .
            "[br][strong]Peso do Produto: [/strong]"                 . $peso . "Kg" .

            "[br][strong]Quantidade: [/strong]"                      . $quantity .
            
            "[br][strong]Distância: [/strong]"                       . $distance . "m" .            
            "[br][strong]Geo Origem Status: [/strong]"               . $geo_origin['status'] . 
            "[br][strong]Geo Destino Status: [/strong]"              . $geo_destination['status'] .
            "[br][strong]Debug: [/strong]"                           . $debug
          ;
          //if ($debug != 'yes') { $debug_data = ''; }
          //echo $debug_data;
          /*
          Adicionar debug aos arquivos de LOG do WC de forma correta na próxima versão
          */

          $rate = array(
            'id'        => $this->id . $this->instance_id,
            'label'     => $title,                             
            'cost'      => $totalFrete,
            'calc_tax'  => 'per_item',
            'meta_data' => array("Prazo de Entrega"=>$prazoEntrega . ' ' . $textoDosDias, "Observações"=>$observacoes)
          );

          // Register the rate
          $this->add_rate( $rate );
        }
      }
    }
  }

  add_action( 'woocommerce_shipping_init', 'loggi_shipping_method_init' );

  function add_loggi_shipping_method( $methods ) {
    $methods['loggi_shipping_method'] = 'WC_loggi_Shipping_Method';
    return $methods;
  }

  add_filter( 'woocommerce_shipping_methods', 'add_loggi_shipping_method' );

  add_action( 'woocommerce_after_shipping_rate', 'add_loggi_rate_message', 1,2 );

  if (!function_exists('add_loggi_rate_message')) {
    function add_loggi_rate_message ( $method, $index ) {
      $my_method = $method->get_method_id();
      $meta      = $method->get_meta_data();        

      if ($my_method == 'loggi_shipping_method') {
        $message = "";
        if ($meta['Prazo de Entrega'] > 0){
          $message .= "<p id='loggi_shipping_method'><small>Entrega em <span class='loggi_shipping_method_dias'>" . $meta['Prazo de Entrega'] . "</span> " . $meta['Contagem dos Dias'];
        }        
        if ($meta['Observações'] != '') {
          $message .= "<br><span class='loggi_shipping_method_observacoes'>" . $meta['Observações'] . "</span></small></p>";
        }
        else {
          $message .= "</small></p>";
        }

        echo $message;

        $debug_data = $meta['Debug'];
        $debug_data = str_replace("[br]","<br>",$debug_data);
        $debug_data = str_replace("[strong]","<strong>",$debug_data);
        $debug_data = str_replace("[/strong]","</strong>",$debug_data);
        echo $debug_data;
      }
    }
  }
}

//*************************************** FUNCTIONS ***************************************//

if( !function_exists('get_geo_location') ) {
  function get_geo_location($string, $API_key){
   
    //$string = str_replace (" ", "+", urlencode($string));
    $details_url = "https://maps.googleapis.com/maps/api/geocode/json?address=".$string."+BR&sensor=false&key=".$API_key;

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $details_url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $response = json_decode(curl_exec($ch), true);

    // If Status Code is ZERO_RESULTS, OVER_QUERY_LIMIT, REQUEST_DENIED or INVALID_REQUEST

    /*
    URLs que funcionaram fora do plugin!
https://maps.googleapis.com/maps/api/geocode/json?address=04089-000+BR&sensor=false&key=AIzaSyDQpMMCc6fiVgTn6DZXEaiH4v90vIMTHI4
https://maps.googleapis.com/maps/api/geocode/json?address=05415-040+BR&sensor=false&key=AIzaSyDQpMMCc6fiVgTn6DZXEaiH4v90vIMTHI4
    */

    if ($response['status'] != 'OK') {
      return null;
    }
   
    $geometry = $response['results'][0]['geometry'];

    $array = array(
      'latitude' => $geometry['location']['lat'],
      'longitude' => $geometry['location']['lng'],
      'location_type' => $geometry['location_type'],
      'status' => $response['status']
    );
     return $array;
  } 
}

if( !function_exists('get_driving_distance') ) {
  function get_driving_distance($lat1, $long1, $lat2, $long2, $API_key) {

    $details_url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=" . $lat1 . "," . $long1 . "&destinations=" . $lat2 . "," . $long2 . "&mode=driving&sensor=false&key=".$API_key;
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $details_url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $response = json_decode(curl_exec($ch), true);

    return $response['rows'][0]['elements'][0]['distance']['value'] / 1000;  
  }
}

if( !function_exists('verifyCep') ) {
  function verifyCep($cep) {
    // retira espacos em branco
    $cep = trim($cep);
    // expressao regular para validar o cep
     if (preg_match('/[0-9]{5,5}([-]?[0-9]{3})?$/', $cep)) {
      return true;
    }
    else {
      return false;
    }
  }
}

if( !function_exists('ceiling') ) {
  function ceiling($number, $significance = 1) {
      return ( is_numeric($number) && is_numeric($significance) ) ? (ceil($number/$significance)*$significance) : false;
  }
}



/* Display Shipping meta-data on order notification
*/
if( !function_exists('action_woocommerce_order_details_after_order_table') ) {
    function action_woocommerce_order_details_after_order_table( $order ) { 
       echo '<style>
         .shipping_details {border-bottom: 1px solid #ccc; margin-bottom:30px;}
          . shipping_details p {margin-bottom:15px;}
       </style>';
       echo "<h3>Detalhes da Entrega</h3>";
        foreach ($order->data['shipping_lines'] as $item) {         
            echo "<div class='shipping_details'>";
            echo '<p><strong>Método de Frete</strong><br>' . $item['name'] . '</p>';
           
            foreach ($item->get_meta_data() as $item_child){
                $meta = $item_child->get_data();
                if ($meta['key'] == 'vendor_id') {
                    echo '<strong>Vendor:</strong> ' . get_the_author_meta( 'display_name', $meta['value'] ) . '<br>';
                }
                else {
                    echo '<p><strong>' . $meta['key'] . '</strong><br>' . $meta['value'] . '</p>';
                }
            }
            echo '</div>';       
        }
        
    }; 
    add_action( 'woocommerce_order_details_after_order_table', 'action_woocommerce_order_details_after_order_table', 10, 1 ); 
    add_action( 'woocommerce_email_after_order_table', 'action_woocommerce_order_details_after_order_table', 10, 1 ); 
}

/* Função para verificar limite de tamanho
*/
if( !function_exists('check_max_size') ) {
  function check_max_size($LAC, $maxLAC){
    sort($LAC);
    sort($maxLAC);
    if ($LAC[0] > $maxLAC[0]) {return false;}
    if ($LAC[1] > $maxLAC[1]) {return false;}
    if ($LAC[2] > $maxLAC[2]) {return false;}
    return true; 
  }       
}  