# Loggi para WooCommerce

Plugin de Método de Frete Loggi para WooCommerce.

Compatível com WooCommerce versão 3.7.0 e MarketPlaces.

[https://woocommerce-marketplace.com.br/](https://woocommerce-marketplace.com.br/)

## Changelog

**13/08/2019 - Versão 1.3.5**  
Detalhes da Entrega no Pedido  
Custo Fixo  
Custo Percentual  
Taxa de Coleta  
Arredondamento do Valor

**15/07/2019 - Versão 1.2.6**  
Cabeçalho com informações de compatibilidade de versões do WordPress e WooCommerce

**08/07/2019 - Versão 1.2.5**  
Suporte a Classes de Entrega

**02/07/2019 - Versão 1.2.0**  
Campos Exibição no site e Exibição no painel.

## Recursos

*   Suporte a Áreas de Entrega;
*   Suporte a Classes de Entrega;
*   Múltiplas Instâncias;
*   Observações;
*   Leitura a tabela CSV para retorno de valor, ICMS e prazo de entrega
*   Estimativa de Entrega;
*   Taxa de Manuseio;
*   Limite de Medidas;
*   Limite de Peso;
*   Títulos editáveis tanto no painel quanto no site;

## Configurações

![](readme.files/setup_01.jpg)  
_WooCommerce > Entrega > Editar_  

![](readme.files/setup_02.jpg)  
_Adicionar método de entrega_

![](readme.files/setup_03_loggi.jpg)  
_Selecionar método de entrega Loggi_

![](readme.files/setup_04_loggi.jpg)  
_Editar método de entrega Loggi_

![](readme.files/setup_05_loggi.jpg)  
_Preencher os parâmetros e Salvar alterações_

![](readme.files/setup_06.jpg)  
_**IMPORTANTE:** Método de frete será exibido para produtos que tenham as informações de Peso e Dimensões devidamente preenchidos._

## Market Places

Para configurar diferentes parâmetros para diferentes lojistas, utilize uma classe de entrega para cada lojista e crie uma instância do método para cada um.

Dica: No painel do WooCommerce, para melhor organização, o plugin dispõe do campo "Título de Exibição no Painel" para facilitar a identificação de diferentes instâncias do mesmo método.


© [WooCommerce Marketplace Brasil](https://woocommerce-marketplace.com.br/) - Todos os direitos reservados