<?php

// Register Custom Post Type
function pt_imoveis() {

	$labels = array(
		'name'                  => _x( 'Imóveis', 'Post Type General Name', 'fortram' ),
		'singular_name'         => _x( 'Imóvel', 'Post Type Singular Name', 'fortram' ),
		'menu_name'             => __( 'Imóveis', 'fortram' ),
		'name_admin_bar'        => __( 'Imóveis', 'fortram' ),
		'archives'              => __( 'Arquivo de Imóveis', 'fortram' ),
		'attributes'            => __( 'Atributos', 'fortram' ),
		'parent_item_colon'     => __( 'Pai', 'fortram' ),
		'all_items'             => __( 'Todo os Itens', 'fortram' ),
		'add_new_item'          => __( 'Adicionar Novo Imóvel', 'fortram' ),
		'add_new'               => __( 'Adicionar Imóvel', 'fortram' ),
		'new_item'              => __( 'Novo Imóvel', 'fortram' ),
		'edit_item'             => __( 'Editar Imóvel', 'fortram' ),
		'update_item'           => __( 'Atualizar Imóvel', 'fortram' ),
		'view_item'             => __( 'Ver Imóvel', 'fortram' ),
		'view_items'            => __( 'Ver Imóveis', 'fortram' ),
		'search_items'          => __( 'Buscar Imóveis', 'fortram' ),
		'not_found'             => __( 'Nada Encontrado', 'fortram' ),
		'not_found_in_trash'    => __( 'Nada Encontrado', 'fortram' ),
		'featured_image'        => __( 'Imagem Destacada', 'fortram' ),
		'set_featured_image'    => __( 'Definir Imagem Destacada', 'fortram' ),
		'remove_featured_image' => __( 'Remover Imagem Destacada', 'fortram' ),
		'use_featured_image'    => __( 'Usar como Imagem Destacada', 'fortram' )
	);
	$args = array(
		'label'                 => __( 'Imóvel', 'fortram' ),
		'description'           => __( 'Imóveis', 'fortram' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'thumbnail', 'custom-fields', 'page-attributes' ),
		'hierarchical'          => true,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-admin-home',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'imovel', $args );

}
add_action( 'init', 'pt_imoveis', 0 );

function imovel_to_imoveis() {
    if( is_post_type_archive( 'imovel' ) ) {
        wp_redirect( home_url( '/imoveis/' ), 301 );
        exit();
    }
}
add_action( 'template_redirect', 'imovel_to_imoveis' );


//METABOX


function metabox_imoveis( $meta_boxes ) {
	$prefix = 'imovel_';

	$meta_boxes[] = array(
		'id' => 'metabox_imoveis',
		'title' => esc_html__( 'Informações do Imóvel', 'fortram' ),
		'post_types' => 'imovel',
		'context' => 'after_editor',
		'priority' => 'default',
		'autosave' => 'true',
		'fields' => array(
			array(
				'id' => $prefix . 'negocio',
				'name' => esc_html__( 'Negócio', 'fortram' ),
				'type' => 'select',
				'placeholder' => esc_html__( 'Selecione', 'fortram' ),
				'options' => array(
					'Venda' => esc_html__( 'Venda', 'fortram' ),
                    'Aluguel' => esc_html__( 'Aluguel', 'fortram' ),
				),
			),
			array(
				'id' => $prefix . 'preco',
				'type' => 'text',
				'name' => esc_html__( 'Preço (R$)', 'fortram' ),
				'desc' => esc_html__( 'Defina o valor de venda total, ou o valor de aluguel mensal.', 'fortram' ),
			),
			array(
				'id' => $prefix . 'condominio',
				'type' => 'text',
				'name' => esc_html__( 'Condomínio (R$)', 'fortram' ),
			),
			array(
				'id' => $prefix . 'iptu',
				'type' => 'text',
				'name' => esc_html__( 'IPTU (R$)', 'fortram' ),
			),
			array(
				'id' => $prefix . 'whatsapp',
				'type' => 'text',
				'name' => esc_html__( 'WhatsApp', 'fortram' ),
				'desc' => esc_html__( 'Número do WhatsApp para contato.', 'fortram' ),
				'class' => 'ftr_celular',
			),
			array(
				'id' => $prefix . 'descricao',
				'type' => 'textarea',
				'name' => esc_html__( 'Descrição do Imóvel', 'fortram' ),
				'desc' => esc_html__( '250 caracteres restantes.', 'fortram' ),
				'rows' => 4,
				'class' => 'campo_descricao',
			),
			array(
				'id' => $prefix . 'area',
				'type' => 'number',
				'name' => esc_html__( 'Área (m2)', 'fortram' ),
			),
			array(
				'id' => $prefix . 'quartos',
				'type' => 'number',
				'name' => esc_html__( 'Quartos', 'fortram' ),
			),
			array(
				'id' => $prefix . 'banheiros',
				'type' => 'number',
				'name' => esc_html__( 'Banheiros', 'fortram' ),
			),
			array(
				'id' => $prefix . 'vagas',
				'type' => 'number',
				'name' => esc_html__( 'Vagas', 'fortram' ),
			),
			array(
				'id' => $prefix . 'bairro',
				'type' => 'text',
				'name' => esc_html__( 'Bairro', 'fortram' ),
			),
			array(
				'id' => $prefix . 'cidade',
				'type' => 'text',
				'name' => esc_html__( 'Cidade', 'fortram' ),
			),
			array(
				'id' => $prefix . 'estado',
				'name' => esc_html__( 'Estado', 'fortram' ),
				'type' => 'select',
				'placeholder' => esc_html__( 'Selecione', 'fortram' ),
				'options' => array(
					'AC' => esc_html__( 'Acre', 'fortram' ),
                    'AL' => esc_html__( 'Alagoas', 'fortram' ),
                    'AP' => esc_html__( 'Amapá', 'fortram' ),
                    'AM' => esc_html__( 'Amazonas', 'fortram' ),
                    'BA' => esc_html__( 'Bahia', 'fortram' ),
                    'CE' => esc_html__( 'Ceará', 'fortram' ),
                    'DF' => esc_html__( 'Distrito Federal', 'fortram' ),
                    'ES' => esc_html__( 'Espírito Santo', 'fortram' ),
                    'GO' => esc_html__( 'Goiás', 'fortram' ),
                    'MA' => esc_html__( 'Maranhão', 'fortram' ),
                    'MT' => esc_html__( 'Mato Grosso', 'fortram' ),
                    'MS' => esc_html__( 'Mato Grosso do Sul', 'fortram' ),
                    'MG' => esc_html__( 'Minas Gerais', 'fortram' ),
                    'PR' => esc_html__( 'Paraná', 'fortram' ),
                    'PB' => esc_html__( 'Paraíba', 'fortram' ),
                    'PA' => esc_html__( 'Pará', 'fortram' ),
                    'PE' => esc_html__( 'Pernambuco', 'fortram' ),
                    'PI' => esc_html__( 'Piauí', 'fortram' ),
                    'RN' => esc_html__( 'Rio Grande do Norte', 'fortram' ),
                    'RS' => esc_html__( 'Rio Grande do Sul', 'fortram' ),
                    'RJ' => esc_html__( 'Rio de Janeiro', 'fortram' ),
                    'RO' => esc_html__( 'Rondônia', 'fortram' ),
                    'RR' => esc_html__( 'Roraima', 'fortram' ),
                    'SC' => esc_html__( 'Santa Catarina', 'fortram' ),
                    'SE' => esc_html__( 'Sergipe', 'fortram' ),
                    'SP' => esc_html__( 'São Paulo', 'fortram' ),
                    'TO' => esc_html__( 'Tocantins', 'fortram' ),

				),
			),
			array(
				'id' => $prefix . 'endereco',
				'type' => 'text',
				'name' => esc_html__( 'Endereço', 'fortram' ),
				'desc' => esc_html__( 'Digite o endereço e selecione o local mais próximo no mapa.', 'fortram' ),
			),
			array(
                'id'            => $prefix . 'mapa',
                'name'          => 'Mapa',
                'type'          => 'osm',
                'zoom'          => 14,
                'height'        => '300px',
                'address_field' => 'imovel_endereco',
            ),
		),
	);

	return $meta_boxes;
}
add_filter( 'rwmb_meta_boxes', 'metabox_imoveis' );

function galeria_imovel( $meta_boxes ) {
	$prefix = 'imovel';

	$meta_boxes[] = array(
		'id' => 'galeria_imovel',
		'title' => esc_html__( 'Galeria do Imóvel', 'fortram' ),
		'post_types' => 'imovel',
		'context' => 'side',
		'priority' => 'default',
		'autosave' => 'false',
		'fields' => array(
			array(
				'id' => $prefix . 'galeria',
				'type' => 'image_advanced',
				'name' => esc_html__( 'Galeria de Imagens', 'fortram' ),
				'max_file_uploads' => '12',
				'max_status' => 'false',
			),
		),
	);

	return $meta_boxes;
}
add_filter( 'rwmb_meta_boxes', 'galeria_imovel' );

function single_imovel( $template ) {
    global $post;

    if ( 'imovel' === $post->post_type && locate_template( array( 'single-imovel.php' ) ) !== $template ) {
        return plugin_dir_path( __FILE__ ) . 'single-imovel.php';
    }

    return $template;
}

add_filter( 'single_template', 'single_imovel' );