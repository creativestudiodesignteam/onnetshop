<?php

function widget_imoveis(){
    $query_imoveis = new WP_Query( array( 
	'posts_per_page'    => -1,
	'post_type'         => 'imovel',
    'orderby'           => 'rand',
	'meta_query' => array(
        'relation' => 'AND',
        array(
            'key' => 'ocultar',
            'value' => '1',
            'compare' => '!='
            ),
        )
) );
 
if ( $query_imoveis->have_posts() ) {
?>
<style>
    .tag_negocio {
        background:#FED700;
        color:black;
        padding:0px 15px;
        font-weight:bold;
        text-transform:uppercase;
        position:relative;
        top:10px;
        left:10px;
        display:inline-block;
        border-radius:50px;
        font-size:10px;
    }
    .tag_preco {
        background:#429A45;
        color:black;
        padding:0px 15px;
        font-weight:bold;
        text-transform:uppercase;
        position:relative;
        top:10px;
        left:10px;
        display:inline-block;
        border-radius:50px;
        font-size:13px;
    }
    .img_imovel {
        height:150px;
    }
    @media (max-width:768px){
        .img_imovel {
            height:250px;
        }
    }
</style>
<div class="row">
<?php
    $i = 1;
	while ( $query_imoveis->have_posts() && $i <= 4) {
		$query_imoveis->the_post();
		$featured_img_url = get_the_post_thumbnail_url(get_the_ID(),'full');
?>
    
		<div class="col-sm-3 col-xs-12" style="margin-bottom:20px;">
		    <a href="<?php echo get_the_permalink(); ?>">
		    <div style="background:rgba(0,0,0,0.03);">
                <div style="background:url('<?php echo $featured_img_url; ?>') center center;background-size:cover;" class="img_imovel">
                    <div class="tag_negocio"><?php echo rwmb_meta('imovel_negocio'); ?> &bull; R$<?php echo rwmb_meta('imovel_preco'); ?></div>
                </div>
                <div style="padding:15px;">
                    <h6 style="font-weight:bold;"><?php echo get_the_title(); ?></h6>
                    <p style="color:#333e48 !important; font-size:13px;line-height:1.3;"><?php echo rwmb_meta('imovel_bairro') . ' <br> <strong>' . rwmb_meta('imovel_cidade') . ' - ' . rwmb_meta('imovel_estado') . '</strong>'; ?></p>
                    
                     <div class="row" style="text-align:center;">
                        <div class="col-sm-3 col-xs-3">
                            <i class="fas fa-ruler-combined" style="font-size:15px;color:#fed700;margin-bottom:2px;"></i><br>
                            <span style="font-size:12px;color:#333e48 !important;"><?php echo rwmb_meta('imovel_area'); ?>m²</span>
                        </div>
                        
                        <div class="col-sm-3 col-xs-3">
                            <i class="fa fa-bed" style="font-size:15px;color:#fed700;margin-bottom:2px;"></i><br>
                            <span style="font-size:12px;color:#333e48 !important;"><?php echo rwmb_meta('imovel_quartos'); ?></span>
                        </div>
                        
                        <div class="col-sm-3 col-xs-3">
                            <i class="fa fa-shower" style="font-size:15px;color:#fed700;margin-bottom:2px;"></i><br>
                            <span style="font-size:12px;color:#333e48 !important;"><?php echo rwmb_meta('imovel_banheiros'); ?></span>
                        </div>
                        
                        <div class="col-sm-3 col-xs-3">
                            <i class="fa fa-car" style="font-size:15px;color:#fed700;margin-bottom:2px;"></i><br>
                            <span style="font-size:12px;color:#333e48 !important;"><?php echo rwmb_meta('imovel_vagas'); ?></span>
                        </div>
                    </div>
                </div>
		    </div>
		    </a>
		</div>
<?php
$i++;
	}
?>
</div>
<?php
	wp_reset_postdata();
} 
}

add_shortcode('widget_imoveis','widget_imoveis');


//WIDGET 1

function widget_imoveis_1(){
echo "<div style='display:block;'>";
    $query_imoveis = new WP_Query( array( 
	'posts_per_page'    => -1,
	'post_type'         => 'imovel',
    'orderby'           => 'rand',
	'meta_query' => array(
        'relation' => 'AND',
        array(
            'key' => 'ocultar',
            'value' => '1',
            'compare' => '!='
            ),
        )
) );
 
if ( $query_imoveis->have_posts() ) {
?>
<style>
    .tag_negocio {
        background:#FED700;
        color:black;
        padding:0px 15px;
        font-weight:bold;
        text-transform:uppercase;
        position:relative;
        top:10px;
        left:10px;
        display:inline-block;
        border-radius:50px;
        font-size:10px;
    }
    .tag_preco {
        background:#429A45;
        color:black;
        padding:0px 15px;
        font-weight:bold;
        text-transform:uppercase;
        position:relative;
        top:10px;
        left:10px;
        display:inline-block;
        border-radius:50px;
        font-size:13px;
    }
    .img_imovel {
        height:150px;
    }
    @media (max-width:768px){
        .img_imovel {
            height:250px;
        }
    }
</style>
<div class="row">
<?php
    $i = 1;
	while ( $query_imoveis->have_posts() && $i <= 1) {
		$query_imoveis->the_post();
		$featured_img_url = get_the_post_thumbnail_url(get_the_ID(),'full');
?>
    
		<div class="col-xs-12" style="margin-bottom:20px;">
		    <a href="<?php echo get_the_permalink(); ?>">
		    <div style="background:rgba(0,0,0,0.03);">
                <div style="background:url('<?php echo $featured_img_url; ?>') center center;background-size:cover;" class="img_imovel">
                    <div class="tag_negocio"><?php echo rwmb_meta('imovel_negocio'); ?> &bull; R$<?php echo rwmb_meta('imovel_preco'); ?></div>
                </div>
                <div style="padding:15px;">
                    <h6 style="font-weight:bold;"><?php echo get_the_title(); ?></h6>
                    <p style="color:#333e48 !important; font-size:13px;line-height:1.3;"><?php echo rwmb_meta('imovel_bairro') . ' <br> <strong>' . rwmb_meta('imovel_cidade') . ' - ' . rwmb_meta('imovel_estado') . '</strong>'; ?></p>
                    
                     <div class="row" style="text-align:center;">
                        <div class="col-sm-3 col-xs-3">
                            <i class="fas fa-ruler-combined" style="font-size:15px;color:#fed700;margin-bottom:2px;"></i><br>
                            <span style="font-size:12px;color:#333e48 !important;"><?php echo rwmb_meta('imovel_area'); ?>m²</span>
                        </div>
                        
                        <div class="col-sm-3 col-xs-3">
                            <i class="fa fa-bed" style="font-size:15px;color:#fed700;margin-bottom:2px;"></i><br>
                            <span style="font-size:12px;color:#333e48 !important;"><?php echo rwmb_meta('imovel_quartos'); ?></span>
                        </div>
                        
                        <div class="col-sm-3 col-xs-3">
                            <i class="fa fa-shower" style="font-size:15px;color:#fed700;margin-bottom:2px;"></i><br>
                            <span style="font-size:12px;color:#333e48 !important;"><?php echo rwmb_meta('imovel_banheiros'); ?></span>
                        </div>
                        
                        <div class="col-sm-3 col-xs-3">
                            <i class="fa fa-car" style="font-size:15px;color:#fed700;margin-bottom:2px;"></i><br>
                            <span style="font-size:12px;color:#333e48 !important;"><?php echo rwmb_meta('imovel_vagas'); ?></span>
                        </div>
                    </div>
                </div>
		    </div>
		    </a>
		</div>
<?php
$i++;
	}
?>
</div>
<?php
	wp_reset_postdata();
}
echo "</div>";
}

add_shortcode('widget_imoveis_1','widget_imoveis_1');