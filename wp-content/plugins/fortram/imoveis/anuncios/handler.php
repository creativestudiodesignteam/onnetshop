<?php
//COUNT COMPRADOS

function countComprados($id_p, $id_u){

// GET CURRENT USER ORDERS
   $customer_orders = wc_get_orders(
      array(
         'limit'    => -1,
         'status'   => array('completed'),
         'customer' => $id_u,
      )
   );
    
   // LOOP THROUGH ORDERS AND SUM QUANTITIES PURCHASED
   $count = 0;
   foreach ( $customer_orders as $customer_order ) {
      $order = wc_get_order( $customer_order->get_id() );
      $items = $order->get_items();
      foreach ( $items as $item ) {
         $product_id = $item->get_product_id();
         if ( $product_id == $id_p ) {
            $count = $count + absint( $item['qty'] );
         }
      }
   }
   
   return $count;
}

function updateCountImoveis(){

    $users = get_users( array( 'fields' => array( 'ID' ) ) );
    $options = get_option( 'ftr_settings' );
    $id_p = $options['ftr_id_imovel'];
    // loop trough each author
    foreach ($users as $user)
    {
        update_user_meta( $user->ID, 'qtde_imoveis_comprados', intval( countComprados($id_p, $user->ID) ) );

        //POSTADOS
        $args = array(
            'post_type' => 'imovel',
            'author'    => $user->ID,
            'post_status' => array('publish', 'pending', 'draft', 'auto-draft', 'future', 'private', 'inherit', 'trash'),
            'posts_per_page' => -1
        );
    
        $query = new WP_Query($args);
    
        $count = $query->found_posts;
        update_user_meta( $user->ID, 'qtde_imoveis_postados', intval( $count ) );
    }

}

add_action('wp_head','updateCountImoveis');
add_action('admin_head','updateCountImoveis');