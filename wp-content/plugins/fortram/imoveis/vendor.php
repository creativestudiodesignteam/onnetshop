<?php
add_action('wcmp_init', 'after_wcmp_init');
function after_wcmp_init() {
	// add a setting field to wcmp endpoint settings page
	add_action('settings_vendor_general_tab_options', 'add_custom_endpoint_option');
	// save setting option for custom endpoint
	add_filter('settings_vendor_general_tab_new_input', 'save_custom_endpoint_option', 10, 2);
	// add custom endpoint
	add_filter('wcmp_endpoints_query_vars', 'add_wcmp_endpoints_query_vars');
	// add custom menu to vendor dashboard
	add_filter('wcmp_vendor_dashboard_nav', 'add_tab_to_vendor_dashboard');
	// display content of custom endpoint
	add_action('wcmp_vendor_dashboard_anuncios_endpoint', 'custom_menu_endpoint_content');
}

function add_custom_endpoint_option($settings_tab_options) {
	$settings_tab_options['sections']['wcmp_vendor_general_settings_endpoint_section']['fields']['wcmp_anuncios_endpoint'] = array('title' => __('Anúncios', 'dc-woocommerce-multi-vendor'), 'type' => 'text', 'id' => 'wcmp_anuncios_endpoint', 'label_for' => 'wcmp_anuncios_endpoint', 'name' => 'wcmp_anuncios_endpoint', 'hints' => __('Gerenciar anúncios no site.', 'dc-woocommerce-multi-vendor'), 'placeholder' => 'anuncios');
	return $settings_tab_options;
}

function save_custom_endpoint_option($new_input, $input) {
	if (isset($input['wcmp_anuncios_endpoint']) && !empty($input['wcmp_anuncios_endpoint'])) {
		$new_input['wcmp_anuncios_endpoint'] = sanitize_text_field($input['wcmp_anuncios_endpoint']);
	}
	return $new_input;
}
function add_wcmp_endpoints_query_vars($endpoints) {
	$endpoints['anuncios'] = array(
		'label' => __('Anúncios', 'dc-woocommerce-multi-vendor'),
		'endpoint' => get_wcmp_vendor_settings('wcmp_anuncios_endpoint', 'vendor', 'general', 'anuncios')
	);
	return $endpoints;
}
function add_tab_to_vendor_dashboard($nav) {
	$nav['custom_wcmp_nenu'] = array(
		'label' => __('Anúncios', 'dc-woocommerce-multi-vendor'), // menu label
		'url' => wcmp_get_vendor_dashboard_endpoint_url('anuncios'), // menu url
		'capability' => true, // capability if any
		'position' => 75, // position of the menu
		'submenu' => array(), // submenu if any
		'link_target' => '_self',
		'nav_icon' => 'wcmp-font ico-store-settings-icon', // menu icon
	);
	return $nav;
}
function custom_menu_endpoint_content(){
	echo '<div class="wrap_anuncios">';
	echo '<p><strong><big>Gerenciar espaços de anúncios:</big></strong></p>
	<ul>
	    <li><a href="'.get_site_url().'/imoveis/gerenciar/">Imóveis</a> ATIVOS: '.countAtivos("imovel").' <span>|</span> DISPONÍVEIS: '.countDisponiveis("imoveis").' <span>|</span> EXPIRADOS: '.countExpirados("imovel").'</li>
	    <li><a href="'.get_site_url().'/servicos/gerenciar/">Serviços</a> ATIVOS: '.countAtivos("servico").' <span>|</span> DISPONÍVEIS: '.countDisponiveis("servicos").' <span>|</span> EXPIRADOS: '.countExpirados("servico").'</li>
	    <li><a href="'.get_site_url().'/usados/gerenciar/">Produtos Usados</a> ATIVOS: '.countAtivos("usado").' <span>|</span> DISPONÍVEIS: '.countDisponiveis("usados").' <span>|</span> EXPIRADOS: '.countExpirados("usado").'</li>
	</ul>';
	echo '<p><a class="link_btn" target="_blank" href="'.get_site_url().'/comprar-espaco/">COMPRAR ESPAÇOS</a></p>';
	echo '</div>';
}