<?php
function front_servico(){
    
if ( is_user_logged_in() ) {
    
} else {
    wp_redirect( get_site_url() . '/painel/' ); exit;
}

$args = array(
    'post_type' => 'servico',
    'author'    => get_current_user_id(),
    'post_status' => array('publish', 'pending', 'draft', 'auto-draft', 'future', 'private', 'inherit', 'trash'),
    'posts_per_page' => -1
);

$query = new WP_Query($args);

$count = $query->found_posts;


$args2 = array(
    'post_type' => 'servico',
    'author'    => get_current_user_id(),
    'post_status' => array('publish'),
    'posts_per_page' => -1
);

$query2 = new WP_Query($args2);

$count2 = $query2->found_posts;

    
	$qtde_postados = get_the_author_meta( 'qtde_servicos_postados', get_current_user_id() );
	$qtde_comprados = get_the_author_meta( 'qtde_servicos_comprados', get_current_user_id() );
	
	$qtde_postar = $qtde_comprados - $qtde_postados;
	
if(isset($_COOKIE['msg_s']) && $_COOKIE['msg_s'] == 1) {
?>
<div class="msg_onet" style="background:rgba(0,0,0,0.08);border-left:4px solid #FED700;padding:5px 15px;margin-bottom:10px;">O anúncio foi publicado/alterado com sucesso.</div>
<?php
setcookie('msg_s', '0', time() + (5000), "/"); // 86400 = 1 day
}
?>
<h4><strong>Anúncios de Serviços</strong></h4>
<?php if($count >= 0){ ?>
<p>Você já anunciou <strong><?php echo $count; ?></strong>* <?php if($count == 1){ echo 'serviço'; } else { echo 'serviços'; } ?>, <strong><?php if($count2 == 1){ echo 'possui ' . $count2 . ' anúncio ativo'; } if($count2 > 1) { echo 'possui ' . $count2 . ' anúncios ativos'; } if($count2 == '0'){ echo 'atualmente não possui anúncios ativos'; }  ?></strong>, e ainda pode anunciar <strong><?php echo $qtde_postar; ?></strong>.<br>
<a href="<?php echo get_site_url(); ?>/comprar-espaco/">Clique aqui para adquirir mais espaços para anunciar.</a></p>
<?php } ?>
<hr>
<?php if($qtde_postar >= 1){ ?><a style="background:#FED700;padding: 10px 15px;border-radius:5px;font-size: 15px;color: black !important;margin-bottom: 10px;display: inline-block; font-weight:bold;" href="<?php echo get_site_url(); ?>/servicos/novo/">CRIAR ANÚNCIO</a><br><?php } ?>
<?php if($count2 != 0){ echo do_shortcode('[mb_frontend_dashboard post_type="servico" edit_page="6040"]'); }?>
<br>
<small>*Esta contagem inclui anúncios já expirados.</small>
<?php
}

add_shortcode('front_servico','front_servico');

function front_add_servico(){
    
if ( is_user_logged_in() ) {
    
} else {
    wp_redirect( get_site_url() . '/painel/' ); exit;
}

	$qtde_postados = get_the_author_meta( 'qtde_servicos_postados', get_current_user_id() );
	$qtde_comprados = get_the_author_meta( 'qtde_servicos_comprados', get_current_user_id() );
	
	$qtde_postar = $qtde_comprados - $qtde_postados;
	
    if(!empty($_GET['rwmb_frontend_field_post_id'])){
        if (authorByPostID($_GET['rwmb_frontend_field_post_id']) == get_current_user_id() && getPostType($_GET['rwmb_frontend_field_post_id']) == 'servico'){
            echo do_shortcode('[mb_frontend_form post_type="servico" id="metabox_servicos" post_fields="title,thumbnail"]');
        } else {
            wp_redirect( get_site_url().'/servicos/gerenciar/' ); exit;
        }
    }
    if(empty($_GET['rwmb_frontend_field_post_id']) && $qtde_postar >= 1){
        echo do_shortcode('[mb_frontend_form post_type="servico" id="metabox_servicos" post_fields="title,thumbnail"]');
    }
    if(empty($_GET['rwmb_frontend_field_post_id']) && $qtde_postar < 1){
?>
<h5>Você não possui espaços para anunciar serviços.<br>
Você pode adquirir mais clicando no botão abaixo:</h5>
<a style="background:#FED700;padding: 10px 15px;border-radius:5px;font-size: 15px;color: black !important;margin-bottom: 10px;display: inline-block; font-weight:bold;" href="<?php echo get_site_url(); ?>/servicos/novo/">COMPRAR ESPAÇO PARA ANUNCIAR</a>
<?php
    }
    if(!empty($_GET['rwmb-form-submitted'])){
        echo 'Você será redirecionado.';
        $cookie_name = "msg_s";
        $cookie_value = "1";
        setcookie($cookie_name, $cookie_value, time() + (5000), "/"); // 86400 = 1 day
        wp_redirect( get_site_url().'/servicos/gerenciar/' ); exit;
    }
}

add_shortcode('front_add_servico','front_add_servico');