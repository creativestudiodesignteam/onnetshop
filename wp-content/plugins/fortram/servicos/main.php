<?php

require_once(dirname(__FILE__) . '/post_type.php');
require_once(dirname(__FILE__) . '/front.php');
require_once(dirname(__FILE__) . '/busca.php');
require_once(dirname(__FILE__) . '/widget.php');

require_once(dirname(__FILE__) . '/anuncios/main.php');

function servicos_sidebar() {
    register_sidebar(
        array (
            'name' => __( 'Serviços', 'fortram' ),
            'id' => 'servicos_sidebar',
            'description' => __( 'Página Serviços', 'fortram' ),
            'before_widget' => '<div class="widget-content" style="margin-bottom:30px;">',
            'after_widget' => "</div>",
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>',
        )
    );
}
add_action( 'widgets_init', 'servicos_sidebar' );


//SELECT CAGEGORIAS

function select_categorias($i){
    $cats = array(
                    'Academia' => esc_html__( 'Academia', 'fortram' ),
                    'Açougue' => esc_html__( 'Açougue', 'fortram' ),
                    'Adega' => esc_html__( 'Adega', 'fortram' ),
                    'Advocacia' => esc_html__( 'Advocacia', 'fortram' ),
					'Adestrador de Animais' => esc_html__( 'Adestrador de Animais', 'fortram' ),
                    'Agência de Marketing' => esc_html__( 'Agência de Marketing', 'fortram' ),
                    'Animação Digital' => esc_html__( 'Animação Digital', 'fortram' ),
                    'Animação de Festa' => esc_html__( 'Animação de Festa', 'fortram' ),
                    'Artista Profissional' => esc_html__( 'Artista Profissional', 'fortram' ),
                    'Arquiteto(a)' => esc_html__( 'Arquiteto(a)', 'fortram' ),
                    'Assistência Técnica' => esc_html__( 'Assistência Técnica', 'fortram' ),
                    'Aula de Pilates' => esc_html__( 'Aula de Pilates', 'fortram' ),
                    'Auto Escola' => esc_html__( 'Auto Escola', 'fortram' ),
                    'Auto Peças' => esc_html__( 'Auto peças', 'fortram' ),
                    'Babá' => esc_html__( 'Babá', 'fortram' ),
                    'Bar e Restaurante' => esc_html__( 'Bar e Restaurante', 'fortram' ),
                    'Balonismo' => esc_html__( 'Balonismo', 'fortram' ),
                    'Banca de Jornal' => esc_html__( 'Banca de Jornal', 'fortram' ),
                    'Banho e tosa' => esc_html__( 'Banho e tosa', 'fortram' ),
                    'Bicicletaria' => esc_html__( 'Bicicletaria', 'fortram' ),
                    'Brinquedos Festivos' => esc_html__( 'Brinquedos Festivos', 'fortram' ),
                    'Bufett’s' => esc_html__( 'Bufett’s', 'fortram' ),
                    'Cabeleireiros' => esc_html__( 'Cabeleireiros', 'fortram' ),
                    'Cafeteria' => esc_html__( 'Cafeteria', 'fortram' ),
                    'Confecção de Tecidos' => esc_html__( 'Confecção de Tecidos', 'fortram' ),
                    'Carreto' => esc_html__( 'Carreto', 'fortram' ),
                    'Contador' => esc_html__( 'Contador', 'fortram' ),
                    'Coreógrafo' => esc_html__( 'Coreógrafo', 'fortram' ),
                    'Costureira(o)' => esc_html__( 'Costureira(o)', 'fortram' ),
                    'Chocolateria' => esc_html__( 'Chocolateria', 'fortram' ),
                    'Clínica de Saúde ' => esc_html__( 'Clínica de Saúde ', 'fortram' ),
                    'Confeitaria' => esc_html__( 'Confeitaria', 'fortram' ),
                    'Consertos e Manutenção Geral' => esc_html__( 'Consertos e Manutenção Geral', 'fortram' ),
                    'Construtora' => esc_html__( 'Construtora', 'fortram' ),
                    'Criação de Jogos Digitais' => esc_html__( 'Criação de Jogos Digitais', 'fortram' ),
                    'Delivery' => esc_html__( 'Delivery', 'fortram' ),
                    'Dentista e Ortodontia' => esc_html__( 'Dentista e Ortodontia', 'fortram' ),
                    'Desenhista' => esc_html__( 'Desenhista', 'fortram' ),
                    'Design Gráfico' => esc_html__( 'Design Gráfico', 'fortram' ),
                    'Diarista' => esc_html__( 'Diarista', 'fortram' ),
                    'Distribuidora de Água Mineral' => esc_html__( 'Distribuidora de Água Mineral', 'fortram' ),
                    'Doceria' => esc_html__( 'Doceria', 'fortram' ),
                    'Educação' => esc_html__( 'Educação', 'fortram' ),
                    'Eletricista' => esc_html__( 'Eletricista', 'fortram' ),
                    'Encanador' => esc_html__( 'Encanador', 'fortram' ),
                    'Enfermeira(o)' => esc_html__( 'Enfermeira(o)', 'fortram' ),
                    'Escola de Cabeleireiros' => esc_html__( 'Escola de Cabeleireiros', 'fortram' ),
                    'Escola de Futebol' => esc_html__( 'Escola de Futebol', 'fortram' ),
                    'Escola de Idiomas' => esc_html__( 'Escola de Idiomas', 'fortram' ),
                    'Escola de Música' => esc_html__( 'Escola de Música', 'fortram' ),
                    'Esmalteria' => esc_html__( 'Esmalteria', 'fortram' ),
                    'Esteticista' => esc_html__( 'Esteticista', 'fortram' ),
                    'Estilista' => esc_html__( 'Estilista', 'fortram' ),
                    'Eletrônica' => esc_html__( 'Eletrônica', 'fortram' ),
                    'Engenharia' => esc_html__( 'Engenharia', 'fortram' ),
                    'Esfiharia' => esc_html__( 'Esfiharia', 'fortram' ),
                    'Fábrica' => esc_html__( 'Fábrica', 'fortram' ),
                    'Farmácia' => esc_html__( 'Farmácia', 'fortram' ),
                    'Fisioterapeuta' => esc_html__( 'Fisioterapeuta', 'fortram' ),
                    'Fonoaudiologia' => esc_html__( 'Fonoaudiologia', 'fortram' ),
                    'Floricultura' => esc_html__( 'Floricultura', 'fortram' ),
                    'Food-truck' => esc_html__( 'Food-truck', 'fortram' ),
                    'Fotógrafo' => esc_html__( 'Fotógrafo', 'fortram' ),
                    'Funerária' => esc_html__( 'Funerária', 'fortram' ),
                    'Gestão Comercial' => esc_html__( 'Gestão Comercial', 'fortram' ),
                    'Gestão Financeira' => esc_html__( 'Gestão Financeira', 'fortram' ),
                    'Gestão de Recursos Humanos' => esc_html__( 'Gestão de Recursos Humanos', 'fortram' ),
                    'Gestão de Seguros' => esc_html__( 'Gestão de Seguros', 'fortram' ),
                    'Gráfica' => esc_html__( 'Gráfica', 'fortram' ),
                    'Home care' => esc_html__( 'Home care', 'fortram' ),
                    'Hambúrgueria' => esc_html__( 'Hambúrgueria', 'fortram' ),
                    'Hospital' => esc_html__( 'Hospital', 'fortram' ),
                    'Hotel' => esc_html__( 'Hotel', 'fortram' ),
                    'Hotelaria' => esc_html__( 'Hotelaria', 'fortram' ),
                    'Informática' => esc_html__( 'Informática', 'fortram' ),
                    'Imobiliária' => esc_html__( 'Imobiliária', 'fortram' ),
                    'Jardineiro' => esc_html__( 'Jardineiro', 'fortram' ),
                    'Lava rápido' => esc_html__( 'Lava rápido', 'fortram' ),
                    'Lavanderia' => esc_html__( 'Lavanderia', 'fortram' ),
                    'Livraria' => esc_html__( 'Livraria', 'fortram' ),
                    'Locutor' => esc_html__( 'Locutor', 'fortram' ),
                    'Loja de Artigos de Pesca' => esc_html__( 'Loja de Artigos de Pesca', 'fortram' ),
                    'Loja de Calçados' => esc_html__( 'Loja de Calçados', 'fortram' ),
                    'Loja de Departamentos' => esc_html__( 'Loja de Departamentos', 'fortram' ),
                    'Loja de Eletrodomésticos' => esc_html__( 'Loja de Eletrodomésticos', 'fortram' ),
                    'Loja de Eletronicos' => esc_html__( 'Loja de Eletronicos', 'fortram' ),
                    'Loja de Fantasias' => esc_html__( 'Loja de Fantasias', 'fortram' ),
                    'Loja de Informática' => esc_html__( 'Loja de Informática', 'fortram' ),
                    'Loja de Materiais de construção' => esc_html__( 'Loja de Materiais de construção', 'fortram' ),
                    'Loja de Vestuários' => esc_html__( 'Loja de Vestuários', 'fortram' ),
                    'Lotérica' => esc_html__( 'Lotérica', 'fortram' ),
                    'Marido de Aluguel' => esc_html__( 'Marido de Aluguel', 'fortram' ),
                    'Marcenaria' => esc_html__( 'Marcenaria', 'fortram' ),
                    'Maquiador(a)' => esc_html__( 'Maquiador(a)', 'fortram' ),
                    'Marceneiro' => esc_html__( 'Marceneiro', 'fortram' ),
                    'Mecânico Particular' => esc_html__( 'Mecânico Particular', 'fortram' ),
                    'Médico Particular' => esc_html__( 'Médico Particular', 'fortram' ),
                    'Motel' => esc_html__( 'Motel', 'fortram' ),
                    'Motoboy' => esc_html__( 'Motoboy', 'fortram' ),
                    'Motorista Particular' => esc_html__( 'Motorista Particular', 'fortram' ),
                    'Músico' => esc_html__( 'Músico', 'fortram' ),
                    'Nutricionista' => esc_html__( 'Nutricionista', 'fortram' ),
                    'Oficina de Arquitetura' => esc_html__( 'Oficina de Arquitetura', 'fortram' ),
                    'Oficina de Funilaria' => esc_html__( 'Oficina de Funilaria', 'fortram' ),
                    'Oficina Mecânica' => esc_html__( 'Oficina Mecânica', 'fortram' ),
                    'ONG' => esc_html__( 'ONG', 'fortram' ),
                    'Padaria' => esc_html__( 'Padaria', 'fortram' ),
                    'Papelaria' => esc_html__( 'Papelaria', 'fortram' ),
                    'Pastelaria' => esc_html__( 'Pastelaria', 'fortram' ),
                    'Passadeira' => esc_html__( 'Passadeira', 'fortram' ),
                    'Passeador de animais' => esc_html__( 'Passeador de animais', 'fortram' ),
                    'Perfumaria' => esc_html__( 'Perfumaria', 'fortram' ),
                    'Petshop' => esc_html__( 'Petshop', 'fortram' ),
                    'Pedreiro' => esc_html__( 'Pedreiro', 'fortram' ),
                    'Personal Trainer' => esc_html__( 'Personal Trainer', 'fortram' ),
                    'Pintor' => esc_html__( 'Pintor', 'fortram' ),
                    'Pipoqueiro' => esc_html__( 'Pipoqueiro', 'fortram' ),
                    'Pizzaria' => esc_html__( 'Pizzaria', 'fortram' ),
                    'Ponto de Taxi' => esc_html__( 'Ponto de Taxi', 'fortram' ),
                    'Posto de Gasolina' => esc_html__( 'Posto de Gasolina', 'fortram' ),
                    'Produtora de Eventos' => esc_html__( 'Produtora de Eventos', 'fortram' ),
                    'Produtor Audiovisual' => esc_html__( 'Produtor Audiovisual', 'fortram' ),
                    'Professor Particular' => esc_html__( 'Professor Particular', 'fortram' ),
                    'Professor(a)Yoga' => esc_html__( 'Professor(a)Yoga', 'fortram' ),
                    'Professor de Artes Marciais' => esc_html__( 'Professor de Artes Marciais', 'fortram' ),
                    'Programador' => esc_html__( 'Programador', 'fortram' ),
                    'Quiropraxia' => esc_html__( 'Quiropraxia', 'fortram' ),
                    'Radialista' => esc_html__( 'Radialista', 'fortram' ),
                    'Relojoarias' => esc_html__( 'Relojoarias', 'fortram' ),
                    'Restaurante' => esc_html__( 'Restaurante', 'fortram' ),
                    'Restaurante Vegano' => esc_html__( 'Restaurante Vegano', 'fortram' ),
                    'Salgados' => esc_html__( 'Salgados', 'fortram' ),
 	 	 	        'Sapataria' => esc_html__( 'Sapataria', 'fortram' ),
 	 	 	        'Serviços de Fretes online' => esc_html__( 'Serviços de Fretes online', 'fortram' ),
                    'Secretário(a)' => esc_html__( 'Secretário(a)', 'fortram' ),
                    'Segurança Particular' => esc_html__( 'Segurança Particular', 'fortram' ),
                    'Segurança no Trabalho' => esc_html__( 'Segurança no Trabalho', 'fortram' ),
                    'Sorveteria' => esc_html__( 'Sorveteria', 'fortram' ),
                    'Tatuador' => esc_html__( 'Tatuador', 'fortram' ),
                    'Taxi Dog' => esc_html__( 'Taxi Dog', 'fortram' ),
                    'Técnico de Informática' => esc_html__( 'Técnico de Informática', 'fortram' ),
                    'Têxtil' => esc_html__( 'Têxtil', 'fortram' ),
                    'Turismo' => esc_html__( 'Turismo', 'fortram' ),
                    'Vidraceiro' => esc_html__( 'Vidraceiro', 'fortram' ),
                    'Veterinária' => esc_html__( 'Veterinária', 'fortram' ),








                    






        
	);
?>
<?php
    foreach($cats as $key => $value):
?>
        <option value="<?php echo $key; ?>" <?php if($i == $key){ echo 'selected'; }?>><?php echo $value; ?></option>
<?php
    endforeach;
?>
<?php
}

function busca_servicos(){
    
if(isset($_GET['categoria'])) {
    $categoria = $_GET['categoria'];
}    
if(isset($_GET['cidade'])) {
    $cidade = $_GET['cidade'];
}    
if(isset($_GET['estado'])) {
    $estado = $_GET['estado'];
}
?>
<form method="get" action="<?php get_site_url(); ?>/servicos/busca/">
    <div style="margin-bottom:10px;">
        <select name="categoria" style="padding:10px 15px;border-radius:30px; width:100%; border:1px solid #e0e0e0;">
            <option value="" selected>Categoria</option>
            <option value="" ></option>
            <?php echo select_categorias($i = $categoria); ?>
        </select>
    </div>
    <div style="margin-bottom:10px;">
        <input type="text" name="cidade" placeholder="Cidade" style="padding:10px 15px;border-radius:30px; width:100%; border:1px solid #e0e0e0;" <?php if(!empty($cidade)){ echo 'value="'.$cidade.'"'; } ?>>
    </div>
    <div style="margin-bottom:10px;">
        <select name="estado" style="padding:10px 15px;border-radius:30px; width:100%; border:1px solid #e0e0e0;">
            <option value="" selected>Estado</option>
            <option value="" ></option>
            <option value="AC" <?php if($estado == 'AC'){ echo 'selected'; } ?>>Acre</option>
            <option value="AL" <?php if($estado == 'AL'){ echo 'selected'; } ?>>Alagoas</option>
            <option value="AP" <?php if($estado == 'AP'){ echo 'selected'; } ?>>Amapá</option>
            <option value="AM" <?php if($estado == 'AM'){ echo 'selected'; } ?>>Amazonas</option>
            <option value="BA" <?php if($estado == 'BA'){ echo 'selected'; } ?>>Bahia</option>
            <option value="CE" <?php if($estado == 'CE'){ echo 'selected'; } ?>>Ceará</option>
            <option value="DF" <?php if($estado == 'DF'){ echo 'selected'; } ?>>Distrito Federal</option>
            <option value="ES" <?php if($estado == 'ES'){ echo 'selected'; } ?>>Espírito Santo</option>
            <option value="GO" <?php if($estado == 'GO'){ echo 'selected'; } ?>>Goiás</option>
            <option value="MA" <?php if($estado == 'MA'){ echo 'selected'; } ?>>Maranhão</option>
            <option value="MT" <?php if($estado == 'MT'){ echo 'selected'; } ?>>Mato Grosso</option>
            <option value="MS" <?php if($estado == 'MS'){ echo 'selected'; } ?>>Mato Grosso do Sul</option>
            <option value="MG" <?php if($estado == 'MG'){ echo 'selected'; } ?>>Minas Gerais</option>
            <option value="PA" <?php if($estado == 'PA'){ echo 'selected'; } ?>>Pará</option>
            <option value="PB" <?php if($estado == 'PB'){ echo 'selected'; } ?>>Paraíba</option>
            <option value="PR" <?php if($estado == 'PR'){ echo 'selected'; } ?>>Paraná</option>
            <option value="PE" <?php if($estado == 'PE'){ echo 'selected'; } ?>>Pernambuco</option>
            <option value="PI" <?php if($estado == 'PI'){ echo 'selected'; } ?>>Piauí</option>
            <option value="RJ" <?php if($estado == 'RJ'){ echo 'selected'; } ?>>Rio de Janeiro</option>
            <option value="RN" <?php if($estado == 'RN'){ echo 'selected'; } ?>>Rio Grande do Norte</option>
            <option value="RS" <?php if($estado == 'RS'){ echo 'selected'; } ?>>Rio Grande do Sul</option>
            <option value="RO" <?php if($estado == 'RO'){ echo 'selected'; } ?>>Rondônia</option>
            <option value="RR" <?php if($estado == 'RR'){ echo 'selected'; } ?>>Roraima</option>
            <option value="SC" <?php if($estado == 'SC'){ echo 'selected'; } ?>>Santa Catarina</option>
            <option value="SP" <?php if($estado == 'SP'){ echo 'selected'; } ?>>São Paulo</option>
            <option value="SE" <?php if($estado == 'SE'){ echo 'selected'; } ?>>Sergipe</option>
            <option value="TO" <?php if($estado == 'TO'){ echo 'selected'; } ?>>Tocantins</option>
        </select>
    </div>
    <div style="margin-bottom:10px; text-align:left;">
        <input type="submit" value="FILTRAR"> <?php if(!empty($negocio) || !empty($cidade) || !empty($estado)){ echo '<a href="'.get_site_url().'/servicos/" style="color:darkred; font-size:12px; margin-left:10px;">LIMPAR FILTROS</a>'; } ?>
    </div>
</form>
<?php
}

add_shortcode('busca_servicos','busca_servicos');

function remover_servicos() {
  global $wpdb;
  $wpdb->query($wpdb->prepare("UPDATE on_posts SET post_status='trash' WHERE post_type='servico' AND post_date < DATE_SUB(NOW(), INTERVAL 30 DAY);"));
}

add_action('wp_head','remover_servicos');
add_action('admin_head','remover_servicos');