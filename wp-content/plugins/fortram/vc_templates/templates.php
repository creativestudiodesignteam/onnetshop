<?php

// Register Custom Post Type
function ftr_templates_post_type() {

	$labels = array(
		'name'                  => _x( 'Blocos Dinâmicos', 'Post Type General Name', 'fortram' ),
		'singular_name'         => _x( 'Bloco Dinâmico', 'Post Type Singular Name', 'fortram' ),
		'menu_name'             => __( 'Blocos Dinâmicos', 'fortram' ),
		'name_admin_bar'        => __( 'Bloco Dinâmico', 'fortram' ),
	);
	$args = array(
		'label'                 => __( 'Bloco Dinâmico', 'fortram' ),
		'description'           => __( 'Blocos Dinâmicos', 'fortram' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor' ),
		'hierarchical'          => true,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => get_site_url() . '/wp-content/plugins/fortram/vc_templates/i-template-16x16.png',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => false,
		'exclude_from_search'   => true,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'ftr_template', $args );

}
add_action( 'init', 'ftr_templates_post_type', 0 );

function ftr_getTemplate_sc($atts){
    $aid = $atts['id'];
    $id = get_post($aid); 
    $check = get_post_type( $aid );
    if ($check == "ftr_template"){
    $content = $id->post_content;
    return do_shortcode($content);
    } else {
        return "not a template!!!";
    }
}

add_shortcode('ftr_template','ftr_getTemplate_sc');

