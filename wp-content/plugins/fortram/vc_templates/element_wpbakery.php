<?php


/*
Element Description: Directory
*/


// Element Class
class vcFtrGetTemplate extends WPBakeryShortCode {

    // Element Init
    function __construct() {
        add_action( 'vc_before_init', array( $this, 'vc_FtrGetTemplate_mapping' ) );
    }

    // Element Mapping
    public function vc_FtrGetTemplate_mapping() {

$link_ftr_templates = get_site_url() . "/wp-admin/edit.php?post_type=ftr_template";
$pages_array = array('Selecionar');
$ftr_args = array(
	'sort_order' => 'asc',
	'sort_column' => 'post_title',
	'post_type' => 'ftr_template',
	'post_status' => 'publish'
); 
$get_pages = get_pages($ftr_args);
foreach ( $get_pages as $page ) {
    $pages_array[$page->post_title] = esc_attr( $page->ID );
}

        // Stop all if VC is not enabled
        if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
        }

        // Map the block with vc_map()
        vc_map(
            array(
                'name' => __('Blocos Dinâmicos', 'fortram'),
                'base' => 'ftr_template',
                'class' => 'ftr-template',
                'description' => __('Blocos Dinâmicos by Fortram', 'fortram'),
                'category' => __('Fortram', 'fortram'),
                'icon' => get_site_url() . '/wp-content/plugins/fortram/vc_template/i-template.png',
                'params' => array(
                    
                    array(
                          'param_name'    => 'id',
                          'type'          => 'dropdown',
                          'value'         => $pages_array, // here I'm stuck
                          'heading'       => __('Selecione um Template', 'fortram'),
                          'description'   => '',
                          'holder'        => 'div',
                          'class' => 'ftrgt_id',
                          'description' => __( 'Você pode criar novos templates <a target="_blank" href="' . $link_ftr_templates . '">aqui</a>.', 'fortram' )
                    ),

                ),
            )
        );

    }

} // End Element Class

// Element Class Init
new vcFtrGetTemplate();