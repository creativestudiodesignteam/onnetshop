<?php

function ocultar_anuncio_mb( $meta_boxes ) {
	$prefix = '';

	$meta_boxes[] = array(
		'id' => 'ocultar',
		'title' => esc_html__( 'Visibilidade do Anúncio', 'fortram' ),
		'post_types' => array('imovel', 'usado', 'servico' ),
		'context' => 'side',
		'priority' => 'high',
		'autosave' => 'true',
		'fields' => array(
			array(
				'id' => $prefix . 'ocultar',
				'name' => esc_html__( 'Ocultar Anúncio?', 'fortram' ),
				'type' => 'checkbox',
				'desc' => esc_html__( 'Marcando essa opção, o anúncio deixa de ser público. Marcar essa opção não adiará a data de expiração. Você pode ocultar e exibir novamente o anúncio dentro do prazo.', 'fortram' ),
			),
		),
	);

	return $meta_boxes;
}
add_filter( 'rwmb_meta_boxes', 'ocultar_anuncio_mb' );

function btn_ocultar($id){
    $ocultar = get_post_meta( $id, 'ocultar', true );
    
    if($ocultar == "0"){
?>
    <a href="<?php echo get_site_url(); ?>/ocultar-anuncio/?id=<?php echo $id; ?>" title="<?php esc_html_e( 'Ocultar', 'mb-frontend-submission' ) ?>">
        <i class="fa fa-eye-slash"></i>
    </a>
<?php
    }
    if($ocultar == "1"){
?>
    <a href="<?php echo get_site_url(); ?>/ocultar-anuncio/?id=<?php echo $id; ?>" title="<?php esc_html_e( 'Exibir', 'mb-frontend-submission' ) ?>">
        <i class="fa fa-eye"></i>
    </a>
<?php
    }
}

function tag_ocultar($id){
    $ocultar = get_post_meta( $id, 'ocultar', true );
    
    if($ocultar == "1"){
?>
    <div style="display:inline-block;font-size:10px;font-weight:bold;color:white;background:darkred;padding:4px;border-radius:3px;">ANÚNCIO OCULTO</div>
<?php
    }
}

function update_ocultar(){
    if(!empty($_GET['id'])){
        $id = $_GET['id'];
    } else {
        wp_redirect( get_site_url() ); exit;
    }

    $author_id = get_post_field ('post_author', $id);
    $current_id = get_current_user_id();
    if(!empty($id) && $author_id == $current_id) {
    
        $ocultar = get_post_meta( $id, 'ocultar', true );
        
        if($ocultar == "0"){
            update_post_meta( $id, 'ocultar', '1' );
        }
        if($ocultar == "1"){
            update_post_meta( $id, 'ocultar', '0' );
        }
        
        $check = get_post_type( $id );
        if($check == "imovel"){
            $type = "imoveis";
        }
        if($check == "usado"){
            $type = "usados";
        }
        if($check == "servico"){
            $type = "servicos";
        }
        wp_redirect( get_site_url().'/'. $type . '/gerenciar/' ); exit;
    
    } else {
        wp_redirect( get_site_url() ); exit;
    }
}

add_shortcode('content_ocultar_anuncio','update_ocultar');