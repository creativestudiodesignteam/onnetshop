<?php
function updateCountUsados(){

    $users = get_users( array( 'fields' => array( 'ID' ) ) );
    $options = get_option( 'ftr_settings' );
    $id_p = $options['ftr_id_produto'];
    // loop trough each author
    foreach ($users as $user)
    {
        update_user_meta( $user->ID, 'qtde_usados_comprados', intval( countComprados($id_p, $user->ID) ) );

        //POSTADOS
        $args = array(
            'post_type' => 'usado',
            'author'    => $user->ID,
            'post_status' => array('publish', 'pending', 'draft', 'auto-draft', 'future', 'private', 'inherit', 'trash'),
            'posts_per_page' => -1
        );
    
        $query = new WP_Query($args);
    
        $count = $query->found_posts;
        update_user_meta( $user->ID, 'qtde_usados_postados', intval( $count ) );
    }

}

add_action('wp_head','updateCountUsados');
add_action('admin_head','updateCountUsados');