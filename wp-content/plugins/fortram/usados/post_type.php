<?php

// Register Custom Post Type
function pt_usados() {

	$labels = array(
		'name'                  => _x( 'Produtos Usados', 'Post Type General Name', 'fortram' ),
		'singular_name'         => _x( 'Produto Usado', 'Post Type Singular Name', 'fortram' ),
		'menu_name'             => __( 'Produtos Usados', 'fortram' ),
		'name_admin_bar'        => __( 'Produtos Usados', 'fortram' ),
		'archives'              => __( 'Arquivo de Produtos Usados', 'fortram' ),
		'attributes'            => __( 'Atributos', 'fortram' ),
		'parent_item_colon'     => __( 'Pai', 'fortram' ),
		'all_items'             => __( 'Todo os Itens', 'fortram' ),
		'add_new_item'          => __( 'Adicionar Novo Produto Usado', 'fortram' ),
		'add_new'               => __( 'Adicionar Produto Usado', 'fortram' ),
		'new_item'              => __( 'Novo Produto Usado', 'fortram' ),
		'edit_item'             => __( 'Editar Produto Usado', 'fortram' ),
		'update_item'           => __( 'Atualizar Produto Usado', 'fortram' ),
		'view_item'             => __( 'Ver Produto Usado', 'fortram' ),
		'view_items'            => __( 'Ver Produtos Usados', 'fortram' ),
		'search_items'          => __( 'Buscar Produtos Usados', 'fortram' ),
		'not_found'             => __( 'Nada Encontrado', 'fortram' ),
		'not_found_in_trash'    => __( 'Nada Encontrado', 'fortram' ),
		'featured_image'        => __( 'Imagem Destacada', 'fortram' ),
		'set_featured_image'    => __( 'Definir Imagem Destacada', 'fortram' ),
		'remove_featured_image' => __( 'Remover Imagem Destacada', 'fortram' ),
		'use_featured_image'    => __( 'Usar como Imagem Destacada', 'fortram' )
	);
	$args = array(
		'label'                 => __( 'Produto Usado', 'fortram' ),
		'description'           => __( 'Produtos Usados', 'fortram' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'thumbnail', 'custom-fields', 'page-attributes' ),
		'hierarchical'          => true,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-cart',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'usado', $args );

}
add_action( 'init', 'pt_usados', 0 );

function usado_to_usados() {
    if( is_post_type_archive( 'usado' ) ) {
        wp_redirect( home_url( '/usados/' ), 301 );
        exit();
    }
}
add_action( 'template_redirect', 'usado_to_usados' );


//METABOX


function metabox_usados( $meta_boxes ) {
	$prefix = 'usado_';

	$meta_boxes[] = array(
		'id' => 'metabox_usados',
		'title' => esc_html__( 'Informações do Produto Usado', 'fortram' ),
		'post_types' => 'usado',
		'context' => 'after_editor',
		'priority' => 'default',
		'autosave' => 'true',
		'fields' => array(
			array(
				'id' => $prefix . 'descricao',
				'type' => 'textarea',
				'name' => esc_html__( 'Descrição do Produto Usado', 'fortram' ),
				'desc' => esc_html__( '250 caracteres restantes.', 'fortram' ),
				'rows' => 4,
				'class' => 'campo_descricao',
			),
			array(
				'id' => $prefix . 'preco',
				'type' => 'text',
				'name' => esc_html__( 'Preço (R$)', 'fortram' ),
			),
			array(
				'id' => $prefix . 'cidade',
				'type' => 'text',
				'name' => esc_html__( 'Cidade', 'fortram' ),
			),
			array(
				'id' => $prefix . 'estado',
				'name' => esc_html__( 'Estado', 'fortram' ),
				'type' => 'select',
				'placeholder' => esc_html__( 'Selecione', 'fortram' ),
				'options' => array(
					'AC' => esc_html__( 'Acre', 'fortram' ),
                    'AL' => esc_html__( 'Alagoas', 'fortram' ),
                    'AP' => esc_html__( 'Amapá', 'fortram' ),
                    'AM' => esc_html__( 'Amazonas', 'fortram' ),
                    'BA' => esc_html__( 'Bahia', 'fortram' ),
                    'CE' => esc_html__( 'Ceará', 'fortram' ),
                    'DF' => esc_html__( 'Distrito Federal', 'fortram' ),
                    'ES' => esc_html__( 'Espírito Santo', 'fortram' ),
                    'GO' => esc_html__( 'Goiás', 'fortram' ),
                    'MA' => esc_html__( 'Maranhão', 'fortram' ),
                    'MT' => esc_html__( 'Mato Grosso', 'fortram' ),
                    'MS' => esc_html__( 'Mato Grosso do Sul', 'fortram' ),
                    'MG' => esc_html__( 'Minas Gerais', 'fortram' ),
                    'PR' => esc_html__( 'Paraná', 'fortram' ),
                    'PB' => esc_html__( 'Paraíba', 'fortram' ),
                    'PA' => esc_html__( 'Pará', 'fortram' ),
                    'PE' => esc_html__( 'Pernambuco', 'fortram' ),
                    'PI' => esc_html__( 'Piauí', 'fortram' ),
                    'RN' => esc_html__( 'Rio Grande do Norte', 'fortram' ),
                    'RS' => esc_html__( 'Rio Grande do Sul', 'fortram' ),
                    'RJ' => esc_html__( 'Rio de Janeiro', 'fortram' ),
                    'RO' => esc_html__( 'Rondônia', 'fortram' ),
                    'RR' => esc_html__( 'Roraima', 'fortram' ),
                    'SC' => esc_html__( 'Santa Catarina', 'fortram' ),
                    'SE' => esc_html__( 'Sergipe', 'fortram' ),
                    'SP' => esc_html__( 'São Paulo', 'fortram' ),
                    'TO' => esc_html__( 'Tocantins', 'fortram' ),

				),
			),
			array(
				'id' => $prefix . 'whatsapp',
				'type' => 'text',
				'name' => esc_html__( 'WhatsApp', 'fortram' ),
				'desc' => esc_html__( 'Número do WhatsApp para contato.', 'fortram' ),
				'class' => 'ftr_celular',
			),
			array(
				'id' => $prefix . 'tags',
				'type' => 'text',
				'name' => esc_html__( 'Tags', 'fortram' ),
				'desc' => esc_html__( 'Insira tags que auxiliarão na busca do seu produto. Separe-as utilizando vírgula.', 'fortram' ),
			),
		),
	);

	return $meta_boxes;
}
add_filter( 'rwmb_meta_boxes', 'metabox_usados' );

function galeria_usado( $meta_boxes ) {
	$prefix = 'usado';

	$meta_boxes[] = array(
		'id' => 'galeria_usado',
		'title' => esc_html__( 'Galeria do Produto Usado', 'fortram' ),
		'post_types' => 'usado',
		'context' => 'side',
		'priority' => 'default',
		'autosave' => 'false',
		'fields' => array(
			array(
				'id' => $prefix . 'galeria',
				'type' => 'image_advanced',
				'name' => esc_html__( 'Galeria de Imagens', 'fortram' ),
				'max_file_uploads' => '12',
				'max_status' => 'false',
			),
		),
	);

	return $meta_boxes;
}
add_filter( 'rwmb_meta_boxes', 'galeria_usado' );

function single_usado( $template ) {
    global $post;

    if ( 'usado' === $post->post_type && locate_template( array( 'single-usado.php' ) ) !== $template ) {
        return plugin_dir_path( __FILE__ ) . 'single-usado.php';
    }

    return $template;
}

add_filter( 'single_template', 'single_usado' );