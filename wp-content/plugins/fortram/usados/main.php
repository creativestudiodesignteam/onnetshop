<?php

require_once(dirname(__FILE__) . '/post_type.php');
require_once(dirname(__FILE__) . '/front.php');
require_once(dirname(__FILE__) . '/busca.php');
require_once(dirname(__FILE__) . '/widget.php');

require_once(dirname(__FILE__) . '/anuncios/main.php');

function usados_sidebar() {
    register_sidebar(
        array (
            'name' => __( 'Produtos Usados', 'fortram' ),
            'id' => 'usados_sidebar',
            'description' => __( 'Página Produtos Usados', 'fortram' ),
            'before_widget' => '<div class="widget-content" style="margin-bottom:30px;">',
            'after_widget' => "</div>",
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>',
        )
    );
}
add_action( 'widgets_init', 'usados_sidebar' );

function busca_usados(){
    
if(isset($_GET['tag'])) {
    $tag = $_GET['tag'];
}    
if(isset($_GET['cidade'])) {
    $cidade = $_GET['cidade'];
}    
if(isset($_GET['estado'])) {
    $estado = $_GET['estado'];
}
?>
<form method="get" action="<?php get_site_url(); ?>/usados/busca/">
    <div style="margin-bottom:10px;">
        <input type="text" name="tag" placeholder="Buscar por..." style="padding:10px 15px;border-radius:30px; width:100%; border:1px solid #e0e0e0;" <?php if(!empty($tag)){ echo 'value="'.$tag.'"'; } ?>>
    </div>
    <div style="margin-bottom:10px;">
        <input type="text" name="cidade" placeholder="Cidade" style="padding:10px 15px;border-radius:30px; width:100%; border:1px solid #e0e0e0;" <?php if(!empty($cidade)){ echo 'value="'.$cidade.'"'; } ?>>
    </div>
    <div style="margin-bottom:10px;">
        <select name="estado" style="padding:10px 15px;border-radius:30px; width:100%; border:1px solid #e0e0e0;">
            <option value="" selected>Estado</option>
            <option value=""></option>
            <option value="AC" <?php if($estado == 'AC'){ echo 'selected'; } ?>>Acre</option>
            <option value="AL" <?php if($estado == 'AL'){ echo 'selected'; } ?>>Alagoas</option>
            <option value="AP" <?php if($estado == 'AP'){ echo 'selected'; } ?>>Amapá</option>
            <option value="AM" <?php if($estado == 'AM'){ echo 'selected'; } ?>>Amazonas</option>
            <option value="BA" <?php if($estado == 'BA'){ echo 'selected'; } ?>>Bahia</option>
            <option value="CE" <?php if($estado == 'CE'){ echo 'selected'; } ?>>Ceará</option>
            <option value="DF" <?php if($estado == 'DF'){ echo 'selected'; } ?>>Distrito Federal</option>
            <option value="ES" <?php if($estado == 'ES'){ echo 'selected'; } ?>>Espírito Santo</option>
            <option value="GO" <?php if($estado == 'GO'){ echo 'selected'; } ?>>Goiás</option>
            <option value="MA" <?php if($estado == 'MA'){ echo 'selected'; } ?>>Maranhão</option>
            <option value="MT" <?php if($estado == 'MT'){ echo 'selected'; } ?>>Mato Grosso</option>
            <option value="MS" <?php if($estado == 'MS'){ echo 'selected'; } ?>>Mato Grosso do Sul</option>
            <option value="MG" <?php if($estado == 'MG'){ echo 'selected'; } ?>>Minas Gerais</option>
            <option value="PA" <?php if($estado == 'PA'){ echo 'selected'; } ?>>Pará</option>
            <option value="PB" <?php if($estado == 'PB'){ echo 'selected'; } ?>>Paraíba</option>
            <option value="PR" <?php if($estado == 'PR'){ echo 'selected'; } ?>>Paraná</option>
            <option value="PE" <?php if($estado == 'PE'){ echo 'selected'; } ?>>Pernambuco</option>
            <option value="PI" <?php if($estado == 'PI'){ echo 'selected'; } ?>>Piauí</option>
            <option value="RJ" <?php if($estado == 'RJ'){ echo 'selected'; } ?>>Rio de Janeiro</option>
            <option value="RN" <?php if($estado == 'RN'){ echo 'selected'; } ?>>Rio Grande do Norte</option>
            <option value="RS" <?php if($estado == 'RS'){ echo 'selected'; } ?>>Rio Grande do Sul</option>
            <option value="RO" <?php if($estado == 'RO'){ echo 'selected'; } ?>>Rondônia</option>
            <option value="RR" <?php if($estado == 'RR'){ echo 'selected'; } ?>>Roraima</option>
            <option value="SC" <?php if($estado == 'SC'){ echo 'selected'; } ?>>Santa Catarina</option>
            <option value="SP" <?php if($estado == 'SP'){ echo 'selected'; } ?>>São Paulo</option>
            <option value="SE" <?php if($estado == 'SE'){ echo 'selected'; } ?>>Sergipe</option>
            <option value="TO" <?php if($estado == 'TO'){ echo 'selected'; } ?>>Tocantins</option>
        </select>
    </div>
    <div style="margin-bottom:10px; text-align:left;">
        <input type="submit" value="FILTRAR"> <?php if(!empty($tag) || !empty($cidade) || !empty($estado)){ echo '<a href="'.get_site_url().'/usados/" style="color:darkred; font-size:12px; margin-left:10px;">LIMPAR FILTROS</a>'; } ?>
    </div>
</form>
<?php
}

add_shortcode('busca_usados','busca_usados');


function remover_usados() {
  global $wpdb;
  $wpdb->query($wpdb->prepare("UPDATE on_posts SET post_status='trash' WHERE post_type='usado' AND post_date < DATE_SUB(NOW(), INTERVAL 30 DAY);"));
}

add_action('wp_head','remover_usados');
add_action('admin_head','remover_usados');