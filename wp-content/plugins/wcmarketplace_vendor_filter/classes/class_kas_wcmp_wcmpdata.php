<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/**
 * Get all required data from wcmp
 *
 *
 * @link       http://mamukb.comm
 * @since      1.0.6
 *
 * @package    Kas_WCMP_Filter
 * @subpackage Kas_WCMP_filter/classes
 */

/**
 * The core plugin class.
 *
 *
 * @since      1.0.0
 * @package    Kas_WCMP_Filter
 * @subpackage Kas_WCMP_filter/classes
 * @author     Syed Muhammad Shafiq <shafiq_shaheen@hotmail.com>
 */
class Kas_WCMP_WCMPData {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $kas_filter    The ID of this plugin.
	 */
	private $kas_filter;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.6
	 * @param      string    $kas_filter       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */


	public function __construct( $kas_filter, $version ) {

		$this->kas_filter = $kas_filter;
		$this->version = $version;
		//$this->dokan_data = array();

	}

	/**
	 * Collect all required dokan seller data in assosiative array.
	 *
	 * @since    1.0.0
	 */
	public function kas_wcmp_data() {
		$data = array();
		// args to get all users
		$args = array(
	            'role' => '',
	            'meta_key' => '',
	            'meta_value' => '',
	            'meta_compare' => '',
	            'meta_query' => array(),
	            'date_query' => array(),
	            'include' => array(),
	            'exclude' => array(),
	            'orderby' => 'login',
	            'order' => 'ASC',
	            'offset' => '',
	            'search' => '',
	            'number' => '',
	            'count_total' => false,
	            'fields' => 'all',
	            'who' => ''
	            );

	            if (isset($role) AND ! empty($role)){
	            	$args['role'] = $role;
	            }

	            $kas_authors = get_users($args);
				global $WCMp;
		
	            foreach ($kas_authors as $user){
					
	            	
	            		// get vendor products
		            	$args = array(
						    'author'     =>  $user->data->ID,
						    'post_type'  => 'product',
		            	 	'post_status' => 'publish',
						);
						
						$author_posts = get_posts( $args );
						
						// sort categories 
						$category = array();
						foreach ( $author_posts as $post ) {
							
							$terms = get_the_terms( $post->ID, 'product_cat' );
							
							foreach ($terms as $term){
								if (!empty($term->name)) {
									if (!in_array($term->name, $category)) {
										array_push($category,$term->name);
									}
								}
							}
						}	
						
						$kas_usermeta = $WCMp->user->get_vendor_fields($user->data->ID);
	            	 	$store_link = trailingslashit(get_home_url()).trailingslashit('vendor').$kas_usermeta['vendor_page_slug']['value'];
						
	            	 if (!empty($kas_usermeta['vendor_page_title']['value']) && !empty($kas_usermeta['vendor_country']['value'])) {
		            		// create array of select values
		            		array_push($data, array(
				                'id' => $user->data->ID,
				            	'state' => $kas_usermeta['vendor_state']['value'],
				            	'country' => $kas_usermeta['vendor_country']['value'],
				                'city' => $kas_usermeta['vendor_city']['value'],
				                'zip' => $kas_usermeta['vendor_postcode']['value'],
	            				'category' => implode(',',$category),
				                'store_thumb' => $kas_usermeta['vendor_image']['value'],
				                'store_name' => $kas_usermeta['vendor_page_title']['value'],
				            	'store_link' => $store_link,
		            		));	

	            	}

	            }
	            return $data;
	}


	/**
	 * Collect all countries and in unique format.
	 *
	 * @since    1.0.0
	 */
	public function kas_wcmp_countries() {
		$countries = array();
		$data = $this->kas_wcmp_data();
			
		foreach ($data as $detail){
			// sort country
			if (!in_array($detail['country'], $countries)) {
				array_push($countries,$detail['country']);
			}
		}
		return $countries;
	}

	/**
	 * Collect all states and in unique format.
	 *
	 * @since    1.0.0
	 */
	public function kas_wcmp_states() {
		$states = array();
		$data = $this->kas_wcmp_data();
			
		foreach ($data as $detail){
			// sort country
			if (!in_array($detail['state'], $states)) {
				array_push($states,$detail['state']);
			}
		}
		return $states;
	}


	/**
	 * Collect all cities and in unique format.
	 *
	 * @since    1.0.0
	 */
	public function kas_wcmp_cities() {
		$cities = array();
		$data = $this->kas_wcmp_data();
			
		foreach ($data as $detail){
			// sort country
			if (!in_array($detail['city'], $cities)) {
				array_push($cities,$detail['city']);
			}
		}
		return $cities;
	}


	/**
	 * Collect all zipcodes and in unique format.
	 *
	 * @since    1.0.2
	 */
	public function kas_wcmp_zips() {
		$zips = array();
		$data = $this->kas_wcmp_data();
			
		foreach ($data as $detail){
			// sort country
			if (!in_array($detail['zip'], $zips)) {
				array_push($zips,$detail['zip']);
			}
		}
		return $zips;
	}


	/**
	 * Collect all category and in unique format.
	 *
	 * @since    1.2.0
	 */
	public function kas_wcmp_category() {
		$categories = array();
		$data = $this->kas_wcmp_data();
		
		foreach ($data as $detail){
			
			if (!empty($detail['category'])) {
				$catag = explode( ',',$detail['category']);
				array_unique($catag);
				foreach ($catag as $cat){
						
					// sort country
					if (!in_array($cat, $categories)) {
						array_push($categories,$cat);
					}						
						
				}
			}				
		}
		return $categories;
	}


	/**
	 * Collect all cities and in unique format.
	 *
	 * @since    1.0.0
	 */
	public function kas_wcmp_stores() {
		$stores = array();
		$data = $this->kas_wcmp_data();
			
		foreach ($data as $detail){
			array_push($stores,array($detail['store_link'],$detail['store_name']));
		}
		return $stores;
	}


}