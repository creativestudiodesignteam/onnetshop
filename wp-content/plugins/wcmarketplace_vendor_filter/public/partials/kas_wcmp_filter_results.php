<?php

/**
 * Provide a public-facing view
 *
 * This file is used to markup the public-facing aspects for filter form.
 *
* @link       http://kodeflow.com
 * @since      1.0.6
 *
 * @package    Kas_WCMP_Filter
 * @subpackage Kas_WCMP_Filter/public/partials
 */
?>
<!-- This file should primarily consist of HTML with a little bit of PHP. -->
<?php 
		// check if bootstrap enable
		if (get_option('kas-enable-bootstrap') == 1){
			
			$css_row = 'row';
			$css_col = 'col-sm-6 col-md-4';
			$css_thumb = 'thumbnail';
			$btn_css ="btn btn-default ";
			$css_image = 'img-responsive';
			$css_capt = 'caption';
		}else{
			$css_row = 'kas_row';
			$css_col = 'kas_col';
			$css_thumb = 'kas_thumbnail';
			$btn_css = 'kas_submit';
			$css_image = 'img-responsive';
			$css_capt = 'caption';
		}	
		       
if ( $args['id'] ) {
	
	global $WCMp;
    ?>

	    <div class="<?php echo $css_row; ?>" id="kas_result">
	        <?php
	        foreach ( $args['id'] as $seller ) {
	   
	            $store_info = $WCMp->user->get_vendor_fields($seller['id']);
	            
		        $store_name = isset( $store_info['vendor_page_title']['value'] ) ? esc_html( $store_info['vendor_page_title']['value'] ) : __( 'N/A', $this->kas_filter );
		        $store_url  = trailingslashit(get_home_url()).trailingslashit('vendor').$store_info['vendor_page_slug']['value'];

	            $banner_url  = $store_info['vendor_banner']['value'];
	            ?>

	            <div class="<?php echo $css_col; ?>">
	            <div class="<?php echo  $css_thumb; ?>" style="background: url('<?php echo $banner_url; ?>') no-repeat;">

					
	                    <div class="kas_overflow">
							<div class="<?php echo $css_capt; ?>">
								<h3><a href="<?php echo $store_url; ?>"><?php echo $store_name; ?></a></h3>
	
								<address>
	
									<?php if ( isset( $store_info['vendor_address_1']['value'] ) && !empty( $store_info['vendor_address_1']['value'] ) ) {
										echo $store_info['vendor_address_1']['value'].'<br>'.$store_info['vendor_address_2']['value'].'<br>'.$store_info['vendor_city']['value'].'<br>'.$store_info['vendor_state']['value'].'<br>'.$store_info['vendor_postcode']['value'];
									} ?>
	
									<?php if ( isset( $store_info['vendor_phone']['value'] ) && !empty( $store_info['vendor_phone']['value'] ) ) { ?>
										<br>
										<abbr title="<?php _e( 'Phone Number', $this->kas_filter ); ?>"><?php _e( 'P:', $this->kas_filter ); ?></abbr> <?php echo esc_html( $store_info['vendor_phone']['value'] ); ?>
									<?php } ?>
	
								</address>
								<p class="kas_button"><a class="<?php echo $btn_css;?>" href="<?php echo $store_url; ?>"><?php _e( 'Visit Store', $this->kas_filter ); ?></a></p>
							</div><!-- .caption -->
	                    </div>
	                </div> <!-- .thumbnail -->
	            </div> <!-- .single-seller -->
	        <?php } ?>
	    </div> <!-- .wcmp-seller-wrap -->

    <?php
    $user_count   = $args['count'];
    $num_of_pages = ceil( $user_count / 20 );

    if ( $num_of_pages > 1 ) {
        global $post;
        $pagination_base = str_replace( $post->ID, '%#%', esc_url( get_pagenum_link( $post->ID ) ) );  
		$paged    = max( 1, $wp_query->get( 'paged' ) );  	
    	
        echo '<div class="pagination-container clearfix">';
        $pagination_args = array(
            'current'   => $paged,
            'total'     => $num_of_pages,
            'base'      => $pagination_base,
            'type'      => 'array',
            'prev_text' => __( '&larr; Anterior',  $this->kas_filter ),
            'next_text' => __( 'Próximo &rarr;',  $this->kas_filter ),
        );



        $page_links = paginate_links( $pagination_args );

        if ( $page_links ) {
            $pagination_links  = '<div class="pagination-wrap">';
            $pagination_links .= '<ul class="pagination"><li>';
            $pagination_links .= join( "</li>\n\t<li>", $page_links );
            $pagination_links .= "</li>\n</ul>\n";
            $pagination_links .= '</div>';

            echo $pagination_links;
        }

        echo '</div>';
    }
    ?>

<?php } else { ?>
    <p class="error"><?php _e( 'No seller found!',$this->kas_filter); ?></p>
<?php } ?>		