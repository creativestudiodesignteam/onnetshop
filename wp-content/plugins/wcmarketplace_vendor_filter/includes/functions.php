<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       http://kodeflow.com
 * @since      1.0.0
 *
 * @package    Kas_WCMP_Filter
 * @subpackage Kas_WCMP_Filter/includes
 */


/**
 * search for given value in array
 * @since 1.0.2
 */
function kas_search_in_array($search, $array_key, $array) {
	$ret_arr = array();
	$total = count($array);

	for ($i = 0; $i < $total; $i++) {
		switch ($array_key) {
			case 'country':
				if ($array[$i]['country'] === $search) {
					array_push($ret_arr,$array[$i]);
				}
				break;
			case 'state':
				if ($array[$i]['state'] === $search) {
					array_push($ret_arr,$array[$i]);
				}
				break;
			case 'city':
				if ($array[$i]['city'] === $search) {
					array_push($ret_arr,$array[$i]);
				}
				break;
			case 'zip':
				if ($array[$i]['zip'] === $search) {
					array_push($ret_arr,$array[$i]);
				}
				break;
			default:
				return NULL;
				break;
		}
	}
	return $ret_arr;
}


/**
 * Unique for any field like id, name or num. 
 * 
 * @since 1.0.2
 */
function unique_multidim_array($array, $key) {
    $temp_array = array();
    $i = 0;
    $key_array = array();
   
    foreach($array as $val) {
        if (!in_array($val[$key], $key_array)) {
            $key_array[$i] = $val[$key];
            $temp_array[$i] = $val;
        }
        $i++;
    }
    return $temp_array;
}

