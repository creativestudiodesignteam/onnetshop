<p>
<?php
/**
 * @var $thepostid int the product id
 */
woocommerce_wp_checkbox(array(
    'id'    => '_arti_shipping_correios_disabled',
    'label' => __('Disable Correios shipping for this product', 'arti-marketplace-correios'),
));
?>
</p>
