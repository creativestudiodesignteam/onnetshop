<div class="inline-edit-group">
    <label class="manage_stock">
        <input type="checkbox" name="_arti_shipping_correios_disabled" value="1">
        <span class="checkbox-title"><?php esc_html_e( 'Disable Correios shipping for this product', 'arti-marketplace-correios' ); ?></span>
    </label>
</div>
