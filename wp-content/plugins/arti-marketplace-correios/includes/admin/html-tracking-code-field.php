<li>
    <label>
        <i><strong><?php _e('Vendor', 'arti-marketplace-correios')?></strong>: <?=$field_title?></i><br>
        <?php if(!$is_correios):?>
            <?php _e('URL (with HTTP or HTTPS, ex.: https://mytrackingsite.com/)', 'arti-marketplace-correios')?>
            <input type="url" id="correios_tracking_url_<?=$package_id?>" name="correios_tracking_urls[<?=$package_id?>]" value="<?=$correios_tracking_url?>" style="width: 100%;">
        <?php endif?>
        <?php _e('Tracking code:', 'arti-marketplace-correios')?>
        <input type="text" id="correios_tracking_code_<?=$package_id?>" name="correios_tracking_codes[<?=$package_id?>]" value="<?=$correios_tracking_code?>" style="width: 100%;">
    </label>
</li>
