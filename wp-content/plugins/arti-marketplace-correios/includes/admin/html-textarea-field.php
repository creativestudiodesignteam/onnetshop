<?php
/**
 * Textarea field view.
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}
?>
<textarea id='<?php echo esc_attr( $id ); ?>' name='<?php echo esc_attr( $id ); ?>' rows='7' cols='50' type='textarea'><?php echo esc_html( $current );?></textarea>
<?php if ( isset( $args['description'] ) ) : ?>
    <p class="description"><?php echo wp_kses_post( $args['description'] ); ?></p>
<?php endif;
