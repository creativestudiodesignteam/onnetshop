<div class="correios-tracking-code">
    <div class="correios-tracking-code__list">
        <ul>
            <?php

            foreach ($shipping_items as $package_id => $item) {

                $pacakge_data = (new WC_Order_Item_Shipping($package_id))->get_data();
                $is_correios  = arti_wcvc_is_correios_method(wc_get_order_item_meta($package_id, 'method_label'));
                $method_title = $pacakge_data['method_title'];
                $method_title = !empty($method_title) ? "($method_title)" : '';

                $correios_tracking_code = wc_get_order_item_meta($package_id, 'correios_tracking_code');
                $correios_tracking_url  = wc_get_order_item_meta($package_id, 'correios_tracking_url');

                $vendor_name = $vendor_provider->get_vendor_name($item['vendor_id']);

                $field_title = sprintf('%s %s', $vendor_name, $method_title);

                include 'html-tracking-code-field.php';
            }
            ?>
        </ul>
    </div>
    <?php submit_button(); ?>
</div>
