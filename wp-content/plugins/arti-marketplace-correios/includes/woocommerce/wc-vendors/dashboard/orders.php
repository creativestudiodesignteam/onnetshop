<?php
/**
 * The template for displaying the dashboard order
 *
 * Override this template by copying it to yourtheme/wc-vendors/dashboard/
 *
 * @package    WCVendors
 * @version    1.9.4
 */
?>

<script type="text/javascript">
jQuery(function () {
    jQuery('a.view-items').on('click', function (e) {
        e.preventDefault();
        var id = jQuery(this).closest('tr').data('order-id');

        if ( jQuery(this).text() == "<?php _e('Hide items', 'arti-marketplace-correios'); ?>" ) {
        	jQuery(this).text("<?php _e('View items', 'arti-marketplace-correios'); ?>");
    	} else {
        	jQuery(this).text("<?php _e('Hide items', 'arti-marketplace-correios'); ?>");
    	}

        jQuery("#view-items-" + id).fadeToggle();
    });

    jQuery('a.view-order-tracking').on('click', function (e) {
        e.preventDefault();
         var id = jQuery(this).closest('tr').data('order-id');
        jQuery("#view-tracking-" + id).fadeToggle();
    });
});
</script>

<h2><?php _e( 'Orders', 'arti-marketplace-correios' ); ?></h2>

<?php global $woocommerce; ?>

<?php if ( function_exists( 'wc_print_notices' ) ) { wc_print_notices(); } ?>

<table class="table table-condensed table-vendor-sales-report">
	<thead>
	<tr>
	<th class="product-header"><?php _e( 'Order', 'arti-marketplace-correios' ); ?></th>
	<th class="quantity-header"><?php _e( 'Address', 'arti-marketplace-correios' ) ?></th>
	<th class="shipping-header"><?php _e( 'Shipping', 'arti-marketplace-correios' ) ?></th>
	<th class="commission-header"><?php _e( 'Total', 'arti-marketplace-correios' ) ?></th>
	<th class="rate-header"><?php _e( 'Date', 'arti-marketplace-correios' ) ?></th>
	<th class="links-header"><?php _e( 'Links', 'arti-marketplace-correios' ) ?></th>
	</thead>
	<tbody>

	<?php  if ( !empty( $order_summary ) ) : $totals = 0;
			$user_id = get_current_user_id();

			$correios_shipping_methods = arti_wcvc_list_shipping_methods();

	 		foreach ( $order_summary as $order ) :

				$order = new WC_Order( $order->order_id );
				$order_id 		= ( version_compare( WC_VERSION, '2.7', '<' ) ) ? $order_id : $order->get_id();
				$valid_items = WCV_Queries::get_products_for_order( $order_id );
				$valid = array();
				$needs_shipping = false;

				$order_date = ( version_compare( WC_VERSION, '2.7', '<' ) ) ? $order->order_date : $order->get_date_created();

				$items = $order->get_items();

				foreach ($items as $key => $value) {
					if ( in_array( $value['variation_id'], $valid_items) || in_array( $value['product_id'], $valid_items ) ) {
						$valid[] = $value;
					}
				}

				$shipping_items = $order->get_shipping_methods();

				foreach($shipping_items as $id => $item) {

					$method_label = wc_get_order_item_meta($id, 'method_label');

					if(wc_get_order_item_meta($id, 'vendor_id') == $user_id) {
					    $needs_shipping = true;
					}
				}

				$package_per_vendor = arti_wcvc_current_provider()->get_package_by_vendor($order_id, $user_id);

				?>

				<tr id="order-<?php echo $order_id; ?>" data-order-id="<?php echo $order_id; ?>">
					<td><?php echo $order->get_order_number(); ?></td>
					<td><?php
						$address = esc_html( preg_replace( '#<br\s*/?>#i', ', ', $order->get_formatted_shipping_address() ) );
						echo apply_filters( 'wcvendors_dashboard_google_maps_link', '<a target="_blank" href="' . esc_url( 'http://maps.google.com/maps?&q=' . urlencode($address) . '&z=16' ) . '">'. $address .'</a>' ); ?>

					</td>
					<td>
						<?php
							$total_shiping = array_sum(wp_list_pluck($package_per_vendor, 'cost'));

							$give_shipping = get_user_meta( $user_id, 'wcv_give_vendor_shipping', true )
											|| WC_Vendors::$pv_options->get_option( 'give_shipping' );
							echo $give_shipping ? wc_price($total_shiping):'&mdash;';
						?>
					</td>
					<td>
					<?php
						$sum = WCV_Queries::sum_for_orders( array( $order_id ), array('vendor_id'=>get_current_user_id()) );
						$total = $sum[0]->line_total;
						$totals += $total;
						echo wc_price( $total );
					?>
					</td>
					<td><?php echo date_i18n(get_option( 'date_format' ), strtotime($order_date)); ?></td>
					<td>
	                <?php
					$order_actions = array(
						'view'		=> array(
							'class' 	=> 'view-items',
							'content'	=> __('View items', 'arti-marketplace-correios'),
						)
					);

					if ( $needs_shipping  ) {
						$order_actions['tracking'] = array(
							'class'		=> 'view-order-tracking',
							'content'	=> __( 'Shipping info', 'arti-marketplace-correios' )
						);
					}

					$order_actions = apply_filters( 'wcvendors_order_actions', $order_actions, $order );

					if ($order_actions) {
						$output = array();
						foreach ($order_actions as $key => $data) {
							$output[] = sprintf(
								'<a href="%s" id="%s" class="%s">%s</a>',
								(isset($data['url'])) ? $data['url'] : '#',
								(isset($data['id'])) ? $data['id'] : $key . '-' . $order_id,
								(isset($data['class'])) ? $data['class'] : '',
								$data['content']
							);
						}
						echo implode(' | ', $output);
					}
					?>
					</td>
				</tr>

				<tr id="view-items-<?php echo $order_id; ?>" style="display:none;">
					<td colspan="6">
						<?php
						$product_id = '';
						foreach ($valid as $key => $item){

							// Get variation data if there is any.
							$variation_detail = !empty( $item['variation_id'] ) ? WCV_Orders::get_variation_data( $item[ 'variation_id' ] ) : '';

							 echo $item['qty'] . 'x ' . $item['name'];
							if ( !empty( $variation_detail ) ) echo '<br />' . $variation_detail;


						};
						?>
					</td>
				</tr>
				<?php if ($needs_shipping): ?>
				<tr id="view-tracking-<?php echo $order_id; ?>" style="display:none;">
					<td colspan="6">
						<div class="order-tracking">
							<?php
							wc_get_template( 'shipping-form.php',
											array(
												'order_id'       => $order_id,
												'product_id'     => $product_id,
												'package_per_vendor' => $package_per_vendor,
											),
											'wc-vendors/orders/shipping/',
											wcv_plugin_dir . 'templates/orders/shipping/'
										);
							?>
						</div>

					</td>
				</tr>
				<?php endif ?>

			<?php endforeach; ?>

			<tr>
				<td><b>Total:</b></td>
				<td colspan="5"><?php echo wc_price( $totals ); ?></td>
			</tr>

	<?php else : ?>

		<tr>
			<td colspan="5"
				style="text-align:center;"><?php _e( 'You have no orders during this period.', 'arti-marketplace-correios' ); ?></td>
		</tr>

	<?php endif; ?>

	</tbody>
</table>
