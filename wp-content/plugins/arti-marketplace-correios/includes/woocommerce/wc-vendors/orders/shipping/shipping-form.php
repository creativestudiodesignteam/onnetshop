<form method="post" name="track_shipment" id="track-shipment_<?php echo $order_id; ?>">
<table>
<tbody>
<tr>
    <th><?php _e( 'Shipping method', 'arti-marketplace-correios' )?></th>
    <th><?php _e( 'Shipping cost', 'arti-marketplace-correios' )?></th>
    <th><?php _e( 'Tracking code:', 'arti-marketplace-correios' )?></th>
</tr>
<?php
wp_nonce_field( 'track-shipment' );

foreach($package_per_vendor as $package_id => $item):

    $tracking_url  = wc_get_order_item_meta($package_id, 'correios_tracking_url');
    $tracking_code = wc_get_order_item_meta($package_id, 'correios_tracking_code');
    $method_label  = wc_get_order_item_meta($package_id, 'method_label');

    $pacakge_data = (new WC_Order_Item_Shipping($package_id))->get_data();

?>
<tr>
    <td><?php echo esc_html($pacakge_data['method_title'])?></td>
    <td><?php echo wc_price(esc_html($pacakge_data['total']))?></td>
    <td>
<?php
    if(!arti_wcvc_is_correios_method($method_label)) {
        woocommerce_wp_text_input(array(
                                    'id'          => "correios_tracking_url[$package_id]",
                                    'label'       => '',
                                    'placeholder' => __('Tracking URL', 'arti-marketplace-correios'),
                                    'description' => sprintf('<i>%s</i>',
                                                     __('URL (with HTTP or HTTPS, ex.: https://mytrackingsite.com/)',
                                                        'arti-marketplace-correios')),
                                    'value'       => $tracking_url
                            ));
    }
?>
<?php
    woocommerce_wp_text_input(array(
                                    'id'          => "correios_tracking_code[$package_id]",
                                    'label'       => '',
                                    'placeholder' => __('Tracking code', 'arti-marketplace-correios'),
                                    'description' => __('<i>Ex.: AA123456789BR</i>', 'arti-marketplace-correios'),
                                    'value'       => $tracking_code
                            ));
?>
    <input type="hidden" name="order_id" value="<?php echo $order_id ?>">
</tr>
<?php
endforeach;
?>
</tbody>
</table>
    <input class="button" type="submit" name="update_correios_tracking"
           value="<?php _e( 'Update tracking number', 'arti-marketplace-correios' ); ?>">

</form>

