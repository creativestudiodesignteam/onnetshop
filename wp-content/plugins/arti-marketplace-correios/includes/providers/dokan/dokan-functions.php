<?php
if ( ! defined( 'ABSPATH' ) ) {
exit; // Exit if accessed directly.
}

function arti_wcvc_dokan_template_part($template, $slug, $name){
    return arti_wcvc_woocommerce_locate_template($template, '/dokan/' . $slug . '.php');
}
add_filter('dokan_get_template_part', 'arti_wcvc_dokan_template_part', 10, 3);
