<?php
/**
* Encapsulates functionalities of Dokan marketplace
*
* @author  Luis Eduardo Braschi <http://art-idesenvolvimento.com.br>
*/

class Arti_Dokan_Provider extends Arti_Abstract_Vendors_Provider implements Arti_Shipping_Processor {

    public function __construct() {

        if(!class_exists('WeDevs_Dokan')) {

            $this->provider_not_found_message = __('The plugin "Dokan" could not be found. Please, install and activate it before registering this adapter.', 'arti-marketplace-correios');
            $this->is_active = false;

            add_action('admin_notices', array($this, 'provider_not_found'));

        } else {

            add_action('woocommerce_order_status_processing', array($this, 'process_shipping_due'), 11, 1);
            add_action('woocommerce_order_status_completed', array($this, 'process_shipping_due'), 11, 1);
            add_action('arti_wcvc_provider_dokan_loaded', array($this, 'hooks'), 11, 1);
            add_action('woocommerce_package_rates',  array($this, 'remove_dokan_shipping_rate'), 10, 2);
            add_filter('dokan_product_updated', array($this, 'product_updated'), 10, 1);

            add_action('arti_wcvc_add_tracking_code_to_package', array($this, 'after_adding_tracking_info'), 10, 4);

            add_filter('dokan_get_seller_amount_from_order_array', array($this, 'seller_shipping_amount_from_order'), 10, 3);

            // admin fields
            add_action('dokan_seller_meta_fields', array($this, 'add_admin_corporate_fields'));
            add_action('dokan_seller_meta_fields', array($this, 'add_admin_disable_correios_vendor_fields'));
            add_action('dokan_process_seller_meta_fields', array($this, 'save_vendor_fields'));

            // front-end  fields
            add_action('dokan_settings_form_bottom', array($this, 'add_vendor_fields'));
            add_action('dokan_settings_form_bottom', array($this, 'add_disable_correios_vendor_fields'));
            add_action('dokan_store_profile_saved', array($this, 'save_vendor_fields'));

        }

    }

    public function get_vendor_postcode($vendor_id){

        $info = get_user_meta($vendor_id, 'dokan_profile_settings', true);
        $post_code = isset($info['address']['zip']) ? $info['address']['zip'] : get_user_meta($vendor_id, 'arti_wcvc_postcode', true);

        if(!$post_code) {
            $message = sprintf(__('Vendor with id %d has no post code.', 'arti-marketplace-correios'), $vendor_id);
            throw new Exception($message);
        }

        return $post_code;

    }

    public function get_vendor_id_from_product($id) {

        $vendor = get_post_field('post_author', $id);

        if(!$vendor || !dokan_is_user_seller($vendor)) {
            $message = sprintf(__('Product with id %d has no vendor assigned to it.', 'arti-marketplace-correios'), $id);
            throw new Exception($message);
        }

        return $vendor;
    }

    public function get_vendor_name($id) {

        $vendor = new Dokan_Vendor($id);

        $vendor_name = $vendor->get_shop_name();

        if(empty($vendor_name)){
            $vendor_name = get_the_author_meta('display_name', $id);
        }

        return $vendor_name;
    }

    public function mark_as_shipped($order_id, $vendor_id = null, $package_id = null){

        $shippers = (array) get_post_meta($order_id, 'dokan_shipped', true);
        $vendor_id = !is_null($vendor_id)?$vendor_id:get_current_user_id();

        if(!in_array($vendor_id, $shippers)) {

            $shippers[] = $vendor_id;
            $shippers = array_unique(array_filter($shippers));
            update_post_meta($order_id, 'dokan_shipped', $shippers);

        }

        $this->add_mark_as_shipped_note($order_id, $vendor_id, $package_id);

    }

    private function add_mark_as_shipped_note($order_id, $vendor_id, $package_id){

        $order = wc_get_order($order_id);
        $shippers[] = $vendor_id;
        $vendor_name = $this->get_vendor_name($vendor_id);

        $message = sprintf(__( '%s added shipping info.', 'arti-marketplace-correios'), $vendor_name);
        $tracking_url = wc_get_order_item_meta($package_id, 'correios_tracking_url');
        $tracking_code = wc_get_order_item_meta($package_id, 'correios_tracking_code');

        ob_start();
        include_once 'views/html-order-shipping-note.php';
        $html = ob_get_clean();

        $order->add_order_note($html, true);

    }

    /**
     * Distributes the shipping dues accourding to their vendors
     * @param  int $order_id the id of the WooCommerce order
     * @return void
     */
    public function process_shipping_due($order_id){

    }

    public function get_order_comission_by_vendor($order_id, $vendor_id){
        return 0;
    }

    public function hooks(){

        add_filter('dokan_shipping_method', function($ret){

            remove_filter('woocommerce_package_rates', 'arti_hide_shipping_methods');

            return $ret;
        });

        if(current_user_can('dokan_manage_order_note')) {
            add_action('wp_ajax_dokan_add_shipping_tracking_info', 'arti_wcvc_save_vendor_tracking_code');
        }

        add_filter('arti_wcvc_cart_shipping_packages', array($this, 'add_vendor_info_to_package'), 10);

        add_filter('wc_shipping_simulator_package', array($this, 'add_vendor_info_to_shipping_simulator_package'), 9, 2);

        // temporary solution to poor package menagement by Dokan
        add_filter('woocommerce_correios_origin_postcode', array($this, 'force_vendor_info_to_package'), 10, 4);

    }

    /**
     * Temporary solution to poor package menagement by Dokan
     */
    public function force_vendor_info_to_package($origin_postcode, $id, $instance_id, $package){

        $vendor_provider = Arti_Vendor_Provider_Factory::$current_provider;

        if(isset($package['seller_id'])) {
            $vendor_id = $package['seller_id'];

            $post_code = '';

            try {

                $post_code = $vendor_provider->get_vendor_postcode($vendor_id);

            } catch (Exception $e) {

               	arti_wcvc_log( $e->getMessage() );

            }

            $package['vendor_name'] = $this->get_vendor_name($vendor_id);
            $package['vendor_postcode'] = $post_code;
            $package['vendor_id'] = $vendor_id;

        }

        return isset($package['vendor_postcode']) ? $package['vendor_postcode'] : $origin_postcode;
    }


    /**
     * Adds Art-i's vendor info to the package
     * @param array $packages
     */
    public function add_vendor_info_to_package($packages){

        $new_packages = array();

        foreach($packages as $package){

            $vendor_id = $package['vendor_id'];

            $package['seller_id'] = $vendor_id;
            $package['user'] = array('ID' => get_current_user_id());

            $new_packages[] = $package;

        }

        return $new_packages;

    }

    public function add_vendor_info_to_shipping_simulator_package($package, $product){

        if( is_array( $product ) ){
            $product_id = $product['variation_id'] > 0 ? $product['variation_id'] : $product['product_id'];
            $product = wc_get_product( $product_id );
        }

        $product_id = $product->get_id();

        try {

            $package['seller_id'] = $this->get_vendor_id_from_product($product_id);

        } catch ( Exception $e ) {

			arti_wcvc_log( $e->getMessage(), 'arti-wcvc-wc-simulador' );

        }

        return $package;
    }

    public function remove_dokan_shipping_rate($package_rates, $package){

        $seller_id = $package['seller_id'];

        if( class_exists('Dokan_WC_Shipping')
            && !Dokan_WC_Shipping::is_shipping_enabled_for_seller($seller_id)
            && isset($package_rates['dokan_product_shipping'])){

            unset($package_rates['dokan_product_shipping']);

        }

        return $package_rates;

    }

    public function product_updated($product_id){
        arti_wcvc_disable_methods_save($product_id);
    }

    public function after_adding_tracking_info($updated, $order_id, $vendor_id, $package_id){
        if(!is_admin()){
            $url = wp_nonce_url(add_query_arg( array( 'order_id' => $order_id), dokan_get_navigation_url( 'orders')), 'dokan_view_order');
            wp_safe_redirect(html_entity_decode($url));
            exit();
        }
    }

    /**
     * Corrects shipping amount in Dokan pro
     * @param  array $net_amount
     * @param  WC_Order $order
     * @param  int|string $seller_id
     * @return array $net_amount
     */
    public function seller_shipping_amount_from_order($net_amount, $order, $seller_id){

        $commission_recipient = dokan_get_option( 'extra_fee_recipient', 'dokan_general', 'seller' );
        if ( 'seller' == $commission_recipient ) {
            $net_amount['shipping'] = $this->get_order_shipping_amount_for_vendor($order->get_id(), $seller_id);
        }
        return $net_amount;

    }

    /**
     * Will add fields to user dashboard in the font-end
     * @return void
     */
    public function add_vendor_fields(){
        $vendor_id = dokan_get_current_user_id();
        $this->render_corporate_fields_html( $vendor_id, ARTI_DOKAN_DIR . '/views/html-corporate-correios-fields.php' );
    }

    public function add_admin_corporate_fields( $vendor ){
        $this->render_corporate_fields_html( $vendor->ID, ARTI_DOKAN_DIR . '/views/html-admin-corporate-fields.php' );
    }

    /**
     * Add admin field to disable Correios store-wide
     * @param WP_User $user
     * @return void
     */
    public function add_admin_disable_correios_vendor_fields( $user ){

        $correios_disabled = get_user_meta( $user->ID, '_arti_shipping_correios_disabled', true );
        include_once ARTI_DOKAN_DIR . '/views/html-disable-correios-field-admin.php';

    }

    /**
     * Add front-end field to disable Correios store-wide
     * @return void
     */
    public function add_disable_correios_vendor_fields(){

        $correios_disabled = get_user_meta( dokan_get_current_user_id(), '_arti_shipping_correios_disabled', true );
        include_once ARTI_DOKAN_DIR . '/views/html-disable-correios-field.php';

    }

    /**
     * Save the corporate info
     * @param  int|string $vendor_id
     * @return void
     */
    public function save_vendor_fields( $vendor_id ){
        arti_wcvc_update_user_meta( $vendor_id );
    }

}
