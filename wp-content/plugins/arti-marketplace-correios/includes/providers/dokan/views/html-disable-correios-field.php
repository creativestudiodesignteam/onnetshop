<div class="dokan-form-group">
    <label class="dokan-w3 dokan-control-label"><?php esc_html_e( 'Disable Correios', 'arti-marketplace-correios' ); ?></label>
    <div class="dokan-w5 dokan-text-left">
        <div class="checkbox">
            <label>
                <input type="checkbox" name="_arti_shipping_correios_disabled" value="yes" <?php checked( $correios_disabled, 'yes' ); ?>> <?php esc_html_e( 'Disable Correios Shipping store-wide', 'arti-marketplace-correios' ); ?>
            </label>
        </div>
    </div>
</div>
