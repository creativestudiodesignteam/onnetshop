<div class="dokan-form-group">
    <label class="dokan-w3 dokan-control-label" for="correios_login"><?php _e( 'Correios Login', 'arti-marketplace-correios' ); ?></label>
    <div class="dokan-w5 dokan-text-left">
        <input id="correios_login" value="<?php echo esc_attr( $correios_login ); ?>" name="_correios_login" class="dokan-form-control input-md" type="text">
    </div>
</div>
<div class="dokan-form-group">
    <label class="dokan-w3 dokan-control-label" for="correios_password"><?php _e( 'Correios Password', 'arti-marketplace-correios' ); ?></label>
    <div class="dokan-w5 dokan-text-left">
        <input id="correios_password"  name="_correios_password" class="dokan-form-control input-md" type="password" placeholder="<?php echo $password_placeholder;?>">
    </div>
</div>
