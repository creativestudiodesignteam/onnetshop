<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly.
}

define( 'ARTI_WCVENDORS_DIR', dirname( __FILE__ ) );

include_once 'class-wc-vendors.php';
