<h2><?php esc_html_e( 'Correios Corporate Code', 'arti-marketplace-correios' ); ?></h2>

<div>
     <p>
        <b>
            <label for="_correios_login"><?php esc_html_e( 'Correios Login', 'arti-marketplace-correios' ); ?></label>
        </b>
        <br>
        <input type="text" id="_correios_login" name="_correios_login" class="regular-text" value="<?php echo esc_attr( $correios_login ) ?>">
        <br>
        <b>
            <label for="_correios_password"><?php esc_html_e( 'Correios Password', 'arti-marketplace-correios' ); ?></label>
        </b>
        <br>
        <input type="password" id="_correios_password" name="_correios_password" class="regular-text" placeholder="<?php echo $password_placeholder;?>">
    </p>
 </div>
