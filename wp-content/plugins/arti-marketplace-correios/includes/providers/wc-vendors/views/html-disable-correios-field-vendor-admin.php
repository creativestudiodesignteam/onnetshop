<tr>
    <th>
        <label for="_arti_shipping_correios_disabled">
            <?php esc_html_e( 'Disable Correios Shipping store-wide', 'arti-marketplace-correios' ); ?>
        </label>
    </th>
    <td>
        <input type="checkbox" id="_arti_shipping_correios_disabled" name="_arti_shipping_correios_disabled" class="regular-text" value="yes" <?php checked( $correios_disabled, 'yes' ) ?>>
    </td>
</tr>
