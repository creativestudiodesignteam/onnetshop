<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly.
}

/**
 * Art-i WC Marketplace root
 */
define( 'ARTI_WCMP_DIR', dirname( __FILE__ ));

function arti_wcvc_wcmp_scripts(){
    $package_url = plugin_dir_url( __FILE__ );
    $suffix = defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ? '' : '.min';
    $dashboard_scripts = $package_url . 'assets/dashboard' . $suffix . '.js';
    wp_enqueue_script('arti-wcmp-dashboard', $dashboard_scripts, array('jquery', 'wp-util'));
}
add_action('wp_enqueue_scripts', 'arti_wcvc_wcmp_scripts');

include_once 'class-wc-marketplace.php';
