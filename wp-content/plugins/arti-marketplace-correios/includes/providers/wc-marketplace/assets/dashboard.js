jQuery(document).ready(function ($) {

    $('#wcmp-vendor-orders, #widget_vendor_pending_shipping').on('draw.dt', function(e, settings){

        var tracking_info = {};

        var rows_data = $(this).DataTable().rows().data();
        var orders_ids = rows_data.pluck('order_id').toArray();

        for (var i = 0; i < orders_ids.length; i++) {

            var order_id = orders_ids[i];
            if( !Number.isInteger( parseInt( order_id ) ) ){
                order_id = parseInt( $(orders_ids[i]).text().replace(/[^0-9]/gi,'') );
            }
            tracking_info[order_id] = JSON.parse($( '#tracking_info_' + order_id ).val());

        }

        window.arti_wcvc_tracking_info = tracking_info;

        $( '.modal-footer' ).append( wp.template( 'tracking-nonce' ) );

    });

});

function populate_modal(self, order_id){

    var packages = window.arti_wcvc_tracking_info[order_id];

    jQuery('.modal-body').remove();

    for(var package_id in packages){
        manage_fields(package_id, packages[package_id]);
    }

    wcmpMarkeAsShip(self, order_id);

    jQuery('.modal-body').show();

}

function manage_fields(package_id, package) {

    var field;
    var template;

    if(package.is_correios) {
        template = wp.template("correios-method");
    } else {
        template = wp.template("other-methods");
    }

    data = {
        'url': package.url,
        'value': package.value,
        'package_id': package_id
    };

    jQuery( template(data) ).insertBefore('.modal-footer');

}
