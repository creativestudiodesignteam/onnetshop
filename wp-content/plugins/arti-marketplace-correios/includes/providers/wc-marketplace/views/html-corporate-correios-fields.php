<div class="panel panel-default pannel-outer-heading">
    <div class="panel-heading">
        <h3>Correios</h3>
    </div>
    <div class="panel-body panel-content-padding form-horizontal">
    <?php
    ( new WCMp_Frontend_WP_Fields )->text_input( $login_field );
    ( new WCMp_Frontend_WP_Fields )->text_input( $password_field );
    do_action( 'arti_wcvc_wcmp_add_disable_field' );
    ?>
    </div>
</div>
