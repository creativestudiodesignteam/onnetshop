<script type="text/html" id="tmpl-tracking-nonce">
    <?php wp_nonce_field( 'track-shipment' ); ?>
</script>
<script type="text/html" id="tmpl-correios-method">
    <div class="modal-body">
        <h3><?php _e('Correios tracking code', 'arti-marketplace-correios') ?></h3>
        <div class="form-group">
            <label for="correios_tracking_code"><?php _e('Enter Tracking ID', 'arti-marketplace-correios'); ?> </label>
            <input type="text" class="form-control" id="correios_tracking_code" name="correios_tracking_code[{{data.package_id}}]" value="{{data.value}}">
        </div>
    </div>
</script>
<script type="text/html" id="tmpl-other-methods">
    <div class="modal-body">
        <h3><?php _e('Tracking code for other methods', 'arti-marketplace-correios') ?></h3>
        <div class="form-group">
            <label for="package_tracking_url_{{data.package_id}}"><?php _e('Enter Tracking Url', 'arti-marketplace-correios'); ?> </label>
            <input type="url" class="form-control" id="package_tracking_url_{{data.package_id}}" name="correios_tracking_url[{{data.package_id}}]" value="{{data.url}}">
        </div>
        <div class="form-group">
            <label for="package_tracking_code_{{data.package_id}}"><?php _e('Enter Tracking ID', 'arti-marketplace-correios'); ?> </label>
            <input type="text" class="form-control" id="package_tracking_code_{{data.package_id}}" name="correios_tracking_code[{{data.package_id}}]" value="{{data.value}}">
        </div>
    </div>
</script>
