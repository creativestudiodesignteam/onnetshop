<?php
if ( ! defined( 'ABSPATH' ) ) {
		exit; // Exit if accessed directly.
}

/**
 * Add Shipping Simulator support
 * @param  array $package
 * @param  WC_Product $product
 * @return array
 */
function arti_wcvc_shipping_simulator_package( $package, $product ){

		$vendor_provider = arti_wcvc_current_provider();

		if( is_array( $product ) ){
				$product_id = $product['variation_id'] > 0 ? $product['variation_id'] : $product['product_id'];
				$product = wc_get_product( $product_id );
		}

		$product_id = $product->get_id();

		$post_code = '';

		try {

				$vendor_id = $vendor_provider->get_vendor_id_from_product($product_id);
				$post_code = $vendor_provider->get_vendor_postcode($vendor_id);

		} catch ( Exception $e ) {

			arti_wcvc_log( $e->getMessage() );

		}

		$package['vendor_postcode'] = $post_code;
		$correios_disabled = get_post_meta($product->get_id(), '_arti_shipping_correios_disabled', true);
		$package['correios_disabled'] = $correios_disabled === 'yes' ? $correios_disabled : 'no';

		return $package;

}

add_filter('wc_shipping_simulator_package', 'arti_wcvc_shipping_simulator_package', 10, 2);

/**
 * Add CFPP support
 * @return void
 */
function arti_wcvc_add_cfpp_support(){

		add_filter('woocommerce_correios_origin_postcode', function($origin_postcode, $id, $instance_id, $package){

				$vendor_provider = arti_wcvc_current_provider();
				$product_id = key($package['contents']);

				$origin_postcode = '';

				try {

					$vendor_id = $vendor_provider->get_vendor_id_from_product($product_id);
					$origin_postcode = $vendor_provider->get_vendor_postcode($vendor_id);

				} catch ( Exception $e ) {

					arti_wcvc_log( $e->getMessage(), 'arti-wcvc-cfpp' );

				}

				return $origin_postcode;

		}, 10, 4);


}

add_action('cfpp_before_calculate_cost', 'arti_wcvc_add_cfpp_support');

/**
 * Assigns the vendor's postcode to their package
 * @param  string     $origin_postcode
 * @param  string/int $id
 * @param  string/int $instance_id
 * @param  array      $package
 * @return string
 * @todo  merge with arti_wcvc_vendor_postcode()
 */
function arti_wcvc_assign_postcode_to_package($origin_postcode, $id, $instance_id, $package){

		$origin_postcode = arti_wcvc_vendor_postcode($origin_postcode, $package);

		return $origin_postcode;

}

add_filter('woocommerce_correios_origin_postcode', 'arti_wcvc_assign_postcode_to_package', 10, 4);
