<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly.
}

/**
 * @author  Luis Eduardo Braschi <http://art-idesenvolvimento.com.br>
 * @todo    Sometimes no vendor is subscribed to an item. Bypass this item.
 */

add_filter( 'woocommerce_cart_shipping_packages', 'arti_wcvc_cart_shipping_packages', 99 );
/**
* Separates all products into packages by vendor in order to calculate the shippings separately.
* We have to add the postcode here and extend every single shipping class
* because the filter provided by the original author looks useless for us.
*
*/
function arti_wcvc_cart_shipping_packages( $packages ) {

    $vendor_provider = Arti_Vendor_Provider_Factory::$current_provider;

    if(!$vendor_provider || !$vendor_provider->is_active ) {
        return $packages;
    }

    $cart_items = WC()->cart->get_cart();

    foreach ( $cart_items as $item ) {

        $product = $item['data'];

        if ( $product->needs_shipping() ) {
            Arti_WCVC_ProtoPackage_Splitting::add_item($item);
        }
    }

    $items_by_package = Arti_WCVC_ProtoPackage_Splitting::get_split_protopackages();

    /**
     * Whether to use the marketplace provider's own package management
     * @param array $items_by_package cart items grouped by methods
     * @var bool
     */
    $use_native = apply_filters( 'arti_wcvc_use_native_packaging', false, $items_by_package );

    if( $use_native ) {
        return $packages;
    }

    $packages = arti_wcvc_get_shipping_packages($items_by_package);

    return apply_filters( 'arti_wcvc_cart_shipping_packages', $packages );
}

function arti_wcvc_get_shipping_packages($items_by_package){
    foreach($items_by_package as $vendor_id => $vendor_protopackage) {
        foreach($vendor_protopackage as $protopackage){

            $items = $protopackage['items'];

            $package = array(
                'contents_cost'     => array_sum( wp_list_pluck( $items, 'line_total' ) ),
                'contents'          => $items,
                'vendor_name'       => $protopackage['vendor_name'],
                'vendor_postcode'   => $protopackage['post_code'],
                'vendor_id'         => $protopackage['vendor_id'],
                'correios_disabled' => $protopackage['correios_disabled'],
                );
            $packages[] = arti_wcvc_default_package($package);
        }
    }

    return $packages;

}

/**
 * Adds a method "label" from method's ID so we can be able to see whether
 * it's one from Correios
 * @see WC_Checkout::create_order_shipping_lines
 */
function arti_wcvc_add_method_id_to_items($item, $package_key, $package, $order){
    $item->add_meta_data( 'method_label', $item['method_id']);
}

add_action( 'woocommerce_checkout_create_order_shipping_item', 'arti_wcvc_add_method_id_to_items', 10, 4 );

/**
 * Adding vendor IDs to shipping line items
 * @param int $order_id
 * @param int $item_id
 * @param int $package_key
 */
function arti_wcvc_add_vendor_id_to_items( $item, $package_key, $package ){

    $vendor_id = $package['vendor_id'];
    $vendor_name = $package['vendor_name'];

    $item->update_meta_data( 'vendor_id', $vendor_id );
    $item->add_meta_data( 'vendor_name', $vendor_name );
    $item->add_meta_data( __('Vendor', 'arti-marketplace-correios'), $vendor_name );

}
add_action( 'woocommerce_checkout_create_order_shipping_item', 'arti_wcvc_add_vendor_id_to_items', 99, 3 );

/**
 * Merge all packages after calculating their shipping rates
 * @param  array $packages
 * @return array
 */
function arti_wcvc_merge_packages( $packages ){

    $one_method_to_rule_them_all = get_option( 'arti_wcvc_one_method_allowed', false );

    if( !wc_string_to_bool( $one_method_to_rule_them_all ) ){
        return $packages;
    }

    // remove vendors names from calculator
    remove_filter('woocommerce_shipping_package_name', 'arti_wcvc_show_vendor_info_in_shipping_calculator', 11 );

    $merged_packages = array(
        'rates' => array(),
        'seller_id' => '',
        'destination' => ''
    );

    $proxy_rates = array();

    foreach( $packages as $package ){

        $rates = $package['rates'];
        $merged_packages['destination'] = $package['destination'];

        foreach( $rates  as $key => $rate ){

            if( !isset($proxy_rates[$key]) ){
                // Create new rate object for every rate in the original packages.
                $proxy_rates[$key] = new WC_Shipping_Rate();
                $proxy_rates[$key]->set_id( $rate->id );
                $proxy_rates[$key]->set_method_id( $rate->get_method_id() );
                $proxy_rates[$key]->set_label( $rate->get_label() );

                $meta_data = $rate->get_meta_data();
                array_map( array( $proxy_rates[$key], 'add_meta_data' ), array_keys( $meta_data ), $meta_data );
            }

            $proxy_rates[$key]->set_cost( $proxy_rates[$key]->cost + $rate->cost );

        }

    }

    $merged_packages['rates'] = $proxy_rates;

    return array($merged_packages);

}

add_action( 'woocommerce_shipping_packages', 'arti_wcvc_merge_packages', 11 );

add_filter( 'woocommerce_checkout_process', function(){
    add_filter( 'option_arti_wcvc_one_method_allowed', '__return_true' );
    remove_action( 'woocommerce_shipping_packages', 'arti_wcvc_merge_packages', 11 );
} );

