<?php

/**
 * Will split cart items into templates of packages here called "protopackages".
 */
class Arti_WCVC_ProtoPackage_Splitting {

    private static $protopackages = array();

    public static function add_item($item){

        $product = $item['data'];
        $product_id = $product->get_id();

        $vendor_provider = Arti_Vendor_Provider_Factory::$current_provider;
        $vendor_id = 0;

        try {

            $vendor_id = $vendor_provider->get_vendor_id_from_product( $product_id );

        } catch ( Exception $e ) {

           $debug = (bool) get_option( 'arti_wcvc_debug', false );

           if( $debug ) {
               $logger = new WC_Logger();
               $logger->add('arti-wcvc', $e->getMessage());
           }

        }

        $correios_disabled = get_post_meta( $product->get_id(), '_arti_shipping_correios_disabled', true );

        $correios_disabled_context = $correios_disabled === 'yes' ? 'yes' : 'no';

        if( 'yes' === get_user_meta( $vendor_id, '_arti_shipping_correios_disabled', true )){
            $correios_disabled = 'yes';
            $correios_disabled_context = 'yes';
        }

        self::split_protopackages_by_context( $vendor_id, $correios_disabled_context, $item, $correios_disabled );

    }

    public static function split_protopackages_by_context( $vendor_id, $context, $item, $correios_disabled = 'no' ){

        $product = $item['data'];
        $product_id = $product->get_id();

        $vendor_provider = Arti_Vendor_Provider_Factory::$current_provider;

        $post_code = '';

        try {

            $post_code = $vendor_provider->get_vendor_postcode( $vendor_id );

        } catch ( Exception $e ) {

           arti_wcvc_log( $e->getMessage() );

        }

        if(!isset(self::$protopackages[$vendor_id][$context]['correios_disabled'])) {
            self::$protopackages[$vendor_id][$context]['correios_disabled'] = $correios_disabled;
        }

        self::$protopackages[$vendor_id][$context]['items'][$product_id] = $item;
        self::$protopackages[$vendor_id][$context]['vendor_id'] = $vendor_id;
        self::$protopackages[$vendor_id][$context]['vendor_name'] = $vendor_provider->get_vendor_name( $vendor_id );
        self::$protopackages[$vendor_id][$context]['post_code'] = $post_code;
        self::$protopackages[$vendor_id][$context]['correios_disabled'] = $correios_disabled;

    }

    public static function reset_protopackages(){
        self::$protopackages = array();
    }

    /**
     * Return the protopackages sorted by vendor ID
     * @return array
     */
    public static function get_split_protopackages(){
        ksort( self::$protopackages );
        return apply_filters( 'arti_get_split_protopackages', self::$protopackages );
    }

}
