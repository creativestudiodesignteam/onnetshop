<?php
/**
 * Will load a marketplace support based on config
 * @author Luis Eduardo Braschi
 */
class Arti_Vendor_Provider_Factory {

    /**
     * The provider loaded from the config
     * @var Arti_Shipping_Processor
     */
    public static $current_provider;

    public static function load_provider($identifier){

        $class_name = "Arti_{$identifier}_Provider";

        if(!class_exists($class_name)) {
            return false;
        }

        $provider_instance =  new $class_name();

        if(!($provider_instance instanceof Arti_Shipping_Processor)) {
            return false;
        }

        self::$current_provider = $provider_instance;

        return self::$current_provider;
    }

}
