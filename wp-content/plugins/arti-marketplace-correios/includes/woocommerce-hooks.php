<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly.
}
/**
 * Add vendors' names to the shipping list
 * @param  string $name    The name providade by the API
 * @param  int|string $i   The index of the shipping package
 * @param  array $package  The shipping package
 * @return string
 */
function arti_wcvc_show_vendor_info_in_shipping_calculator( $name, $i, $package ){

    if( get_option('arti_wcvc_vendorname_on_calculator') ){

        $provider = arti_wcvc_current_provider();
        $vendor_name = $provider->get_vendor_name( $package['vendor_id'] );

        try{

		    $vendor_postcode = $provider->get_vendor_postcode( $package['vendor_id'] );

        } catch ( Exception $e ) {

			arti_wcvc_log( $e->getMessage(), 'arti-wcvc-wc-simulador' );

        }

        $template = get_option( 'arti_wcvc_vendorname_on_calculator_text' );

        $name = str_replace(
            array(
                '{vendor_name}',
                '{vendor_postcode}'
            ),
            array(
                $vendor_name,
                $vendor_postcode
            ),
            $template
        );

    }


    return esc_html( $name );

}

add_filter('woocommerce_shipping_package_name', 'arti_wcvc_show_vendor_info_in_shipping_calculator', 11, 3);

/**
 * Hide methods from the package based on disabled
 * @param  array  $rates
 * @param  array  $package
 * @return array            The filtered rates
 */
function arti_hide_shipping_methods( $rates, $package ) {

    $correios_disabled = $package['correios_disabled'];
    $correios_methods = arti_wcvc_list_shipping_methods();

    // We have to run through all "rates" because they might have keys
    // that are different from the actual method label because
    // WooCommerce appends the instance ID to some array keys.
    $rates = array_filter($rates, function($rate) use ($correios_disabled, $correios_methods){
        return !('yes' === $correios_disabled && in_array($rate->get_method_id(), $correios_methods));
    });

    return $rates;
}

add_filter('woocommerce_package_rates', 'arti_hide_shipping_methods', 10, 2);

/**
 * Returns the corporate info requested
 * @return string
 */
function arti_wcvc_get_corporate_login($login, $id, $instance_id, $package){

    if(isset($package['vendor_id']) &&
        $user_login = get_user_meta($package['vendor_id'], '_correios_login', true)) {
        $login = $user_login;
    }

    return $login;
}

add_filter('woocommerce_correios_login', 'arti_wcvc_get_corporate_login', 10, 4);

/**
 * Returns the corporate info requested
 * @return string
 */
function arti_wcvc_get_corporate_password($password, $id, $instance_id, $package){

    if(isset($package['vendor_id']) &&
        $user_password = get_user_meta($package['vendor_id'], '_correios_password', true)) {

        $password = $user_password;
    }

    return $password;
}

add_filter('woocommerce_correios_password', 'arti_wcvc_get_corporate_password', 10, 4);

/**
 * Adding our own templates
 */
function arti_wcvc_template_base_path() {
    /**
     * Plugin authors will be able to override this to their own plugin path
     * @var string
     */
    $arti_wcvc_template_base_path = apply_filters('arti_wcvc_template_base_path', untrailingslashit(plugin_dir_path(__FILE__)) . '/woocommerce/');
    return $arti_wcvc_template_base_path;
}

add_filter('woocommerce_locate_template', 'arti_wcvc_woocommerce_locate_template', 10, 3);
