<?php
/**
 * electro engine room
 *
 * @package electro
 */

/**
 * Initialize all the things.
 */


// hide update notifications
function remove_core_updates(){
global $wp_version;return(object) array('last_checked'=> time(),'version_checked'=> $wp_version,);
}
add_filter('pre_site_transient_update_core','remove_core_updates'); //hide updates for WordPress itself
add_filter('pre_site_transient_update_plugins','remove_core_updates'); //hide updates for all plugins
add_filter('pre_site_transient_update_themes','remove_core_updates'); //hide updates for all themes

require get_template_directory() . '/inc/init.php';


if(!defined(‘WCMP_UNLOAD_BOOTSTRAP_LIB’)){
define(‘WCMP_UNLOAD_BOOTSTRAP_LIB’, true);
}

add_filter('wcmp_vendor_dashboard_nav', 'add_wcmp_vendor_dashboard_nav');

function add_wcmp_vendor_dashboard_nav($nav) {
   $nav['custom_nav'] = array(
       'label' => __('Impulsionar', 'dc-woocommerce-multi-vendor') // label of the menu
       , 'url' => 'https://onnetshop.com/pedido-de-impulsionamento/' // url of the menu
       , 'capability' => true
       , 'position' => 100 //menu position
       , 'submenu' => array() // submenu items
       , 'link_target' => '_blank' // target window _blank or _self
       , 'nav_icon' => 'dashicons dashicons-megaphone' // menu icon wordpress dashicons resource (https://developer.wordpress.org/resource/dashicons/)
   );
   return $nav;
}

/**
 * Note: Do not add any custom code here. Please use a child theme so that your customizations aren't lost during updates.
 * http://codex.wordpress.org/Child_Themes
 */